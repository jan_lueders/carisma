<?php 
include_once __DIR__.'/override/rewrite_url_path_provider.php';

// Statische Klasse
class rewrite_url
{
    public static function getUrlParameters(){
        if (rex::isBackend() || 'off' == rex_config::get('rewrite_url', 'rewrite_type')) {
            return array();
        }
        return \RewriteUrl\Services\ServiceContainer::getInstance()
            ->get(\rex_config::get('rewrite_url', 'rewrite_type'))
            ->getCurrentRouteParameters();
    }
}

if ('off' != rex_config::get('rewrite_url', 'rewrite_type') ){

    rex_extension::register('PACKAGES_INCLUDED', function ($params) {
        global $REX;
        rex_url::init(new rewrite_url_path_provider($REX['HTDOCS_PATH'], $REX['BACKEND_FOLDER'], false));
        $service = RewriteUrl\Services\ServiceContainer::getInstance()
            ->get(rex_config::get('rewrite_url', 'rewrite_type'));
        return $service->unrewrite();
    }, rex_extension::EARLY);

    rex_extension::register('URL_REWRITE', function (rex_extension_point $ep) {
        $params = $ep->getParams();
        $service = RewriteUrl\Services\ServiceContainer::getInstance()
            ->get(rex_config::get('rewrite_url', 'rewrite_type'));
        return $service->rewrite($params);
    });

    // if anything changes -> refresh PathFile
    if (rex::isBackend()) {
        $extensionPoints = array(
            'CAT_ADDED',
            'CAT_UPDATED',   
            'CAT_DELETED', 
            'CAT_STATUS',
            'ART_ADDED',   
            'ART_UPDATED',   
            'ART_DELETED', 
            'ART_STATUS',
            'CLANG_UPDATED', 
            'CLANG_ADDED', 
            'CLANG_DELETED', 
            'ART_META_UPDATED',
        );
        foreach ($extensionPoints as $extensionPoint) {
            rex_extension::register($extensionPoint, function (rex_extension_point $ep) {
                $service = \RewriteUrl\Services\ServiceContainer::getInstance()
                    ->get(\rex_config::get('rewrite_url', 'rewrite_type'));
                $service->getMap(true);
            });
        }
    }
}

spl_autoload_register(function ($class) {
    $prefix = 'RewriteUrl';
    $base_dir = dirname(__FILE__).'/src';

    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) return;
    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});
spl_autoload_register(function ($class) {
    $prefix = 'Symfony\Component\Routing';
    $base_dir = dirname(__FILE__).'/vendor/smyfony_routing';

    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) return;
    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});




