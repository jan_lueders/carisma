<h1>Redaxo5 Addon Rewrite URL</h1>
<p>Repository: https://github.com/akuehnis/redaxo5_addon_rewrite_url</p>
<p><strong>Installation</strong></p>
<ol>
<li>AddOns &gt; Rewrite URL &gt; Installieren</li>
<li>Hauptnavigation &gt; Rewrite URL &gt; Einstellungen
    <ol>
    <li>.htaccess-Datei anlegen auf dem Root wie vorgeschlagen</li> 
    <li>Rewrite-Methode wählen und Speichern</li> 
    </ol>
</li>
</ol>
