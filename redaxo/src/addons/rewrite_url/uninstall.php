<?php

$sql = rex_sql::factory();

$sql->setQuery(sprintf('DROP TABLE IF EXISTS `%s`;', rex::getTable('rewrite_url_notfound')));

rex_delete_cache();
