<?php
namespace RewriteUrl\Services;

class NotfoundService
{
    public $container;
    public $table;

    public function __construct($container)
    {
        $this->container = $container;
        $this->table = \rex::getTable('rewrite_url_notfound');
    }

    
    public function findAll() 
    {
        $sql = \rex_sql::factory();

        $data = $sql->getDBArray("SELECT * FROM ".\rex::getTable('rewrite_url_notfound').
            " WHERE 1 ORDER BY not_found_url ASC", array(), \PDO::FETCH_ASSOC);
        $a = array();
        foreach ($data as $row) {
            $a[] = $this->getModel($row);
        }
        return $a;
    }

    public function find($id)
    {
        $sql = \rex_sql::factory();
        $data = $sql->getDBArray("SELECT * FROM ".\rex::getTable('rewrite_url_notfound').
            " WHERE id=?", array($id), \PDO::FETCH_ASSOC);
        if (false === $data) {
            return null;
        }
        return $this->getModel($data[0]);
    }

    public function insert($model){
        $data = get_object_vars($model);
        $db = \rex_sql::factory();
        $sql = "INSERT INTO `{$this->table}` (`".
            implode('`,`', array_keys($data))."`)
            VALUES (". implode(',', array_fill(0, count($data), '?')).")";
        $db->setQuery($sql, array_values($data));
        return '';
    }

    public function update($model){
        $data = get_object_vars($model);
        $db = \rex_sql::factory();
        $params = array();
        $sep = ' SET ';
        $sql = "UPDATE `{$this->table}`";
        foreach($data as $key=>$val){
            $sql .= $sep.$key.'=?';
            $binds[] = $val;
            $sep = ',';
        }
        $sql.= ' WHERE id='.$model->id;
        $db->setQuery($sql, array_values($data));

        return '';
        
    }

    public function remove($id){
        $db = \rex_sql::factory();
        $db->setQuery("DELETE FROM {$this->table} WHERE id=".intval($id));
        return '';
    }
    
    public function getModel($data) {
        if(!is_array($data)){
            return null;
        }
        $model = new \RewriteUrl\Models\Notfound();
        foreach ($data as $key => $val) {
            $model->{$key} = $val;
        }
        return $model;
    }

}

