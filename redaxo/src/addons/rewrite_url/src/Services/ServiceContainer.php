<?php
namespace RewriteUrl\Services;

class ServiceContainer
{
    public $services = array();
    public $namespace = '\RewriteUrl';

    static private $instance = null;


    public function get($service){
        if (!isset($this->services[$service])) {
            $class = $this->namespace .'\Services\\'.$service;
            $this->services[$service] = new $class($this);
        }
        return $this->services[$service];
    }
    public static function getInstance(){
        if (null === self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
        return new self();
    }

    private function __construct(){}
    private function __clone(){}
}
