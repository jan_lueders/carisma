<?php 
namespace RewriteUrl\Services;

class RewriteTools
{
    public function slug($string, $locale='all')
    {
        // strtolower verändert umlaute. entsprechend strtolower auch auf search anwenden
        $string = str_replace(
            array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'),
            array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'),
            $string
        );

        return
        // + durch - ersetzen
        str_replace('+', '-',
                // ggf uebrige zeichen url-codieren
                urlencode(
                    // mehrfach hintereinander auftretende spaces auf eines reduzieren
                    preg_replace('/ {2,}/', ' ',
                        // alle sonderzeichen raus
                        preg_replace('/[^a-zA-Z_\-0-9 ]/', '',
                            // sprachspezifische zeichen umschreiben
                            $this->replacements($string, $locale)
                        )
                    )
                )
        );
    }

    public function replacements($string, $locale){
        $a = parse_ini_file(__DIR__.'/../../replace_by.ini');
        if (isset($a['all'])) {
            foreach ($a['all'] as $s=>$r) {
                $string = str_replace($s, $r, $string);
            }
        }

        if ('all' != $locale && isset($a[$locale])) {
            foreach ($a[$locale] as $s=>$r) {
                $string = str_replace($s, $r, $string);
            }
        }
        return $string;
    }
}
