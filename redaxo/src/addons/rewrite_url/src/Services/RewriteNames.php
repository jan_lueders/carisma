<?php
namespace RewriteUrl\Services;

use RewriteUrl\Classes\RewriteCore;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

class RewriteNames extends RewriteCore
{

    public function buildRouteCollection(){
        $routes = parent::buildRouteCollection();//new RouteCollection();
        $tools = $this->container->get('RewriteTools');
        $clang_list = \rex_clang::getAll(false);
        foreach($clang_list as $clang){
            $prefix = '';
            if (
                1 < $clang->getPriority() 
                || 1 == \rex_config::get('rewrite_url', 'add_default_lang') 
            ){
                $prefix = '/'.$clang->getCode();
            }
            
            $db = \rex_sql::factory();
            $articles = $db->getArray("SELECT * FROM `".
                \rex::getTable('article')."` WHERE clang_id=?", array(
                    $clang->getId(),
            ));
            $article_idx = array();
            foreach($articles as $article){
                $article_idx[$article['id']] = $article;
            }
            unset($articles);
            
            foreach ($article_idx as $article){
                if (\rex_article::getSiteStartArticleId() == $article['id']) {
                    // Für Site-Startartikel wird unten eine URL angelegt
                    continue;
                }
                $route_params = array();
                $route_params['_locale'] = $clang->getCode();
                $route_params['_clang_id'] = $article['clang_id'];
                $route_params['_article_id'] = $article['id'];
                if('|' == $article['path'] && '' == $article['catname']){
                    // root artikel
                    $path = $prefix.'/'.$tools->slug($article['name'], $clang->getCode());
                } elseif ('|' == $article['path']) {
                    $path = $prefix.'/'.$tools->slug($article['catname'], $clang->getCode());
                } else {
                    $cats = explode('|', trim($article['path'], '|'));
                    $path = $prefix;
                    foreach($cats as $cat_id){
                        $path.= '/'. $tools->slug($article_idx[$cat_id]['catname'], $clang->getCode());
                    }
                    if( 1 != $article['startarticle']){
                        $path.= '/'. $tools->slug($article['name'], $clang->getCode());
                    }else{
                        $path.= '/'. $tools->slug($article['catname'], $clang->getCode());
                    }
                }
                if (1 == $this->add_trailing_slash) {
                    $path.= '/';
                }
                $route_name = 'article_'.$route_params['_article_id'].'_'.$article['clang_id'];
                $route = new Route($path, $route_params);
                $routes->add($route_name, $route);

                if (isset($article['art_sub_route']) && '' != trim($article['art_sub_route'])) {
                    // Create sub_routing_route
                    $subroute = trim($article['art_sub_route']);
                    $subroute = trim($subroute, '/');
                    if (0 == $this->add_trailing_slash) {
                        $path.= '/';
                    }
                    $path.= $subroute;
                    if (1 == $this->add_trailing_slash) {
                        $path.= '/';
                    }
                    $route_name = 'article_sub_route_'.$route_params['_article_id'].'_'.$article['clang_id'];
                    $route = new Route($path, $route_params);
                    $routes->add($route_name, $route);
                }

                
            }

            // Nur Sprache zuletzt ergänzen
            $path = $prefix;
            if (1 == $this->add_trailing_slash) {
                $path.= '/';
            }
            $route_params = array();
            $route_params['_locale'] = $clang->getCode();
            $route_params['_clang_id'] = $clang->getId();
            $route_params['_article_id'] = \rex_article::getSiteStartArticleId();
            $route_name = 'article_'.$route_params['_article_id'].'_'.$article['clang_id'];
            $route = new Route($path, $route_params);
            $routes->add($route_name, $route);

            // not_found per language
            $route_params = array();
            $route_params['_locale'] = $clang->getCode(); 
            $route_params['_clang_id'] = $clang->getId();
            $route_params['_article_id'] = \rex_article::getNotFoundArticleId();
            $route_name = 'not_found_'.$clang->getCode();
            $route = new Route('/'.$clang->getCode().'/{not_found_url}', $route_params, array(
                'not_found_url' => '.+',
            ));

            $routes->add($route_name, $route);
        }

        // Default language code suchen
        $default_clang_code = 'de';
        foreach($clang_list as $clang){
            if ($clang->getId() == \rex_clang::getStartId()) {
                $default_clang_code = $clang->getCode();
            }
        }

        // / und leer zuletzt ergänzen
        /* 
        $route_params = array();
        $route_params['_locale'] = $default_clang_code; 
        $route_params['_clang_id'] = \rex_clang::getStartId();
        $route_params['_article_id'] = \rex_article::getSiteStartArticleId();
        $route_name = 'default';
        $route = new Route('/', $route_params);
        $routes->add($route_name, $route);
         */

        // not_found path ergänzen
        $route_params = array();
        $route_params['_locale'] = $default_clang_code;
        $route_params['_clang_id'] = \rex_clang::getStartId();
        $route_params['_article_id'] = \rex_article::getNotFoundArticleId();
        $route_name = 'not_found';
        $route = new Route('/{url}', $route_params, array(
            'url' => '.+',
        ));
        $routes->add($route_name, $route);

        return $routes;
    }
}
