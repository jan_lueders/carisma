<?php
namespace RewriteUrl\Models;

class Notfound
{
    public $id;
    public $active = 1;
    public $not_found_url = '';
    public $article_id = 0;
    public $clang_id = 0;
    public $external = '';
    public $http_status_code = '301';


    public function setFormData($data, $container)
    {
        $fs = $container->get('FilterService');

        if (isset($data['not_found_url'])) {
            $this->not_found_url = trim($fs->filterString($data['not_found_url']));
        }
        if (isset($data['external'])) {
            $this->external = trim($fs->filterString($data['external']));
        }
        if (isset($data['article_id'])) {
            $this->article_id = intval($data['article_id']);
        }
        if (isset($data['clang_id'])) {
            $this->clang_id = intval($data['clang_id']);
        }
        if (isset($data['http_status_code'])) {
            $this->http_status_code = intval($data['http_status_code']);
        }
    }

    public function validate($container) {
        $errors = array();
        if ('' == $this->not_found_url) {
            $errors['not_found_url'] = 'Input required';
        }
        if ('' == $this->external) {
            $errors['external'] = 'Input required';
        }

        return $errors;
    }
}
