<?php
namespace RewriteUrl\Models;

class RewriteUrlConfig
{

    public $rewrite_type = 'off';

    public $add_default_lang = 1;

    public $add_trailing_slash = 0;


    public function setFormData($data, $container) {
        $fs = $container->get('FilterService');

        if (isset($data['rewrite_type'])) {
            $this->rewrite_type = $fs->filterString($data['rewrite_type']);
        }
        if (isset($data['add_default_lang'])) {
            $this->add_default_lang = 1;
        } else {
            $this->add_default_lang = 0;
        }
        if (isset($data['add_trailing_slash'])) {
            $this->add_trailing_slash = 1;
        } else {
            $this->add_trailing_slash = 0;
        }
        if (isset($data['alternate_article_name'])) {
            $this->alternate_article_name = $fs->filterString($data['alternate_article_name']);
        }
        if (isset($data['alternate_catname'])) {
            $this->alternate_catname = $fs->filterString($data['alternate_catname']);
        }
    }

    public function validate($container) 
    {
        $errors = array();

        return $errors;
    }

    public function hydrateFromConfig() 
    {
        $this->rewrite_type = \rex_config::get('rewrite_url', 'rewrite_type');
        $this->add_default_lang    = \rex_config::get('rewrite_url', 'add_default_lang');
        $this->add_trailing_slash    = \rex_config::get('rewrite_url', 'add_trailing_slash');
    }
    
    public function updateConfig() 
    {
        \rex_config::set('rewrite_url', 'rewrite_type', $this->rewrite_type);
        \rex_config::set('rewrite_url', 'add_default_lang', $this->add_default_lang);
        \rex_config::set('rewrite_url', 'add_trailing_slash', $this->add_trailing_slash);
    }
}
