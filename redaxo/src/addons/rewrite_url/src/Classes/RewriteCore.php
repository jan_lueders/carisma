<?php
namespace RewriteUrl\Classes;

use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;


class RewriteCore
{
    public $container;
    public $add_trailing_slash = 0;
    public $add_default_lang   = 0;
    public $current_url_parameters = array();

    public function __construct($container)
    {
        $this->container = $container;
        $this->add_trailing_slash  = \rex_config::get('rewrite_url', 'add_trailing_slash');
        $this->add_default_lang    = \rex_config::get('rewrite_url', 'add_default_lang');
    }

    public function getCurrentRouteParameters(){
        return $this->current_url_parameters;
    }

    /* 
     * The rewrite-function receives a params array including 
     * id and clang
     * Must return the path 
     */
    public function rewrite($params) 
    {
        
        if (!isset($params['id'])) {
            $params['id'] = \rex_article::getSiteStartArticleId();
        }
        if (!isset($params['clang'])) {
            $params['clang'] = \rex_clang::getStartId();
        }
        $folder = str_replace('/redaxo/index.php', '', $_SERVER['SCRIPT_NAME']);
        $folder = str_replace('/index.php', '', $folder);
        $routes = $this->getRouteCollection();
        $context = new RequestContext('');
        $generator = new UrlGenerator($routes, $context);
        if (isset($params['params']['rex_media_file'])){ 
            $route_name = 'rex_media_file';
            $path = $generator->generate($route_name, $params['params']);
            return $folder.$path;
        }
        // Erst mit sub_route probieren
        $route_name = 'article_sub_route_'.$params['id'].'_'.$params['clang'];
        if (null !== $routes->get($route_name)) {
            try {
                $path = $generator->generate($route_name, $params['params']);
                return $folder.$path;
            } catch(MissingMandatoryParametersException $e) {
            }
        }

        // Mit Artikel-Route probieren
        $route_name = 'article_'.$params['id'].'_'.$params['clang'];
        if (null !== $routes->get($route_name)) {
            try {
                $path = $generator->generate($route_name, $params['params']);
                return $folder.$path;
            } catch(MissingMandatoryParametersException $e) {
            }
        }

        return 'RewriteCore_can_not_create_url';
    }

    public function unrewrite() {
        $path = '';
        if (false === strpos($_SERVER['REQUEST_URI'], 'index.php')) {
            $folder = str_replace('/redaxo/index.php', '', $_SERVER['SCRIPT_NAME']);
            $folder = str_replace('/index.php', '', $folder);
            $parts = explode('?',  $_SERVER['REQUEST_URI']);
            $path = strtolower(str_replace($folder, '', $parts[0]));
            // remove trailing slashes 
            $path_no_trailing = '/'. trim($path, '/');
            if ('/redaxo' == $path_no_trailing) {
                return;
            }
            // Umleiten auf default-Sprache
            // NO! makes ajax not to work
            /*
            if ('/' == $path && 1 == $this->add_default_lang) { 
                $url = rex_getUrl(\rex_article::getSiteStartArticleId(), \rex_clang::getStartId());
                header("Location: ".$url);
                die();
        */

            // Umleiten auf URL ohne End-Slash
            if ('/' != $path && $path != $path_no_trailing && 0 == $this->add_trailing_slash) {
                header("Location: ".$folder.$path_no_trailing);
                die();
            } 

            // Umleiten auf URL mit End-Slash
            if ('/' != $path && $path == $path_no_trailing && 1 == $this->add_trailing_slash) {
                header("Location: ".$folder.$path_no_trailing.'/');
                die();
            }

            $context = new RequestContext('');
            $matcher = new UrlMatcher($this->getRouteCollection(), $context);
            try { 
                $parameters = $matcher->match($path);
                if (is_array($parameters)) {
                    $this->current_url_parameters = $parameters; 
                    \rex_clang::setCurrentId($parameters['_clang_id']);
                    \rex_addon::get('structure')->setProperty('article_id', $parameters['_article_id']);
                    // GET-ARRAY ergänzen
                    foreach($parameters as $key=> $val) {
                        if (0 === strpos($key, '_')) {
                            continue;
                        }
                        $_GET[$key] = $val;
                    }
                    if ('rex_media_file' == $parameters['_route']) {
                        // Mediamanager triggern (wurde scho früher ausgeführt)
                        \rex_media_manager::init();
                    } elseif (false !== strpos($parameters['_route'], 'not_found')) {
                        // Prüfen ob verfügbare Weiterleitung
                        $this->redirectIfNotFound();
                    }
                    return;
                }
            }catch(\Symfony\Component\Routing\Exception\ResourceNotFoundException $e) {
                // Prüfen ob verfügbare Weiterleitung
                $this->redirectIfNotFound();
                \rex_addon::get('structure')->setProperty('article_id', \rex_article::getNotfoundArticleId());
                \rex_clang::setCurrentId(\rex_clang::getStartId());
            }
        }
    }

    public function getRouteCollection($force_recreate = false){
        $file = \rex_path::addonCache('rewrite_url', 'map.php');
        $map = array();
        if (!$force_recreate && file_exists($file) ){
            $c = file_get_contents($file);
            $map = unserialize($c);
            if( false !== $map ){
                return $map;
            }
        }
        $dir = \rex_path::addonCache('rewrite_url', '');
        if(!is_dir($dir)){
            mkdir($dir, 0777, true);
        }
        $map = $this->buildRouteCollection();
        file_put_contents($file, serialize($map));
        return $map;
    }

    public function getMap($force_recreate = false) {
        $route_collection = $this->getRouteCollection($force_recreate);
        $a = array();
        foreach($route_collection as $route){
            $a[] = array(
                'path' => $route->getPath(),
            );
        }
        return $a;
    }

    /* Diese Funktion muss überschrieben werden*/
    public function buildRouteCollection(){
        $routes = new RouteCollection();
        $route_params = array();
        $route_params['_locale'] = '';
        $route_params['_clang_id'] = 1;
        $route_params['_article_id'] = 1;
        $route_params['rex_media_type'] = 'original';
        $route_name = 'rex_media_file';
        $route = new Route('/media_file/{rex_media_type}/{rex_media_file}', $route_params, array(
        ));
        $routes->add($route_name, $route);
        return $routes;
    }

    public function redirectIfNotFound(){
        $folder = str_replace('/redaxo/index.php', '', $_SERVER['SCRIPT_NAME']);
        $folder = str_replace('/index.php', '', $folder);
        $url = $folder.$_SERVER['REQUEST_URI'];
        $db = \rex_sql::factory();
        $data = $db->getDBArray("SELECT * FROM ".\rex::getTable('rewrite_url_notfound').
            " WHERE not_found_url LIKE ?", array('%'.$url), \PDO::FETCH_ASSOC);
        if( is_array($data)) {
            foreach ($data as $row) {
                header("Location: ". $row['external'], $row['http_status_code']);
                die();
            }
        }
    }
}

