<?php

class rewrite_url_path_provider extends rex_path_default_provider
{
    public $folder = '';

    public function __construct($htdocs, $backend, $provideAbsolutes)
    {
        global $REX;
        parent::__construct($htdocs, $backend, $provideAbsolutes);
        $folder = str_replace('/redaxo/index.php', '', $_SERVER['SCRIPT_NAME']);
        $folder = str_replace('/index.php', '', $folder);
        $this->backend = $folder.'/redaxo/';
        $this->base = $folder.'/';
    }
    
}

