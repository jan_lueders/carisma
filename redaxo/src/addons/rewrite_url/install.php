<?php
/**
 * Urite Addon.
 *
 * @author Adrian Kühnis
 *
 * @package redaxo\rewrite_url
 *
 */

if(null === rex_config::get('rewrite_url', 'rewrite_type')){
    rex_config::set('rewrite_url', 'rewrite_type', 'off');
}

if(null === rex_config::get('rewrite_url', 'add_default_lang')){
    rex_config::set('rewrite_url', 'add_default_lang', 1);
}
if(null === rex_config::get('rewrite_url', 'add_trailing_slash')){
    rex_config::set('rewrite_url', 'add_trailing_slash', 0);
}

$sql = rex_sql::factory();

$sql->setQuery('CREATE TABLE IF NOT EXISTS `'.rex::getTable('rewrite_url_notfound')."` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `active` tinyint(1) NOT NULL DEFAULT 1,
    `not_found_url` varchar(255) NOT NULL DEFAULT '',
    `article_id` int(11) NOT NULL DEFAULT 0,
    `clang_id` int(11) NOT NULL DEFAULT 0,
    `external` varchar(255) NOT NULL DEFAULT '',
    `http_status_code` varchar(3) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

rex_delete_cache();

