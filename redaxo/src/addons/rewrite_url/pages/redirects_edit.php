<?php
$container = RewriteUrl\Services\ServiceContainer::getInstance();
$form = $container->get('FormService');
$errors = array();
$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
$model = $container->get('NotfoundService')->find($_GET['id']);
if (null === $model) {
    $id = 0;
    $model = new \RewriteUrl\Models\Notfound();
}
if (isset($_POST['model'])) {
    $model->setFormData($_POST['model'], $container);
    $errors = $model->validate($container);
    if (0 == count($errors)) {
        if (0 == $id) {
            $container->get('NotfoundService')->insert($model);
        }else{
            $container->get('NotfoundService')->update($model);
        }
        $url = 'index.php?page=rewrite_url/redirects';
        header("Location: $url");
        die();

    }

}

?>
<form class="form"
    action="index.php?page=rewrite_url/redirects&view=edit&id=<?php echo $id;?>"
    method="post"
>
<div class="form-group<?php if (isset($errors['not_found_url'])){echo ' has-error';}?>">
<label>Alte Url</label>
<?php echo $form->text('model[not_found_url]', $model->not_found_url, array(
    'class' => 'form-control',
    'placeholder' => 'http://domain.de/alte_url',
    'required' => 'required',
));?>
</div>

<div class="form-group<?php if (isset($errors['external'])){echo ' has-error';}?>">
<label>Neue Url</label>
<?php echo $form->text('model[external]', $model->external, array(
    'class' => 'form-control',
    'placeholder' => 'http://domain.de/neue_url',
    'required' => 'required',
));?>
</div>

<div class="form-group">
<label>Http-Statuscode für die Weiterleitung</label>
<?php echo $form->select('model[http_status_code]', array(
    '301' => 'Permanente Weiterleitung',
    '302' => 'Temporäre Weiterleitung',
    ), $model->http_status_code, array(
        'class' => 'form-control',
    ));?>
</div>
<div class="form-group">
<button class="btn btn-default btn-success" type="submit">Speichern</button>
<a href="index.php?page=rewrite_url/redirects" 
    class="btn btn-default" 
    >Abbrechen</a>
</form>
