<?php
$container = RewriteUrl\Services\ServiceContainer::getInstance();
$form = $container->get('FormService');
$model = new RewriteUrl\Models\RewriteUrlConfig();
$model->hydrateFromConfig();

$success = '';
$error   = '';

if(isset($_POST['urite'])){
    $model->setFormData($_POST['urite'], $container);
    $errors = $model->validate($container);
    if (0 == count($errors)) {
        $model->updateConfig();
        $success = 'Änderungen gespeichert';
        // Map neu erstellen
        if ('off' != \rex_config::get('rewrite_url', 'rewrite_type')) {
            $service = $container->get(\rex_config::get('rewrite_url', 'rewrite_type'));
            $service->getMap(true);
        }
    }
}
if (isset($_GET['action']) && 'create_htaccess' == $_GET['action']) {
    if( file_exists(rex_path::base('.htaccess')) ){
        $res = rename(rex_path::base('.htaccess'), rex_path::base('htaccess_bak_'. date('Y-m-d_H-i-s'). '.txt'));
    }
    $res = copy(__DIR__ .'/../views/htaccess.txt', rex_path::base('.htaccess'));
    if($res) {
        $success = '.htaccess-Datei angelegt';
    }else{
        $error = '.htaccess-Datei konnte nicht angelegt werden';
    }
}

// Ausgabe
if('' != $success):?>
    <div class="alert alert-success"><?php echo $success;?></div>
<?php endif;?>
<?php if('' != $error):?>
    <div class="alert alert-danger"><?php echo $error;?></div>
<?php endif;?>

<form action="index.php?page=rewrite_url/index" method="post">
<section class="rex-page-section">

    <div class="panel panel-edit">
        <header class="panel-heading"><div class="panel-title">
        Einstellungen 
        </div></header>
    <div class="panel-body">

    <div class="form-group">
    <label>Rewrite Aktivieren</label>
    <?php echo $form->select('urite[rewrite_type]', array(
        'off' => 'Deaktiviert',
        'RewriteNumbers' => '/1-0-Artikelname',
        'RewriteNames'   => '/kategorie/unterkategorie/artikel',
        ), $model->rewrite_type, array(
            'class' => 'form-control',
        ));?>
    </div>

    <div class="checkbox">
        <label>
            <?php 
            echo $form->checkbox('urite[add_default_lang]', 
                $model->add_default_lang, array());
           ?> Default-Sprache in URL</label>
    </div>

    <div class="checkbox">
        <label>
            <?php 
            echo $form->checkbox('urite[add_trailing_slash]', 
                $model->add_trailing_slash, array());
           ?> URL mit Slash (&quot;/&quot;) abschliessen</label>
    </div>

    <div class="form-group">
        <button 
            class="btn btn-default"
            type="submit"
            >Speichern</button>
    <div>
    </section>
</form>

<section class="rex-page-section">
    <div class="panel panel-edit">
        <header class="panel-heading"><div class="panel-title">
        .htaccess-Datei  
        </div></header>
    <div class="panel-body">
<p>Speichern Sie den folgenden Inhalt in einer .htaccess-Datei im Root der Redaxo-Installation.
Oder klicken Sie hier: <a 
    href="index.php?page=rewrite_url/index&action=create_htaccess"
    class="btn btn-info"
>.htaccess-Datei Erstellen</a>
</p>
    <div class="form-group">
<textarea class="form-control" style="width:100%;" rows="50">
<?php include __DIR__ .'/../views/htaccess.txt'; ?>
</textarea>
</div>
</div>
</section>

