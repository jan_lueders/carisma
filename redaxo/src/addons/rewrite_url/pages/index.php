<?php
echo rex_view::title('Rewrite URL');
if ( 'rewrite_url/index' == $_GET['page'] ) {
    include 'settings.php';
}elseif ( 'rewrite_url/redirects' == $_GET['page'] ) {
    if (isset($_GET['view']) && 'edit' == $_GET['view']) {
        include 'redirects_edit.php';
    } else {
        include 'redirects.php';
    }
}else {
    include 'settings.php';
}

