<?php
$container = RewriteUrl\Services\ServiceContainer::getInstance();
$success = '';
if (isset($_GET['remove'])) {
    $container->get('NotfoundService')->remove($_GET['remove']);
}

$data = $container->get('NotfoundService')->findAll();
// Ausgabe
?>
<div id="redirects">
<div class="alert alert-info">Erfassen Sie hier veraltete Urls (z.B. via Google Webmaster Tools gemeldet) und das neue Ziel.</div>
<div class="col-xs-12">
<a href="index.php?page=rewrite_url/redirects&view=edit&id=0"
    class="btn btn-default pull-right"
    >Hinzufügen</a>
</div>
<table class="table table-condensed">
<thead>
    <tr>
        <th>Alte Url</th>
        <th>Neues Ziel</th>
        <th>Http-Statuscode</th>
        <th></th>
    </tr>
</thead>
<tbody>

<?php foreach ($data as $row) :?>
<tr>
    <td><?php echo $row->not_found_url;?></th>
    <td>
            <?php echo $row->external;?>
    </td>
    <td><?php echo $row->http_status_code;?></th>
    <td>
        <a 
            class="btn btn-default btn-xs"
                href="index.php?page=rewrite_url/redirects&view=edit&id=<?php echo $row->id;?>"
            >Ändern</a>
        <a 
            class="btn btn-default btn-xs"
            href="index.php?page=rewrite_url/redirects&remove=<?php echo $row->id;?>"
            onclick="return confirm('löschen?');"
            >Löschen</a>
    </td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
</div>
