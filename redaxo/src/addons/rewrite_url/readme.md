#Redaxo5-Addon rewrite_url 

##URL Rewriting Modi
+ Nummern: /{article_id}-{clang_id}-name

+ Namen:   /{catname}/{subcatname}[/{articlename}]

##URL Rewrite Optionen
+ Mit oder ohne End-Slash

+ Default-Sprache in URL anzeigen/nicht anzeigen

##Sub-Routing
Sub-Routing ermöglicht es, eine Seiten-URL mit zusätzlichen Parametern zu erweitern. 

Normale URL zum Artikel: 
```
http://domain.ch/meineseite
```

Erweiterte URL mit Sub-Routing:
```
http://domain.ch/meineseite/xxx/yyy
```

Sub-Route-Konfiguration: 
```
/{param1}/{param2}
```


###Vorbereitung
Um Sub-Routing nutzen zu können erstellen Sie ein zusätzliches Meta-Feld mit dem Namen 'art_sub_route', wie folgt:

+ Addons > Meta-Infos > Artikel
+ [+] Feld erstellen
+ Spaltenname: sub_route (wird automatisch zu art_sub_route)
+ Feldtyp: Text
+ Feldbezeichnung: Sub-Route

###Sub-Route definieren
Definieren Sie die gewünschte Sub-URL eines Artikels wie folgt:

Struktur > ... > Artikel > Meta-Daten > Sub-Route

Eingabe-Beispiel 
```
/{param1}/{param2}
```

###URL erzeugen
Für obiges Beispiel lässt sich die URL mit Redaxo-eigenen Funktion erstellen:

```
<?php $url = rex_getUrl($article_id, $clang_id, array(
    'param1' => '...',
    'param2' => '...',
    ));?>
```

###URL-Parameter auslesen
Die Subrouting-Parameter werden in den $_GET-Array geschrieben. Sie können einfach aus dem $_GET-Array ausgelesen werden.

```
<?php if(isset($_GET['param1'])) {
    ...
}?>
```


###Rewrite von URLs für Medien (z.B. Bilder)

Dieses Addon ermöglicht schöne URL's im Zusammenhang mit dem Addon Media-Manager.

Normale URL des Media-Manager: 

```
index.php?rex_media_type=ImgTypeName&rex_media_file=ImageFileName
```

Erstellen der URL mittels rex_getUrl() (dies funktioniert auch bei deaktiviertem Rewrite-Addon):
```
<?php $url = rex_getUrl(null, null, array(
    'rex_media_type' => 'ImgTypeName',
    'rex_media_file' => 'ImageFileName',
    ));

// Erzeugt diese URL:
// /media_file/ImgTypeName/ImageFileName
?>
```

Um die Originaldatei zu laden kann man den Parameter ImageTypeName weglassen oder 'original' eingeben.



