<?php
class rex_global_settings {
	protected static $globalValues;
	protected static $curClangId;
	protected static $defaultClang;

	const FIELD_PREFIX = 'glob_';

	public static function init() {
		$sql = rex_sql::factory();
		$sql->setQuery('SELECT * FROM '. rex::getTablePrefix() . 'global_settings');

		self::$globalValues = $sql;
		self::$curClangId = rex_clang::getCurrentId();
		self::$defaultClang = rex_clang::getStartId();
	}

	public static function getDefaultValue($field, $allowEmpty = true) {
		return self::getValue($field, self::$defaultClang, $allowEmpty);
	}

	public static function getValue($field, $clangId = null, $allowEmpty = true) {
		if ($clangId == null) {
			$clangId = self::$curClangId;
		}

		$field = self::FIELD_PREFIX . self::getStrippedField($field);

		self::$globalValues->reset();

		if (!self::$globalValues->hasValue($field)) {
			return self::getEmptyFieldOutput($field, '', $allowEmpty);
		}

		for ($i = 0; $i < self::$globalValues->getRows(); $i++) {
			if (self::$globalValues->getValue('clang') == $clangId) {
				return self::getEmptyFieldOutput($field, self::$globalValues->getValue($field), $allowEmpty);
			}

			self::$globalValues->next();
		}

		return self::getEmptyFieldOutput($field, '', $allowEmpty);
	}

	public static function getDefaultString($field, $allowEmpty = false) {
		return self::getDefaultValue($field, $allowEmpty);
	}

	public static function getString($field, $clangId = null, $allowEmpty = false) {
		return  self::getValue($field, $clangId, $allowEmpty);
	}

	protected static function getEmptyFieldOutput($field, $value, $allowEmpty) {
		if (!$allowEmpty && $value == '') {
			return '{{ ' . self::getStrippedField($field) . ' }}';
		} else {
			return $value;
		}
	}

	public static function getStrippedField($field) {
		if (strpos($field, self::FIELD_PREFIX) === 0) {
		    $field = substr($field, strlen(self::FIELD_PREFIX));
        }

		return $field;
	}
}

