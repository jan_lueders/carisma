Globale Einstellungen - Changelog
=================================

### Version 2.0.0 - 15. März 2017

* Portierung zu REDAXO 5
* Neuer Feldtyp: Tab, dadurch kann man das AddOn auch als String Table oder Sprog ersatz benutzen. Die Feldbezeichnung können auch leer gelassen werden, dann wird direkt der Feldname (Spaltenname) dem Enduser angezeigt.
* Neuer Feldtyp: Colorpicker (siehe Readme für Hinweise)
* Der `glob_` Prefix ist jetzt optional. Aufruf sollte so erfolgen: `rex_global_settings::getValue('my_field');`. Beim Feldanlegen sollte ebenfalls kein `glob_` benutzt werden.
* Hinzugefügt: `rex_global_settings::getString()` und `rex_global_settings::getDefaultString()`. Wie `getValue()` nur dass standardmäßig ein Platzhalter angezeigt wird wenn Ausgabe leer ist.
* Ein dritter Parameter `$allowEmpty` für `getValue()` und `getString()` wurde hinzugefügt der steuert ob ein Platzhalter angezeigt wird wenn Feld leer oder nicht da. `getValue()` Standard: nicht anzeigen, `getString()` Standard: anzeigen

### Version 1.1.0 - 01. März 2016

* Fixed #10: Checkboxen gingen nicht, specialthx@Sysix
* Fixed #11: Wenn Feld nicht vorhanden war gab es eine Fehlermeldung, specialthx@Sysix
* Kategorie-Checkbox entfernt, da keine Funktion
* Fixed: Database down Problem wenn REDAXO Setup gestartet wurde
* Fixed #8: Felder wurden nicht korrekt ausgelesen unter PHP 5.3

### Version 1.0.1 - 20. August 2015

* Englische Backend Übersetzung hinzugefügt

### Version 1.0.0 - 11. August 2015

