<?php
header('Access-Control-Allow-Origin: *');

use PHPMailer\PHPMailer\PHPMailer;

$matchArray = [
  "fahrzeug",
  "PKW",
  "Krad",
  "Traktor",
  "LKW",
  "Wohnmobil",
  "Bus",
];
$replaceArray = [
  "fahrzeug" => "PKW",
  "PKW" => "PKW",
  "Krad" => "Motorräder",
  "Traktor" => "Traktoren",
  "LKW" => "LKW",
  "Wohnmobil" => "Wohnmobile",
  "Bus" => "Busse",
];
$urlConfig = parse_ini_file("config.ini");
$command = $_REQUEST["command"];
$tarId = 1;
$result = jsonLoginCurlRequest($urlConfig);
$key = $result->sessiontoken;
error_log("====REQUEST-COMMAND=== ".$command." : ".json_encode($_REQUEST));
switch ($command) {
	case "login":
		$tariflist = getTarriflist($urlConfig, $key);
		$jsonTarList = json_decode($tariflist);
		$tarId = $jsonTarList->result[0]->ID;
		$details = getKVSDetailList($urlConfig, $key, $tarId);
		$content = "<option value='0'></option>";
		$contentArray = [
		  "json" => $details,
		];
		foreach ($details->result as $index => $obj) {
			$name = convertString(htmlspecialchars_decode($obj->name));
			if ($obj->ID == 109001) {
				$abstellplatz = $name;
			}
			if ($obj->ID > 202000) {

				$content .= "<option value='" . $obj->ID . "'>" . $name . "</option>";
			}
		}
		$contentArray["html"] = $content;
		echo json_encode($contentArray);
		break;
	case "group":
		$group = explode(",", $_REQUEST["id"]);
		$vehGroup = getVehGroup($urlConfig, $key, $tarId, $group[0]);
		$content = "<option value='0'></option>";
		$contentArray = [
		  "json" => $vehGroup,
		];
		foreach ($vehGroup->result as $index => $obj) {
			$content .= "<option value='" . convertString($obj) . "'>" . convertString($obj) . "</option>";
		}
		$contentArray["html"] = $content;
		echo json_encode($contentArray);
		break;
	case "vehType":
		$manufacturer = $_REQUEST["vehType"];
		$group = $_REQUEST["vehGroupId"];
		$year = $_REQUEST["id"];
		$type = getVehType($urlConfig, $key, $tarId, $manufacturer, $year, $group);
		$content = "<option value='0'></option>";
		$contentArray = [
		  "json" => $type,
		];
		foreach ($type->result as $index) {
			if (($index->modelSeries == "-" && $index->series == "-")) {
				$content .= "<option value='" . convertString($index->series) . "," . convertString($index->modelSeries) . "'>Alle Fahrzeugtypen</option>";
			} else {
				if ($index->modelSeries == "" || $index->modelSeries == "-") {
					$content .= "<option value='" . convertString($index->series) . "," . convertString($index->modelSeries) . "'>" . convertString($index->series) . "</option>";
				} else {
					if ($index->series == "" || $index->series == "-") {
						$content .= "<option value='" . convertString($index->series) . "," . convertString($index->modelSeries) . "'>" . convertString($index->modelSeries) . "</option>";
					} else {
						$values = convertString($index->series) . "," . convertString($index->modelSeries);
						$content .= "<option value='" . $values . "'>" . $values . "</option>";
					}
				}
			}
		}
		$contentArray["html"] = $content;
		echo json_encode($contentArray);
		break;
	case "vehList":
		$vehModel = $_REQUEST["vehModel"];
		$vehClass = $_REQUEST["vehClass"];
		$brand = $_REQUEST["brand"];
		$year = $_REQUEST["yearofconstruct"];
		$group = $_REQUEST["vehGroupId"];
		$type = getVehList($urlConfig, $key, $brand, $vehClass, $vehModel, $year, $group);

		$content = "<option value='0'></option>";
		$contentArray = [
		  "json" => $type,
		];
		foreach ($type->result as $index) {
			$bodytext = convertString($index->vehicleBody);
			$manufactor = convertString($index->manufacturer);
			$content .= "<option value='" . $index->idVehicle . "," . $manufactor . "," . convertString($index->vehicleType) . "," . $year . "," . $bodytext . "," . $index->HP . " PS (" . $index->KW . " KW)" . "'>" . $manufactor . ", " . convertString($index->vehicleType) . ", " . $index->firstYearOfConstruction . " - " . $index->LastYearOfConstruction . ", " . $bodytext . ", " . $index->HP . " PS (" . $index->KW . " KW)</option>";
		}
		$contentArray["html"] = $content;
		echo json_encode($contentArray);
		break;
	case "premiums":
		$vehId = $_REQUEST["vehId"];
		$vehValue = $_REQUEST["vehValue"];
		$premiums = getPremiums($urlConfig, $key, $vehId, $vehValue);
		$contentArray = [
		  "json" => $premiums,
		];
		foreach ($premiums->result as $index => $value) {
			if ($value->insuranceGroup == "Basic") {
				$contentArray["basic"] = $value->totalGrossPremium;
			}
			if ($value->insuranceGroup == "Comfort") {
				$contentArray["comfort"] = $value->totalGrossPremium;
			}
			if ($value->insuranceGroup == "Premium") {
				$contentArray["premium"] = $value->totalGrossPremium;
			}
		}
		$contentArray["html"] = "";
		echo json_encode($contentArray);
		break;
	case "premium":
		$dataString = $_REQUEST["premiumData"];
		$deductible = array_reverse($_REQUEST["deductible"]);
		$dataString["selectedDeductibles"] = explode(",", $dataString["selbstbehalt_paket"]);
		//         error_log(join(",",$dataString["selectedDeductibles"])." === ".count($dataString["selectedDeductibles"]));
		$dataString["vehicleId"] = $_REQUEST["vehicleId"];
		$dataString["vehicleClassID"] = $_REQUEST["vehicleClassID"];
		$dataString["kilometerNeeded"] = $_REQUEST["kilometerNeeded"];
		$dataString["tarifID"] = $_REQUEST["baseTarifID"];
		$result = [];
		$headerArray = [];
		if (is_array($deductible)) {
			foreach ($deductible as $id => $value) {
				//                 if($value["name"] == "Premium"){
				//                     $dataString["optradio"] = "yes";
				//                 }
				if (count($dataString["selectedDeductibles"]) == 1 && $value["name"] == "Basic") {
					$headerArray[$value["name"]] = getPremiumVars($urlConfig, $key, $dataString, $value["value"],
					  $dataString["selectedDeductibles"]);
				} else if (count($dataString["selectedDeductibles"]) == 2 && $value["name"] == "Comfort") {

					$headerArray[$value["name"]] = getPremiumVars($urlConfig, $key, $dataString, $value["value"],
					  $dataString["selectedDeductibles"]);
				} else if (count($dataString["selectedDeductibles"]) == 3 && $value["name"] == "Premium") {

					$headerArray[$value["name"]] = getPremiumVars($urlConfig, $key, $dataString,
					  $value["value"], $dataString["selectedDeductibles"]);
				} else {
					$headerArray[$value["name"]] = getPremiumVars($urlConfig, $key, $dataString,
					  $value["value"], $value);
				}
			}

			$result = multiple_threads_request($urlConfig, $headerArray);
		} else {
			$result[$deductible[0]["name"]] = getPremium($urlConfig, $key, $dataString, $deductible[0]["deductible"]);
		}

		$contentData = [
		  "json" => $result,
		  "html" => "",
		];
		error_log("===GET-PREMIUM-RESULT=== " . json_encode($contentData));
		echo json_encode($contentData);
		break;
	case "deductable":
		$data = $_REQUEST["insuranceId"];
		$vehicleId = $_REQUEST["vehicleId"];
		$result = getDeductableList($urlConfig, $key, $data, $vehicleId);
		$contentData = [
		  "json" => $result,
		  "html" => "",
		];
		echo json_encode($contentData);
		break;
	case "getInputData":
		$sessionkey = $_REQUEST["sessionkey"];
		$username = $_REQUEST["username"];
		$result = getUserInputData($urlConfig, $sessionkey, $username);
		$contentData = [
		  "json" => $result,
		  "html" => "",
		];
		echo json_encode($contentData);
		break;
	case "final":
		$userSessionKey = $_REQUEST["storageData"]["usersession"];
		$bankData = [];
		$responseArray = [];
		$bankData[] = $_REQUEST["storageData"]["Iban"];
		$bankData[] = $_REQUEST["storageData"]["BIC"];
		$bankData[] = $_REQUEST["storageData"]["nameofbank"];
		$bankData[] = $_REQUEST["storageData"]["Bank-Prename"] . " " . $_REQUEST["storageData"]["Bank-Lastname"];
		$username = $_REQUEST["storageData"]["username"];
		$result = saveBankData($urlConfig, $userSessionKey, $bankData, $username);

		echo json_encode($result);
		break;
	case "saveCalc":
		$monthArray = array("JAN" => "01","FEB"=> "02","MAR"=>"03","APR"=>"04","MAY"=>"05","JUN"=>"06","JUL"=>"07","AUG"=>"08","SEP"=>"09","OCT"=>"10","NOV"=>"11","DEC"=>"12");
		$userSessionKey = $_REQUEST["storageData"]["usersession"];
		$fahrzeugdaten = json_decode($_REQUEST["storageData"]["Fahrzeugdaten2"]);
		$sel5 = json_decode($_REQUEST["storageData"]["#sel5"]);
		$initialRegistration = $fahrzeugdaten->erstzulassung_tag.".".$monthArray[$fahrzeugdaten->erstzulassung_monat] . "." . $fahrzeugdaten->erstzulassung;
		$premium = json_decode($_REQUEST["storageData"]["premium"]);
		$tarif = $_REQUEST["storageData"]["tarif"];
		if ($tarif == "Komfort") {
			$tarif = "Comfort";
		} else {
			if ($tarif == "Basis") {
				$tarif = "Basic";
			}
		}
		$carData = explode(",", $_REQUEST["storageData"]["insuranceID"]);
		$contractData = [];
		$contractData[] = $_REQUEST["storageData"]["username"];
		$contractData[] = $premium->json->$tarif->result[0]->calculationID;
		$contractData[] = $carData[3];
		$contractData[] = $carData[1];
		$contractData[] = $initialRegistration;
		$contractId = saveCalculation($urlConfig, $userSessionKey, $contractData, $sel5, $fahrzeugdaten, $carData[0],
		  $initialRegistration);
		echo json_encode($contractId);
		break;
	case "getEvb":
		$userSessionKey = $_REQUEST["storageData"]["usersession"];
		$username = $_REQUEST["storageData"]["username"];
		$fahrzeugdaten = json_decode($_REQUEST["storageData"]["Fahrzeugdaten2"]);
		$premium = json_decode($_REQUEST["storageData"]["premium"]);
		$tarif = $_REQUEST["storageData"]["tarif"];
		$carData = explode(",", $_REQUEST["storageData"]["insuranceID"]);
		$contractID = $_REQUEST["contractID"];
		$evbData = getEVBNumber($urlConfig, $userSessionKey, $_REQUEST, $fahrzeugdaten, $carData[0], $contractID,
		  $username);
		echo json_encode($evbData);
		break;
	case "getCalc":
		$username = $_REQUEST["username"];
		$contractId = $_REQUEST["contractId"];
		$sessionId = $_REQUEST["sessionId"];
		$calcData = getCalculation($urlConfig, $sessionId, $username, $contractId);

		echo json_encode($calcData);
		break;
	case "saveOffer":
		$username = $_REQUEST["username"];
		$contractId = $_REQUEST["contractID"];
		$sessionId = $_REQUEST["sessionId"];
		$yearOfConstruct = $_REQUEST["yearOfConstruct"];
		$initialRegistration = $_REQUEST["initialRegistration"];
		$mileage = $_REQUEST["mileage"];
		$intendedAdmission = $_REQUEST["intendedAdmission"];
		$tacho_einheit = $_REQUEST["mileMetricID"];
		$vehDataArray = explode(",", $_REQUEST["vehData"]);
		$personalData = $_REQUEST["personalData"];
		$saveOfferData = saveOffer($urlConfig, $sessionId, $username, $contractId, $yearOfConstruct,
		  $initialRegistration, $tacho_einheit, $mileage, $intendedAdmission, $vehDataArray, $personalData);
		echo json_encode($saveOfferData);
		break;
	case "contForm":
		$empfaenger = $urlConfig["emailaddress"];
		$fromMail = $_REQUEST["fromMail"];
		$fromName = $_REQUEST["fromName"];
		$betreff = "";
		$body = $_REQUEST["messageBody"];

		sendPHPMail($empfaenger, $fromMail, $fromName, $body, $urlConfig);
		break;
	case "checkIban":
		$iban = $_REQUEST["iban"];
		echo checkIban($iban);
		break;
}

function getUserInputData($urlConfig, $sessionkey, $username)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"] . "/get/inputData/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "username: " . $username;
	$headers[] = "sessiontoken: " . $sessionkey;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}

	curl_close($ch);

	$result2 = json_decode($result);

	return $result2;
}

function getCity($urlConfig, $zipcode)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"] . "/get/city/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "zipCode" . $zipcode;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}

	curl_close($ch);

	$cityArray = [];
	foreach ($result->cities as $index => $city) {
		$cityArray . push(["label" => $city, "value" => $city]);
	}

	return json_encode($cityArray);
}

function jsonLoginCurlRequest($urlConfig)
{
	$curlConfig = parse_ini_file("config.ini");
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/login/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "username: " . $curlConfig["apiuser"];
	$headers[] = "password: " . $curlConfig["apipass"];
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = json_decode(curl_exec($ch));
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);

	return $result;
}

function getTarriflist($urlConfig, $sessionkey)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/tariffList/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $sessionkey;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

function getKVSDetailList($urlConfig, $sessionkey, $tarId)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/KVSDetailList/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $sessionkey;
	$headers[] = "Tarid: " . $tarId;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = json_decode(curl_exec($ch));
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);

	return $result;
}

function getVehGroup($urlConfig, $sessionkey, $tarId, $group)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/manufactureList/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $sessionkey;
	$headers[] = "Tarid: " . $tarId;
	$headers[] = "Vehgroup: " . $group;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = json_decode(curl_exec($ch));

	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);

	return $result;
}

function getVehType($urlConfig, $sessionkey, $tarId, $manufactor, $year, $group)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/typeList/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $sessionkey;
	$headers[] = "Tarid: " . $tarId;
	$headers[] = "Manufacturer: " . utf8_decode(convertString($manufactor));
	$headers[] = "Yearofconstruction: " . $year;
	$headers[] = "Vehgroup: " . $group;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$result = json_decode(curl_exec($ch));
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);

	return $result;
}

function getVehList($urlConfig, $key, $brand, $vehClass, $vehModel, $year, $group)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/vehicleList/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "Tarid: 1";
	$headers[] = "Manufacturer: " . utf8_decode(convertString($brand));
	$headers[] = "Vehgroup: " . $group;
	if ($vehClass != "-") {
		$headers[] = "Series: " . utf8_decode(convertString($vehClass));
	} else {
		$headers[] = "Series: ";
	}

	if ($vehModel != "-") {
		$headers[] = "Modelseries: " . utf8_decode(convertString($vehModel));
	} else {
		$headers[] = "Modelseries: ";
	}
	$headers[] = "Yearofconstruction: " . $year;

	error_log("===VEHLIST=== " . join(",", $headers));
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$result = json_decode(curl_exec($ch));
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);

	return $result;
}

function getPremiums($urlConfig, $key, $vehId, $vehValue)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/premiums/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "Tarid: 1";
	$headers[] = "Vehicleid: " . $vehId;
	$headers[] = "Vehiclevalue: " . $vehValue;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	error_log("===GET-PREMIUMS===" . join(",", $headers));

	$result = json_decode(curl_exec($ch));
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

function getDeductableList($urlConfig, $key, $insuranceID, $vehicleId)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/deductibleList/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "Tarid: 1";
	$headers[] = "Insurancegroupid: " . $insuranceID;
	$headers[] = "Vehicleid: " . $vehicleId;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = json_decode(curl_exec($ch));
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

function getPremium(
  $urlConfig,
  $key,
  $dataString,
  $tarifId,
  $deductible,
  $vehicleId,
  $vehicleClassID,
  $kilometerNeeded,
  $baseTarifID
) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/premium/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$deductibleIds = $deductible;

	$deductibles = explode(",", $dataString["selbstbehalt_paket"]);
	//     error_log($dataString["selbstbehalt_paket"]);
	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "Tarid: 1";
	$headers[] = "Insurancegroupid: " . $tarifId;
	$headers[] = "Vehicleid: " . $vehicleId;
	$headers[] = "vehicleValue: " . str_replace(".", "", $dataString["marktwert"]);
	$headers[] = "numberPlateTypeID: " . $dataString["kennzeichen"];

	if ($dataString["kennzeichen"] == "105003" || $dataString["kennzeichen"] == "105012") {
		if ($dataString["saison_start"] < 10) {
			$headers[] = "monthFromID:1180" . $dataString["saison_start"];
		} else {
			$headers[] = "monthFromID:1180" . $dataString["saison_start"];
		}
		if ($dataString["saison_ende"] < 10) {
			$headers[] = "monthToID:1190" . $dataString["saison_ende"];
		} else {
			$headers[] = "monthToID:1190" . $dataString["saison_ende"];
		}
	}
	$headers[] = "intendedUseID: 106001";
	if (count($deductibles) == 1 || !is_array($deductibles)) {
		$headers[] = "deductiblePartID: " . $deductibles[0];
	} else {
		if (count($deductibles) == 2) {
			$headers[] = "deductiblePartID: " . $deductibles[0];
			$headers[] = "deductibleFullID: " . $deductibles[1];
		} else {
			if (count($deductibles) == 3) {
				$headers[] = "deductiblePartID: " . $deductibles[0];
				$headers[] = "deductibleFullID: " . $deductibles[1];
				$headers[] = "deductibleAllID: " . $deductibles[2];
			}
		}
	}

	if ($dataString["optradio"] == "yes") {
		$headers[] = "compensationID:117002";
	} else {
		$headers[] = "compensationID:";
	}

	if ($dataString["custom_type"] < 4) {
		$headers[] = "vehicleGradeID:12000" . $dataString["custom_type"];
	}
	$headers[] = "leasingID: " . $dataString["leasing"];
	$headers[] = "purchaseYearID: " . $dataString["kaufjahr"];
	$headers[] = "zuersID:111001";
	$headers[] = "parkingAreaID:109001";
	if ($kilometerNeeded == "yes") {
		$headers[] = "kilometrageID:" . $dataString["fahrleistung"];
	} else {
		$headers[] = "kilometrageID:";
	}
	if (array_key_exists("reusecheckbox",
		$$dataString) || $dataString["wiederherstellungswert"] > $dataString["marktwert"]) {
		$headers[] = "useRemanufactValue:1";
		$headers[] = "remanufacturingValue:" . str_replace(".", "", $dataString["wiederherstellungswert"]);
	} else {
		//$headers[] = "useRemanufactValue:0";
	}
	$headers[] = "circleOfUsersID:102001";
	$headers[] = "driverExperienceID:" . $dataString["fahreralter"];
	$headers[] = "paymentMethodID:113002";
	$headers[] = "paymentIntervalID:114001";
	$headers[] = "guaranteeSumID:116002";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$result = json_decode(curl_exec($ch));
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);

	return $result;
}

function getPremiumVars($urlConfig, $key, $dataString, $tarifId, $deductibles)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $urlConfig["smartcalc"] . "/get/premium/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "Tarid: 1";
	$headers[] = "Insurancegroupid: " . $tarifId;
	$headers[] = "Vehicleid: " . $dataString["vehicleId"];
	$headers[] = "vehicleValue: " . str_replace(".", "", $dataString["marktwert"]);
	$headers[] = "numberPlateTypeID: " . $dataString["kennzeichen"];

	if ($dataString["kennzeichen"] == "105003" || $dataString["kennzeichen"] == "105012") {
		if ($dataString["saison_start"] < 10) {
			$headers[] = "monthFromID:1180" . $dataString["saison_start"];
		} else {
			$headers[] = "monthFromID:1180" . $dataString["saison_start"];
		}
		if ($dataString["saison_ende"] < 10) {
			$headers[] = "monthToID:1190" . $dataString["saison_ende"];
		} else {
			$headers[] = "monthToID:1190" . $dataString["saison_ende"];
		}
	}


	$headers[] = "intendedUseID: 106001";
	if (array_key_exists("deductible", $deductibles)) {
		if (count($deductibles["deductible"]) == 1 || !is_array($deductibles)) {
			$headers[] = "deductiblePartID: " . $deductibles["deductible"][0]["ID"];
		} else if (count($deductibles["deductible"]) == 2) {
			if (intval($deductibles["deductible"][0]["ID"]) > intval($deductibles["deductible"][1]["ID"])) {
				$headers[] = "deductiblePartID: " . $deductibles["deductible"][1]["ID"];
				$headers[] = "deductibleFullID: " . $deductibles["deductible"][0]["ID"];
			} else if (intval($deductibles["deductible"][0]["ID"]) <= $deductibles["deductible"][1]["ID"]) {
				$headers[] = "deductiblePartID: " . $deductibles["deductible"][0]["ID"];
				$headers[] = "deductibleFullID: " . $deductibles["deductible"][1]["ID"];
			}
		} else if (count($deductibles["deductible"]) == 3) {
			if (intval($deductibles["deductible"][0]["ID"]) > intval($deductibles["deductible"][1]["ID"])) {
				$headers[] = "deductiblePartID: " . $deductibles["deductible"][1]["ID"];
				$headers[] = "deductibleFullID: " . $deductibles["deductible"][0]["ID"];
				$headers[] = "deductibleAllID: " . $deductibles["deductible"][2]["ID"];
			} else if (intval($deductibles["deductible"][0]["ID"]) < $deductibles["deductible"][1]["ID"]) {
				$headers[] = "deductiblePartID: " . $deductibles["deductible"][0]["ID"];
				$headers[] = "deductibleFullID: " . $deductibles["deductible"][1]["ID"];
				$headers[] = "deductibleAllID: " . $deductibles["deductible"][2]["ID"];
			} else if (intval($deductibles["deductible"][2]["ID"]) < intval($deductibles["deductible"][0]["ID"]) && intval($deductibles["deductible"][2]["ID"]) < intval($deductibles["deductible"][1]["ID"])) {
				$headers[] = "deductiblePartID: " . $deductibles["deductible"][2]["ID"];
				$headers[] = "deductibleFullID: " . $deductibles["deductible"][0]["ID"];
				$headers[] = "deductibleAllID: " . $deductibles["deductible"][1]["ID"];
			} else if (intval($deductibles["deductible"][2]["ID"]) == intval($deductibles["deductible"][0]["ID"]) && intval($deductibles["deductible"][2]["ID"]) == intval($deductibles["deductible"][1]["ID"])) {
				$headers[] = "deductiblePartID: " . $deductibles["deductible"][0]["ID"];
				$headers[] = "deductibleFullID: " . $deductibles["deductible"][1]["ID"];
				$headers[] = "deductibleAllID: " . $deductibles["deductible"][2]["ID"];
			}
		}
	} else {

		if (count($deductibles) == 1 || !is_array($deductibles)) {
			$headers[] = "deductiblePartID: " . $deductibles[0];
		} else {
			if (count($deductibles) == 2) {
				if (intval($deductibles[0]) > intval($deductibles[1])) {
					$headers[] = "deductiblePartID: " . $deductibles[1];
					$headers[] = "deductibleFullID: " . $deductibles[0];
				} else if (intval($deductibles[0]) <= $deductibles[1]) {
					$headers[] = "deductiblePartID: " . $deductibles[0];
					$headers[] = "deductibleFullID: " . $deductibles[1];
				}

			} else if (count($deductibles) == 3) {
				if (intval($deductibles[0]) > intval($deductibles[1])) {
					$headers[] = "deductiblePartID: " . $deductibles[1];
					$headers[] = "deductibleFullID: " . $deductibles[0];
					$headers[] = "deductibleAllID: " . $deductibles[2];
				} else if (intval($deductibles[0]) < $deductibles[1]) {
					$headers[] = "deductiblePartID: " . $deductibles[0];
					$headers[] = "deductibleFullID: " . $deductibles[1];
					$headers[] = "deductibleAllID: " . $deductibles[2];
				} else if (intval($deductibles[2]) < intval($deductibles[0]) && intval($deductibles[2]) < intval($deductibles[1])) {
					$headers[] = "deductiblePartID: " . $deductibles[2];
					$headers[] = "deductibleFullID: " . $deductibles[0];
					$headers[] = "deductibleAllID: " . $deductibles[1];
				} else if (intval($deductibles[2]) == intval($deductibles[0]) && intval($deductibles[2]) == intval($deductibles[1])) {
					$headers[] = "deductiblePartID: " . $deductibles[0];
					$headers[] = "deductibleFullID: " . $deductibles[1];
					$headers[] = "deductibleAllID: " . $deductibles[2];
				}
			}
		}
	}

	if ($dataString["optradio"] == "yes") {
		$headers[] = "compensationID:117002";
	} else {
		$headers[] = "compensationID:";
	}

	if ($dataString["custom_type"] < 4) {
		$headers[] = "vehicleGradeID:12000" . $dataString["custom_type"];
	}
	$headers[] = "leasingID: " . $dataString["leasing"];
	$headers[] = "purchaseYearID: " . $dataString["kaufjahr"];
	$headers[] = "zuersID:111001";
	$headers[] = "parkingAreaID:109001";
	if ($dataString["kilometerNeeded"] == "yes") {
		$headers[] = "kilometrageID:" . $dataString["fahrleistung"];
	} else {
		$headers[] = "kilometrageID:";
	}
	if (array_key_exists("reusecheckbox",
		$dataString) && $dataString["wiederherstellungswert"] > $dataString["marktwert"]) {
		$headers[] = "useRemanufactValue:1";
		$headers[] = "remanufacturingValue:" . str_replace(".", "", $dataString["wiederherstellungswert"]);
	} else {
		$headers[] = "remanufacturingValue:";
	}

	$headers[] = "circleOfUsersID:102001";
	$headers[] = "driverExperienceID:" . $dataString["fahreralter"];
	$headers[] = "paymentMethodID:113002";
	$headers[] = "paymentIntervalID:114001";
	$headers[] = "guaranteeSumID:116002";

	return $headers;
}

function saveBankData($urlConfig, $key, $bankData, $username)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"] . "/get/saveBankData/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$bankName = str_split(htmlentities($bankData[2]), 30);

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "username:" . trim($username);
	$headers[] = "iban: " . trim($bankData[0]);
	$headers[] = "bic: " . trim($bankData[1]);
	$headers[] = "bankName:" . trim(json_encode($bankName[0], JSON_UNESCAPED_SLASHES), '"');
	$headers[] = "accountHolder:" . trim(json_encode($bankData[3], JSON_UNESCAPED_SLASHES), '"');

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = json_decode(curl_exec($ch));

	error_log("=== SAVE-BANK-HEADER === ".json_encode($headers));

	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

function saveCalculation($urlConfig, $key, $calcData, $sel5, $fahrzeugdaten, $vehicleID, $initialRegistration)
{
	$kennzeichenMatchArray = [
	  105001 => "556001",
	  105002 => "556001",
	  105003 => "556002",
	  105012 => "556002",
	];

	$monthArray = [
	  "JAN" => "01",
	  "FEB" => "02",
	  "MAR" => "03",
	  "APR" => "04",
	  "MAY" => "05",
	  "JUN" => "06",
	  "JUL" => "07",
	  "AUG" => "08",
	  "SEP" => "09",
	  "OCT" => "10",
	  "NOV" => "11",
	  "DEC" => "12",
	];

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"] . "/get/saveCalculation/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$intendedAdmissionDate = $fahrzeugdaten->erstzulassung_tag.".".$monthArray[$fahrzeugdaten->erstzulassung_monat].".".$fahrzeugdaten->erstzulassung;

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "username: " . trim($calcData[0]);
	$headers[] = "calculationID:" . trim($calcData[1]);
	$headers[] = "yearOfContruction:" . trim($calcData[2]);
	$headers[] = "manufacturer:" . $calcData[3];
	$headers[] = "initialRegistration:" . trim($initialRegistration);
	$headers[] = "modelSeries:" . $sel5->json->result[0]->modelSeries;
	$headers[] = "series:" . $sel5->json->result[0]->series;
	$headers[] = "vehicleID:" . trim($vehicleID);
	$headers[] = "mileage:" . str_replace(".", "", $fahrzeugdaten->tacho_stand);
	$headers[] = "intendedAdmission:" . trim($intendedAdmissionDate);
	$headers[] = "licenseNumberTypeID:" . $kennzeichenMatchArray[$fahrzeugdaten->kennzeichen];
	if ($fahrzeugdaten->tacho_einheit == "km") {
		$headers[] = "mileageMetricID:794001";
	} else {
		$headers[] = "mileageMetricID:794002";
	}
	error_log("===SAVE-CALC===" . join(",", $headers));
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$res = curl_exec($ch);

	error_log("==SAVE-CALC-RESULT===" . $res . "\n");

	$result = json_decode($res);
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

function getCalculation($urlConfig, $key, $username, $contractID)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig['openviva'] . "/get/calculationParams/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "username: " . trim($username);
	$headers[] = "contractID:" . $contractID;

	error_log("===GET-CALC===" . join(",", $headers));

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$result = json_decode(curl_exec($ch));
	//     error_log(json_encode($result));
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

function saveOffer(
  $urlConfig,
  $key,
  $username,
  $contractID,
  $yearOfConstruct,
  $initialRegistration,
  $tacho_einheit,
  $mileage,
  $intendedAdmission,
  $vehData,
  $personalData
) {
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"] . "/get/saveOffer/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "username: " . trim($username);
	$headers[] = "contractID:" . $contractID;
	$headers[] = "yearOfContruction:" . $yearOfConstruct;
	$headers[] = "initialRegistration:" . $initialRegistration;
	if ($tacho_einheit == "km") {
		$headers[] = "mileageMetricID:794001";
	} else {
		$headers[] = "mileageMetricID:794002";
	}
	$headers[] = "mileage:" . str_replace(".", "", $mileage);
	$headers[] = "intendedAdmission:" . $intendedAdmission;
	$headers[] = "vehicleID:" . $vehData[0];

	if (array_key_exists("sameAddress", $personalData)) {
		$headers[] = "vehicleStreet:" . trim(json_encode($personalData["street"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleHouseNumber:" . $personalData["housenumber"];
		$headers[] = "vehicleCity:" . trim(json_encode($personalData["city"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleZipCode:" . $personalData["zip"];
	} else {
		$headers[] = "vehicleStreet:" . trim(json_encode($personalData["personalStreet"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleHouseNumber:" . $personalData["personalHousenumber"];
		$headers[] = "vehicleCity:" . trim(json_encode($personalData["personalcity"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleZipCode:" . $personalData["personalzip"];
	}
	error_log("===SAVE-OFFER-HEADER===" . join(",", $headers));
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$res = curl_exec($ch);

	error_log("==SAVE-OFFER-RESULT===" . $res . "\n");

	$result = json_decode($res);
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}

	//     error_log("==saveOffer== ".json_encode($result));

	curl_close($ch);
	return $result;
}

function getEVBNumber($urlConfig, $key, $evbData, $carData, $vehicleId, $contractID, $username)
{

	$kennzeichenMatchArray = [
	  105001 => "556001",
	  105002 => "556001",
	  105003 => "556002",
	  105012 => "556002",
	];
	$fahrzeugMatchArray = [
	  202001 => "557003",
	  202002 => "557002",
	  202003 => "557006",
	  202002 => "557002",
	  202004 => "557003",
	];
	$monthArray = [
	  "JAN" => "01",
	  "FEB" => "02",
	  "MAR" => "03",
	  "APR" => "04",
	  "MAY" => "05",
	  "JUN" => "06",
	  "JUL" => "07",
	  "AUG" => "08",
	  "SEP" => "09",
	  "OCT" => "10",
	  "NOV" => "11",
	  "DEC" => "12",
	];
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"] . "/get/evbNumber/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$personalData = json_decode($evbData["storageData"]["personalDataFilled"], true);

	$street = explode(" ", $personalData["street"]);
	$vehData = explode(",", $evbData["storageData"]["insuranceID"]);
	$fullDate = "";
	if ($personalData["birthyear"] == "0" || $personalData["birthyear"] == 0) {
		$personalData["birthyear"] = "00";
	}
	$fullDate = $personalData["birthday"] . "." . $monthArray[$personalData["birthmonth"]] . "." . $personalData["birthyear"];
	$intendedAdmissionDate = $carData->erstzulassung_tag.".".$carData->erstzulassung_monat.".".$carData->erstzulassung;
	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "username: " . $username;
	$headers[] = "salutation:" . $evbData["storageData"]["anredeID"];
	if ($evbData["storageData"]["anredeID"] == "236001") {
		$headers[] = "genderID:252001";
	}
	if ($evbData["storageData"]["anredeID"] == "236002") {
		$headers[] = "genderID:252002";
	} else {
		$headers[] = "genderID:";
	}

	if ($evbData["storageData"]["anredeID"] == "236001" || $evbData["storageData"]["anredeID"] == "236002") {
		$headers[] = "insurantTypeID:554001";
	} else {
		$headers[] = "insurantTypeID:554002";
	}
	$headers[] = "lastname:" . trim(json_encode($evbData["storageData"]["lastname"], JSON_UNESCAPED_SLASHES), '"');
	$headers[] = "prename:" . trim(json_encode($evbData["storageData"]["prename"]), '"');
	$headers[] = "street:" . trim(json_encode($personalData["street"], JSON_UNESCAPED_SLASHES), '"');
	$headers[] = "houseNumber:" . $personalData["housenumber"];

	if (array_key_exists("mobilenumber", $personalData)) {
		$headers[] = "mobileNumber:" . $personalData["mobilenumber"];
	}
	$headers[] = "city:" . trim(json_encode($personalData["city"], JSON_UNESCAPED_SLASHES), '"');
	$headers[] = "zipCode:" . $personalData["zip"];
	$headers[] = "birthDate:" . $fullDate;
	$headers[] = "contractID:" . $contractID;
	$headers[] = "vehicleID:" . $vehData[0];
	$headers[] = "vehicleTypeID:";//.$fahrzeugMatchArray[$evbData["storageData"]["vehicleTypeID"]];
	$headers[] = "yearOfConstruction:" . $vehData[3];
	$headers[] = "mileage:" . str_replace(".", "", $carData->tacho_stand);
	if ($carData->tacho_einheit == "km") {
		$headers[] = "mileageMetricID:794001";
	} else {
		$headers[] = "mileageMetricID:794002";
	}

	if (property_exists($carData, 'herstellerschluesselnummer') && $carData->herstellerschluesselnummer != "") {
		$headers[] = "hsn:" . $carData->herstellerschluesselnummer;
	}
	if (property_exists($carData, 'typschluesselnummer') && $carData->typschluesselnummer != "") {
		$headers[] = "tsn:" . $carData->typschluesselnummer;
	}
	if (property_exists($carData, 'fin') && $carData->typschluesselnummer != "") {
		$headers[] = "fin:" . $carData->fin;
	}
	if (property_exists($carData, 'fahrzeugkennzeichen') && $carData->typschluesselnummer != "") {
		$headers[] = "licenseNumber:" . str_replace('/$\H/i',"",str_replace("-","",$carData->fahrzeugkennzeichen));
	}
	$headers[] = "intendedAdmission:" . $intendedAdmissionDate;
	$headers[] = "licenseNumberTypeID:" . $kennzeichenMatchArray[$carData->kennzeichen];
	if ($carData->kennzeichen == "105003" || $carData->kennzeichen == "105012") {
		$headers[] = "seasonFrom:" . preg_replace("/\\b0*/", "", $carData->saison_start);
		$headers[] = "seasonTo:" . preg_replace("/\\b0*/", "", $carData->saison_ende);
	}
	if (array_key_exists("sameAddress", $personalData)) {
		$headers[] = "vehicleStreet:" . trim(json_encode($personalData["street"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleHouseNumber:" . $personalData["housenumber"];
		$headers[] = "vehicleCity:" . trim(json_encode($personalData["city"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleZipCode:" . $personalData["zip"];
	} else {
		$headers[] = "vehicleStreet:" . trim(json_encode($personalData["personalStreet"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleHouseNumber:" . $personalData["personalHousenumber"];
		$headers[] = "vehicleCity:" . trim(json_encode($personalData["personalcity"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleZipCode:" . $personalData["personalzip"];
	}
	/* HARDCODED STUFF */
	$headers[] = "vehicleStateID:232001";
	if ($carData->grund_der_versicherung == "new") {
		$headers[] = "processTypeID:553001";
	} else {
		if ($carData->grund_der_versicherung == "change") {
			$headers[] = "processTypeID:553002";
		}
	}

	$headers[] = "stateID:232001";

	error_log("===EVB===" . join(",", $headers) . "\n");

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$res = curl_exec($ch);

	error_log("==EVB-RESULT===" . $res . "\n");

	$result = json_decode($res);
	//$result = array();
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

function getEVBNumberExternal($urlConfig, $formData)
{

	$kennzeichenMatchArray = [
	  105001 => "556001",
	  105002 => "556001",
	  105003 => "556002",
	  105012 => "556002",
	];
	$fahrzeugMatchArray = [
	  202001 => "557003",
	  202002 => "557002",
	  202003 => "557006",
	  202002 => "557002",
	  202004 => "557003",
	];
	$monthArray = [
	  "JAN" => "01",
	  "FEB" => "02",
	  "MAR" => "03",
	  "APR" => "04",
	  "MAI" => "05",
	  "JUN" => "06",
	  "JUL" => "07",
	  "AUG" => "08",
	  "SEP" => "09",
	  "OCT" => "10",
	  "NOV" => "11",
	  "DEC" => "12",
	];
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"] . "/get/evbNumber/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = [];
	$headers[] = "Accept: application/json";
	$headers[] = "Sessiontoken: " . $key;
	$headers[] = "username: " . $username;
	$headers[] = "salutation:" . $formData["anredeID"];
	if ($evbData["storageData"]["anredeID"] == "236001") {
		$headers[] = "genderID:252001";
	} else {
		if ($formData["anredeID"] == "236002") {
			$headers[] = "genderID:252002";
		} else {
			$headers[] = "genderID:";
		}
	}

	if ($formData["anredeID"] == "236001" || $formData["anredeID"] == "236002") {
		$headers[] = "insurantTypeID:554001";
	} else {
		$headers[] = "insurantTypeID:554002";
	}
	$headers[] = "lastname:" . trim(json_encode($formData["lastname"], JSON_UNESCAPED_SLASHES), '"');
	$headers[] = "prename:" . trim(json_encode($formData["prename"]), '"');
	$headers[] = "street:" . trim(json_encode($formData["street"], JSON_UNESCAPED_SLASHES), '"');
	$headers[] = "houseNumber:" . $formData["housenumber"];

	if (array_key_exists("mobilenumber", $formData)) {
		$headers[] = "mobileNumber:" . $formData["mobilenumber"];
	}
	$headers[] = "city:" . trim(json_encode($formData["city"], JSON_UNESCAPED_SLASHES), '"');
	$headers[] = "zipCode:" . $formData["zip"];
	$headers[] = "birthDate:" . $fullDate;
	$headers[] = "contractID:" . $contractID;
	$headers[] = "vehicleID:" . $vehData[0];
	$headers[] = "vehicleTypeID:";//.$fahrzeugMatchArray[$evbData["storageData"]["vehicleTypeID"]];
	$headers[] = "yearOfConstruction:" . $vehData[3];
	$headers[] = "mileage:" . $carData->tacho_stand;
	if ($carData->tacho_einheit == "km") {
		$headers[] = "mileageMetricID:794001";
	} else {
		$headers[] = "mileageMetricID:794002";
	}

	if (property_exists($carData, 'herstellerschluesselnummer') && $carData->herstellerschluesselnummer != "") {
		$headers[] = "hsn:" . $carData->herstellerschluesselnummer;
	}
	if (property_exists($carData, 'typschluesselnummer') && $carData->typschluesselnummer != "") {
		$headers[] = "tsn:" . $carData->typschluesselnummer;
	}
	if (property_exists($carData, 'fin') && $carData->typschluesselnummer != "") {
		$headers[] = "fin:" . $carData->fin;
	}
	if (property_exists($carData, 'fahrzeugkennzeichen') && $carData->typschluesselnummer != "") {
		$headers[] = "licenseNumber:" . $carData->fahrzeugkennzeichen;
	}
	$headers[] = "intendedAdmission:" . $carData->geplante_zulassung;
	$headers[] = "licenseNumberTypeID:" . $kennzeichenMatchArray[$carData->kennzeichen];
	if ($carData->kennzeichen == "105003" || $carData->kennzeichen == "105012") {
		$headers[] = "seasonFrom:" . preg_replace("/\\b0*/", "", $carData->saison_start);
		$headers[] = "seasonTo:" . preg_replace("/\\b0*/", "", $carData->saison_ende);
	}
	if (array_key_exists("sameAddress", $personalData)) {
		$headers[] = "vehicleStreet:" . trim(json_encode($personalData["street"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleHouseNumber:" . $personalData["housenumber"];
		$headers[] = "vehicleCity:" . trim(json_encode($personalData["city"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleZipCode:" . $personalData["zip"];
	} else {
		$headers[] = "vehicleStreet:" . trim(json_encode($personalData["personalStreet"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleHouseNumber:" . $personalData["personalHousenumber"];
		$headers[] = "vehicleCity:" . trim(json_encode($personalData["personalcity"], JSON_UNESCAPED_SLASHES), '"');
		$headers[] = "vehicleZipCode:" . $personalData["personalzip"];
	}
	/* HARDCODED STUFF */
	$headers[] = "vehicleStateID:232001";
	if ($carData->grund_der_versicherung == "new") {
		$headers[] = "processTypeID:553001";
	} else {
		if ($carData->grund_der_versicherung == "change") {
			$headers[] = "processTypeID:553002";
		}
	}

	$headers[] = "stateID:232001";

	error_log("===EVB===" . join(",", $headers) . "\n");

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$res = curl_exec($ch);

	error_log("==EVB-RESULT===" . $res . "\n");

	$result = json_decode($res);
	//$result = array();
	if (curl_errno($ch)) {
		error_log('==Error:==' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}

function convertString($obj)
{
	return json_decode(sprintf('"%s"', $obj));
}

function getMonths($date1, $date2)
{
	$explodedDate1 = explode(".", $date1);
	$explodedDate2 = explode(".", $date2);

	return [$explodedDate1[1], $explodedDate2[1]];
}

function multiple_threads_request($urlConfig, $nodes)
{
	$mh = curl_multi_init();
	$curl_array = [];
	foreach ($nodes as $i => $headers) {

		//error_log("===GET-PREMIUM-" . $i . "===" . join(",", $headers));

		$curl_array[$i] = curl_init();
		curl_setopt($curl_array[$i], CURLOPT_URL, $urlConfig["smartcalc"] . "/get/premium/");
		curl_setopt($curl_array[$i], CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_array[$i], CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($curl_array[$i], CURLOPT_HTTPHEADER, $headers);
		curl_multi_add_handle($mh, $curl_array[$i]);
	}
	$running = null;
	do {
		$status = curl_multi_exec($mh, $running);
		if ($status > 0) {
			// Fehlermeldung anzigen
			error_log('==Error:==' . curl_multi_strerror($status));
		}
	} while ($running > 0);

	$res = [];
	foreach ($nodes as $i => $headers) {
		$res[$i] = json_decode(curl_multi_getcontent($curl_array[$i]));
		curl_multi_remove_handle($mh, $curl_array[$i]);
	}

	curl_multi_close($mh);
	return $res;
}

function checkIban($iban){

	$bankData = array("valid" => false);

	// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, "https://fintechtoolbox.com/validate/iban.json?iban=".str_replace(" ","",$iban));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");


	$headers = array();
	$headers[] = "Authorization: token 4QaXEzEOXdQ1cgvkYltJAEObebW1f2bRHEBrGZUMC1E=";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = json_decode(curl_exec($ch),true);

	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);

	if($result["iban"]["valid"]){

		$bankData["valid"] = true;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://fintechtoolbox.com/bankcodes/".$result["iban"]["bank_code"].".json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		$headers = array();
		$headers[] = "Authorization: token 4QaXEzEOXdQ1cgvkYltJAEObebW1f2bRHEBrGZUMC1E=";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result2 = json_decode(curl_exec($ch),true);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}

		$bankData = array_merge($bankData,$result2["bank_code"]);

		curl_close ($ch);
	}
	return json_encode($bankData);
}


function sendPHPMail($empfaenger, $fromMail, $fromName, $body, $config)
{
	require_once '_included_template/phpmailer/src/Exception.php';
	require_once '_included_template/phpmailer/src/PHPMailer.php';
	require_once '_included_template/phpmailer/src/SMTP.php';

	$mail = new PHPMailer(true);
	try {
		$mail->isSMTP();
		$mail->Host = $config["emailhost"];
		$mail->Port = $config["emailport"];
		$mail->SMTPAuth = true;
		$mail->Username = $config["emailusername"];
		$mail->Password = $config["emailpassword"];
		$mail->SMTPSecure = $config["emailencryption"];

		$mail->setFrom($fromMail, $fromName);
		$mail->addAddress($empfaenger, 'Carisma Kontakt ' . date("d-m-Y H:s:i"));
		$mail->CharSet = 'UTF-8';
		$mail->Subject = 'Carisma.auto Kontaktformular';
		$mail->isHTML(false);
		$mail->Body = html_entity_decode(htmlentities($body, ENT_QUOTES | ENT_IGNORE, "UTF-8"));
		$mail->send();
	} catch (phpmailerException $e) {
		error_log("===KONTAKT=== " . $e->errorMessage());
	} catch (Exception $e) {
		error_log("===KONTAKT=== " . $e->getMessage());
	}
}
