/**
 * Created by root on 15.11.17.
 */

window.onload = function () {
    if (typeof history.pushState === "function") {
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
            if (window.innerWidth < 415) {
                var computedWidth = window.innerWidth - 20;
                $('#confirmationOverlay .contentBoxWrapper').css('width', computedWidth);
            } else if (window.innerWidth < 570) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
            }
            $('.contentBox > h3').html("Hinweis!");
            $('#modalMessageBody').html("Bitte benutzen Sie die Naviagtionselemente auf der Seite .");
            $('#confirmationOverlay > div > div > div > div').hide();
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display', 'table');
            $('#closeOverlay').on('click', function () {
                $('#confirmationOverlay').hide();
            });
        };
    }
    else {
        var ignoreHashChange = true;
        window.onhashchange = function () {
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
            }
            else {
                ignoreHashChange = false;
            }
        };
    }
};

(function () {

    $('.srcollToButton').on('click', function () {
        runScroll();
    });
    if (sessionStorage.getItem("Fahrzeugdaten2") == null) {
        $('.progress').hide();
        $('.contentBox > h3').html("Hinweis!");
        $('#modalMessageBody').html("Bitte starten Sie den Tarifrechner neu");
        if (window.innerWidth < 375) {
            $('#confirmationOverlay .contentBoxWrapper').css('width', window.innerWidth);
        } else if (window.innerWidth < 570) {
            $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
        }
        $('#confirmationOverlay').show();
        $('#confirmationOverlay').css('display', 'table');
        $('#closeOverlay').on('click', function () {
            window.location.href = "/";
        });
    } else {
        var MonthObject = {
            "JAN": "01",
            "FEB": "02",
            "MAR": "03",
            "APR": "04",
            "MAY": "05",
            "JUN": "06",
            "JUL": "07",
            "AUG": "08",
            "SEP": "09",
            "OCT": "10",
            "NOV": "11",
            "DEC": "12"
        };
        var errorMessages = {"ORA-20100": "Es besteht bereits ein Vertrag mit diesen Daten und EVB Nummer"};
        var driverMatchinArray = {
            112004: "Alle Fahrer mindestens 23 und jünger 76 Jahren",
            112005: "Alle Fahrer mindestens 35 und jünger 76 Jahren",
            112006: "Alle Fahrer mindestens 45 und jünger 76 Jahren",
            112007: "Alle Fahrer mindestens 55 und jünger 76 Jahren",
            112009: "Alle Fahrer mindestens 25 und jünger 76 Jahren"
        };
        var anredeMatchArray = {236002: "Herr", 236001: "Frau", 236003: "Firma", 236004: "Familie", 236005: "Eheleute"};
        tarif = sessionStorage.getItem("tarif");
        $('#wizard1').attr("href", "?article_id=32&clang=1&restart=1");
        $('#wizard2').attr("href", "?article_id=19&clang=1&tarif=" + tarif);
        $('#wizard3').attr("href", "?article_id=20&clang=1");
        $('#wizard4').attr("href", "?article_id=21&clang=1");
        $('#wizard5').attr("href", "?article_id=22&clang=1");
        if (sessionStorage.getItem("insuranceID") != null) {
            $('.finish_btn').attr('disabled', true);
            vehData = sessionStorage.getItem("insuranceID").split(",");
            if (vehData[4] == " ") {
                if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
                    if (tarif[1] == "Basis" || tarif[1] == "Basic") {
                        tarif[1] = "Komfort";
                    } else if (tarif[1] == "Komfort" || tarif[1] == "Comfort") {
                        tarif[1] = "Premium";
                    }
                }
                headLineString = decodeURI(tarif[1] + "-Tarif-Angebot für meinen " + vehData[1] + " " + vehData[2] + " von Baujahr " + vehData[3] + " mit" + vehData[5]);
            } else {
                if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
                    if (tarif == "Basis" || tarif == "Basic") {
                        tarif = "Komfort";
                    } else if (tarif == "Komfort" || tarif == "Comfort") {
                        tarif = "Premium";
                    }
                }
                headLineString = decodeURI(tarif + "-Tarif " + vehData[1] + " " + vehData[2] + ", Baujahr " + vehData[3] + ", " + vehData[4] + ", " + vehData[5]);
            }
            $('#allFine').html(sessionStorage.getItem('anforderungLi'));

            $('#headlineData').html(headLineString);
            $('#fahrzeug').html(vehData[1] + " " + vehData[2] + ", Baujahr " + vehData[3] + ", " + vehData[4] + ", " + vehData[5]);
            var kvsDetailList = JSON.parse(sessionStorage.getItem('#sel1'));
            var personalData = JSON.parse(sessionStorage.getItem("personalDataFilled"));
            //console.log(carData);
            var insuranceString = " mit " + sessionStorage.getItem('kinofselbstbehaltText').replaceAll("SB", "Selbstbeteiligung").replaceAll("Teilkasko", "Teilkasko - Versicherung und").replaceAll("Vollkasko", "Vollkasko - Versicherung");
            $('#selbstbeteiligung').html(insuranceString);
            var carData = JSON.parse(sessionStorage.getItem("Fahrzeugdaten2"));
            $('#tarifname').html(tarif + "-Tarif");
            $('.insuranceamount').html(sessionStorage.getItem("Amount"));
            $('#prename').html(personalData.firstname);
            $('#lastname').html(personalData.lastname);
            $('#accountholder').html(sessionStorage.getItem("Bank-Accountholder"));
            $('#way_of_payment').html("jährlich");
            $('#type_of_payment').html("Bankeinzug");
            $('#iban').html(sessionStorage.getItem("Iban"));
            $('#gebDate').html(personalData.birthday + "." + MonthObject[personalData.birthmonth] + "." + personalData.birthyear);
            $('#bic').html(sessionStorage.getItem("BIC"));
            if (carData.tacho_einheit == "km") {
                $('#tachoeinheit').html("Kilometer");
            } else {
                $('#tachoeinheit').html("Meilen");
            }

            if (carData.reusecheckbox != undefined && carData.wiederherstellungswert != "") {
                $('#defaultInfo > li:nth-child(9)').after('<li>Wiederherstellungswert: ' + carData.wiederherstellungswert + ' Euro</li><li>Wiederherstellungswert soll versichert werden</li>');
            }
            $('#anrede').html(anredeMatchArray[sessionStorage.getItem("anredeID")]);
            $('#zustandsnote').html(sessionStorage.zustandsnote);
            $('#kilometerstand').html(carData.tacho_stand);
            if (personalData.personalStreet !== "") {
                $('#standort').html(personalData.personalStreet + " " + personalData.personalHousenumber + ", " + personalData.personalzip + " " + personalData.personalcity);
            } else {
                $('#standort').html(personalData.street + " " + personalData.housenumber + ", " + personalData.zip + " " + personalData.city);
            }

            $('#address').html(personalData.street + " " + personalData.housenumber + ", " + personalData.zip + " " + personalData.city);
            if (carData.grund_der_versicherung == "change") {
                $('#grund_der_versicherung').html("Versicherungswechsel");
            } else if (carData.grund_der_versicherung == "new") {
                $('#grund_der_versicherung').html("Neuzulassung");
            }

            $('#geplante_zulassung').html(carData.geplante_zulassung);
            $('#erstzulassung').html(carData.erstzulassung_tag+"."+MonthObject[carData.erstzulassung_monat] + "." + carData.erstzulassung);
            var sel5 = JSON.parse(sessionStorage.getItem("#sel5"));
            $.each(kvsDetailList.json.result, function (i, v) {
                if (carData.fahrleistung == v.ID) {
                    if (sessionStorage.getItem("kilometerNeeded") != null) {
                        $('#fahrleistung').html(v.name);
                    }
                } else if (carData.fahreralter == v.ID) {
                    if (v.ID == 112004) {
                        $('.fahreralter').html(': "keine abweichende Auswahl getroffen"');
                        $('.fahreralter1').html(driverMatchinArray[v.ID]);
                    } else {
                        $('.fahreralter').html("(durch spätere Auswahl eingeschränkt auf " + driverMatchinArray[v.ID] + ")");
                        $('.fahreralter1').html(driverMatchinArray[v.ID]);
                    }
                    /*if(sel5.json.result[0].vehicleClassID == 100001 || sel5.json.result[0].vehicleClassID == 100002){
                        var matchingObject = {100001:"Alle Fahrer mindestens 25 und jünger 76 Jahre"};
                        $('.fahreralter').html(matchingObject[100001]);
                    }else{

                    }*/
                } else if (carData.kennzeichen == v.ID) {
                    $('#kennzeichen').html(v.name);
                    if (parseInt(carData.kennzeichen) == 105003 || parseInt(carData.kennzeichen) == 105012) {
                        $('#defaultInfo > li:nth-child(12)').append("<li>Saison Monat Start: " + carData.saison_start + "</li><li>Saison Monat Ende: " + carData.saison_ende + "</li>");
                    }
                }
            });
            $('#driversSubAge').css('display', 'block');

            $('#marktwert').html(tausender(carData.marktwert) + " ");
            $('#zustandsnote').html(carData.custom_type);
        }

        $('#correct').on('change', function () {
            if (this.checked) {
                $('#rechtlicheHinweisBox').show();
            } else {
                $('#rechtlicheHinweisBox').hide();
            }
        });

        $('#rightButton').on('click', function () {
            $('#correct').attr('required', false);
            $('#correct_right').attr('required', false);
            $("#correct_right").attr("disabled", false);
            $('#rechtlicheHinweisBox > div.checkbox > label > b').css("color", "#000");
            $(this).hide();
        });

        $('#rightButtonClose').on('click', function () {
            $('html, body').scrollTop($("#rightAnchor").offset().top);
            $('#rightButton').show();
        });

        $('#correct_right').on('change', function () {

            if (this.checked) {
                $('#saveDataRow').hide();
                $('#modalMessageBody').html("Ihre Daten werden gespeichert und ein unverbindliches Angebot erstellt");
                $('#closeOverlay').hide();
                if (window.innerWidth < 375) {
                    $('#confirmationOverlay .contentBoxWrapper').css('width', window.innerWidth);
                } else if (window.innerWidth < 570) {
                    $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
                }
                $('#confirmationOverlay').show();
                $('#confirmationOverlay').css('display', 'table');
                var postData = {"command": "saveCalc"};
                storageObject = dumpSessionStorageToVar();
                postData["storageData"] = storageObject;
                $.ajax({
                    url: "../ajaxtest.php",
                    method: "post",
                    data: postData,
                    success: function (content) {
                        data = JSON.parse(content);
                        if (data.success == "true") {
                            sessionStorage.setItem('contractId', data.contractID);
                            var insuranceID = sessionStorage.getItem("insuranceID");
                            var fahrzeug = JSON.parse(sessionStorage.getItem("Fahrzeugdaten2"));
                            var personalData = JSON.parse(sessionStorage.getItem("personalDataFilled"));
                            var carData = sessionStorage.getItem("insuranceID").split(",");
                            var postData = {
                                "command": "saveOffer",
                                "contractID": sessionStorage.getItem('contractId'),
                                "initialRegistration": MonthObject[fahrzeug.erstzulassung_monat] + "." + fahrzeug.erstzulassung,
                                "yearOfConstruct": carData[3],
                                "username": sessionStorage.getItem("username"),
                                "sessionId": sessionStorage.getItem("usersession"),
                                "mileMetricID": fahrzeug["tacho_einheit"],
                                "mileage": fahrzeug["tacho_stand"],
                                "intendedAdmission": fahrzeug["geplante_zulassung"],
                                "personalData": personalData,
                                "vehData": insuranceID
                            };
                            $.ajax({
                                url: "../ajaxtest.php",
                                method: "post",
                                data: postData,
                                success: function (content) {
                                    data = JSON.parse(content);
                                    $('.progress').hide();
                                    if (data.success == "true") {
                                        sessionStorage.setItem('contractId', data.contractID);
                                        $('#modalMessageBody').html("Das Angebot wurde Ihnen per E-Mail zugestellt");
                                        $('#closeOverlay').on('click', function () {
                                            $('#confirmationOverlay').hide();
                                        });
                                        $('#closeOverlay').show();
                                        $('.finish_btn').attr('disabled', false);
                                    } else {
                                        $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                                        $('#closeOverlay').on('click', function () {
                                            window.location.href = "/?article_id=7&clang=1";
                                        });
                                    }
                                },
                                error: function(){
                                    $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                                    $('#closeOverlay').on('click', function () {
                                        window.location.href = "/?article_id=7&clang=1";
                                    });
                                }
                            });
                        } else {
                            $('.progress').hide();
                            $('#closeOverlay').on('click', function () {
                                $('#confirmationOverlay').hide();
                            });
                            $('#modalMessageBody').html("Das Angebot wurde Ihnen per E-Mail zugestellt");
                            $('#closeOverlay').show();
                        }
                    },
                    error: function () {
                        $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                        $('#closeOverlay').on('click', function () {
                            window.location.href = "/?article_id=7&clang=1";
                        });
                    }
                });
            }
        });

        $('.finish_btn').click(function () {
            if (window.innerWidth < 375) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', window.innerWidth);
            } else if (window.innerWidth < 570) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
            }
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display', 'table');
            if (parseInt(sessionStorage.getItem("TarifID")) == 10827 && sessionStorage.getItem("vehicleTypeID") != 202005) {
                $('#modalMessageBody').html('Zum Abschluss der in Ihrem gewählten Tarif enthaltenen All Risk Gefahrenabdeckung müssen Sie innerhalb von 8 Wochen nach Anmeldung Ihres Fahrzeugs ein Wertgutachten vorlegen.');
                $('.progress').hide();
                $('#cancelOverlay').css("display", "inline-block");
                $('#cancelOverlay').on('click', function () {
                    $('#cancelOverlay').hide();
                    $('#confirmationOverlay').hide();
                });
                $('#closeOverlay').on('click', function () {
                    $('#cancelOverlay').hide();
                    $('.finish_btn').unbind('click');
                    $('.finish_btn').on('click',getEVB);
                    //getEVB();
                })
            } else {
                getEVB();
            }
        });
        $('.savebankData').on("click", function () {
            if (window.innerWidth < 375) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', window.innerWidth);
            } else if (window.innerWidth < 570) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
            }
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display', 'table');
            $('.progress-striped').hide();
            $('#modalMessageBody').html("Berechnung wird gespeichert");
            $('#closeOverlay').hide();
            var postData = {"command": "saveCalc"};
            storageObject = dumpSessionStorageToVar();
            postData["storageData"] = storageObject;
            $.ajax({
                url: "../ajaxtest.php",
                method: "post",
                data: postData,
                success: function (content) {
                    data = JSON.parse(content);
                    $('#closeOverlay').show();
                    if (data.success != "true") {
                        $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                        $('#closeOverlay').on('click', function () {
                            window.location.href = "/?article_id=7&clang=1";
                        });
                    } else {
                        $('#closeOverlay').on('click', function () {
                            $('#confirmationOverlay').hide();
                        });
                    }
                },
                error: function () {
                    $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                    $('#closeOverlay').on('click', function () {
                        window.location.href = "/?article_id=7&clang=1";
                    });
                }
            });
        });

        $('.change_btn').on('click', function () {
            $('#correct').attr('required', false);
            $('#correct_right').attr('required', false);
            window.location.href = "?article_id=32&clang=1&restart=1#detailAll";
        });
    }

})();

function runScroll() {
    scrollTo(document.body, 0, 0);
}

function getEVB() {
    if ($('#correct').prop("checked") && $('#correct_right').prop("checked")) {

        if (window.innerWidth < 375) {
            $('#confirmationOverlay .contentBoxWrapper').css('width', window.innerWidth);
        } else if (window.innerWidth < 570) {
            $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
        }

        $('#closeOverlay').hide();
        $('#confirmationOverlay > div > div > div > div > div').show();
        $('.progress').show();
        $('#cancelOverlay').hide();

        $('#confirmationOverlay').show();
        $('#confirmationOverlay').css('display', 'table');
        $('#modalMessageBody').html("Bankdaten werden gespeichert");


        var postData = {"command": "final"};
        storageObject = dumpSessionStorageToVar();
        postData["storageData"] = storageObject;
        $.ajax({
            url: "../ajaxtest.php",
            method: "post",
            data: postData,
            success: function (content) {
                data = JSON.parse(content);
                if (data.success) {
                    $('#modalMessageBody').html("EVB wird angefordert");
                    var contractID = sessionStorage.getItem("contractId");
                    var postData = {"command": "getEvb", "contractID": contractID};
                    storageObject = dumpSessionStorageToVar();
                    postData["storageData"] = storageObject;
                    $.ajax({
                        url: "../ajaxtest.php",
                        method: "post",
                        data: postData,
                        success: function (content) {
                            data = JSON.parse(content);
                            sessionStorage.setItem("evbContent", content);
                            sessionStorage.setItem("evbNumber", data.result.evbNumber);
                            sessionStorage.setItem("validUntil", data.result.validUntil);
                            window.location.href = "/?article_id=36?clang=1";
                        }
                    });
                } else {
                    $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                    $('#closeOverlay').on('click', function () {
                        window.location.href = "/?article_id=7&clang=1";
                    });
                }
            },
            error: function () {
                $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                $('#closeOverlay').on('click', function () {
                    window.location.href = "/?article_id=7&clang=1";
                });
            }
        });
    } else {
        $('html, body').scrollTop($("#iban").offset().top);
        $('#correct').attr('required', 'required');
        $('#correct_right').attr('required', 'required');
        if (window.innerWidth < 768) {
            if (window.innerWidth < 375) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', window.innerWidth);
            } else if (window.innerWidth < 570) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
            }
            $('.contentBox > h3').html("Hinweis!");
            $('#modalMessageBody').html("Sind alle Angaben korrekt?");
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display', 'table');
            $('.progress-striped').hide();
            $('#closeOverlay').on('click', function () {
                $('#confirmationOverlay').hide();
            });
        }
    }
}
