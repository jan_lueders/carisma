/**
 * Created by root on 15.11.17.
 */
window.onload = function () {
    if (typeof history.pushState === "function") {
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
            if (window.innerWidth < 415) {
                var computedWidth = window.innerWidth - 20;
                $('#confirmationOverlay .contentBoxWrapper').css('width', computedWidth);
            } else if (window.innerWidth < 570) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
            }
            $('.contentBox > h3').html("Hinweis!");
            $('#modalMessageBody').html("Bitte benutzen Sie die Naviagtionselemente auf der Seite .");
            $('#confirmationOverlay > div > div > div > div').hide();
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display', 'table');
            $('#closeOverlay').on('click', function () {
                $('#confirmationOverlay').hide();
            });
        };
    }
    else {
        var ignoreHashChange = true;
        window.onhashchange = function () {
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
            }
            else {
                ignoreHashChange = false;
            }
            if (window.innerWidth < 415) {
                computedWidth = window.innerWidth - 20;
                $('#confirmationOverlay .contentBoxWrapper').css('width', computedWidth);
            } else if (window.innerWidth < 570) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
            }
            $('#modalMessageBody').html("Bitte benutzen Sie die Naviagtionselemente auf der Seite .");
            $('#confirmationOverlay > div > div > div > div').hide();
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display', 'table');
            $('#closeOverlay').on('click', function () {
                $('#confirmationOverlay').hide();
            });
        };
    }
};
(function () {


    var OSName = "Unknown OS";
    if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
    else if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
    else if (navigator.appVersion.indexOf("X11") != -1) OSName = "UNIX";
    else if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";

    var is_mobile = window.innerWidth <= 768;

    if (is_mobile) {
        if (OSName == "MacOS") {
            $('#gebdate').css('min-height', '3.8rem');
        }
    }

    var url = window.location.href;
    var params = url.split('?');
    var allParams = params[1].split("&");
    var headLineString = "";

    var username = "";
    var usersession = "";

    var birthYears = "<option value=''></option>";
    var today = new Date();
    var past90 = (today.getFullYear() - 90);
    var totalFullYear = (today.getFullYear() - 18);
    for (var i = past90, j = totalFullYear; j > i; j--) {
        birthYears += "<option value='" + j + "'>" + j + "</option>";
    }
    $('#birthyear').html(birthYears);

    if (window.innerWidth < 415) {
        computedWidth = window.innerWidth - 20;
        $('#confirmationOverlay .contentBoxWrapper').css('width', computedWidth);
    } else if (window.innerWidth < 570) {
        $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
    }
    if (sessionStorage.getItem("Fahrzeugdaten2") == null) {
        $('#modalMessageBody').html("Um die gewählte Funktion ausführen zu können, müssen Sie sich bei CARISMA registrieren. Wenn Sie schon registriert sind, gehen Sie bitte zum Login.");
        $('.progress').removeClass("active").hide();
        $('#confirmationOverlay > div > div > div > div').hide();
        $('#confirmationOverlay').show();
        $('#confirmationOverlay').css('display', 'table');
        $('#closeOverlay').html("zur Registrierung");
        $('#closeOverlay').on('click', function () {
            window.location.href = "?article_id=18&clang=1"
        });
        $('#toLogin').on('click', function () {
            window.location.href = "?article_id=17&clang=1"
        });
        $('#toLogin').show();
        $('#closeOverlay').show();
    } else {
        if (allParams.length > 2 && allParams[(allParams.length - 1)] == "lastpage=reg") {
            params = allParams[2].split("=");
            if (params.length > 1) {
                sessionStorage.setItem("username", params[2]);
                username = params[2];
                sessionStorage.setItem("usersession", params[1]);
                usersession = params[1];
            }
            $('#modalMessageBody').html("Sie haben sich erfolgreich registriert und können nun beliebig viele Berechnungen speichern und wieder laden.");
            $('#confirmationOverlay > div > div > div > div').hide();
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display', 'table');
            $('#closeOverlay').on('click', function () {
                $('#confirmationOverlay').hide();
            });

        } else if (allParams.length > 2 && allParams[(allParams.length - 1)] == "lastpage=login") {
            params = allParams[2].split("=");
            if (params.length > 1) {
                sessionStorage.setItem("username", params[2]);
                username = params[2];
                sessionStorage.setItem("usersession", params[1]);
                usersession = params[1];
            }
            $('#modalMessageBody').html("Sie haben sich erfolgreich eingeloggt und können nun beliebig viele Berechnungen speichern und wieder laden.");
            $('.progress').removeClass("active").hide();
            $('#confirmationOverlay > div > div > div > div').hide();
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display', 'table');
            $('#closeOverlay').on('click', function () {
                $('#confirmationOverlay').hide();
            });
            $('#closeOverlay').show();
        }
        if (sessionStorage.getItem("usersession") !== null) {
            $('#firstname').val(sessionStorage.getItem("prename"));
            $('#lastname').val(sessionStorage.getItem('lastname'));
            username = sessionStorage.getItem("username");
            usersession = sessionStorage.getItem("usersession");
        }
        if (sessionStorage.getItem("personalDataFilled") !== null) {
            dataRefill();
            if ($('#personalStreet').val() !== "" && $('#personalStreet').val() == $('#street').val()) {
                $('#sameAddress').attr('checked', 'checked');
            }
        }

        tarif = sessionStorage.getItem("tarif");
        carData = JSON.parse(sessionStorage.getItem("Fahrzeugdaten2"));
        $('#wizard1').attr("href", "?article_id=32&clang=1&restart=1");
        $('#wizard2').attr("href", "?article_id=19&clang=1&tarif=" + tarif);
        $('#wizard3').attr("href", "?article_id=20&clang=1");
        tmpVehData = sessionStorage.getItem("insuranceID");
        if (tmpVehData != null) {
            var vehData = tmpVehData.split(",");
            if (vehData[4] == " ") {
                if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
                    if (tarif[1] == "Basis" || tarif[1] == "Basic") {
                        tarif[1] = "Komfort";
                    } else if (tarif[1] == "Komfort" || tarif[1] == "Comfort") {
                        tarif[1] = "Premium";
                    }
                }
                headLineString = decodeURI(tarif[1] + "-Tarif-Angebot für meinen " + vehData[1] + " " + vehData[2] + " von Baujahr " + vehData[3] + " mit" + vehData[5]);
            } else {
                if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
                    if (tarif == "Basis" || tarif == "Basic") {
                        tarif = "Komfort";
                    } else if (tarif == "Komfort" || tarif == "Comfort") {
                        tarif = "Premium";
                    }
                }
                headLineString = decodeURI(tarif + "-Tarif " + vehData[1] + " " + vehData[2] + ", Baujahr " + vehData[3] + ", " + vehData[4] + ", " + vehData[5]);
            }
            $('#headlineData').html(headLineString);
        }

        postData = {"command": "getInputData", "username": username, "sessionkey": usersession};
        $.ajax({
            url: "../ajaxtest.php",
            method: "post",
            data: postData,
            success: function (content) {
                var data = JSON.parse(content);
                sessionStorage.setItem("personalData", content);

                var forJSON = '"' + data.json.result.prename.replace(/"/g, '\\"').replace(/[\n\r]+/g, '\\n') + '"';
                $('#firstname').val(JSON.parse(forJSON));
                sessionStorage.setItem("prename", JSON.parse(forJSON));

                var forJSONLastname = '"' + data.json.result.lastname.replace(/"/g, '\\"').replace(/[\n\r]+/g, '\\n') + '"';
                $('#lastname').val(JSON.parse(forJSONLastname));
                sessionStorage.setItem("lastname", JSON.parse(forJSONLastname));

                $('#lastname').addClass('has-no-error');
                $('#firstname').addClass('has-no-error');
                $('#firstname').prop("readonly", true);
                $('#lastname').prop("readonly", true);

                if (data.json.result.birthday != undefined) {
                    var splittedDate = data.json.result.birthday.split("-");

                    $("#birthday option:contains(" + splittedDate[0] + ")").prop('selected', true);
                    $('#birthday').val(splittedDate[0]).change();

                    $("#birthmonth option:contains(" + splittedDate[1] + ")").prop('selected', true);
                    $('#birthmonth').val(splittedDate[1]).change();

                    if (splittedDate[2] < 30 && splittedDate[2] < 99) {
                        splittedDate[2] = "19" + splittedDate[2];
                    } else if (splittedDate[2] == "00") {
                        splittedDate[2] = "20" + splittedDate[2];
                    }

                    $("#birthyear option:contains(" + splittedDate[2] + ")").attr('selected', true);
                    $('#birthyear').val(splittedDate[2]).change();

                    $('#birthday').addClass("has-no-error");
                    $('#birthmonth').addClass("has-no-error");
                    $('#birthyear').addClass("has-no-error");

                    $('#birthday').attr("disabled", true);
                    $('#birthmonth').attr("disabled", true);
                    $('#birthyear').attr("disabled", true);

                } else {
                    if($('#birthday :selected').val() != ""){
                        $('#birthday').addClass("has-no-error");
                    }else if($('#birthmonth :selected').val() != ""){
                        $('#birthmonth').addClass("has-no-error");
                    }else if($('#birthyear :selected').val() != ""){
                        $('#birthyear').addClass("has-no-error");
                    }else{
                        $('#birthday').removeClass("has-no-error");
                        $('#birthmonth').removeClass("has-no-error");
                        $('#birthyear').removeClass("has-no-error");
                    }
                }


                $('#birthyear').find('option[selected="selected"]').each(function () {
                    $(this).prop('selected', true);
                });

                if (data.json.result.salutation != undefined || data.json.result.salutation != null) {
                    var anrede = "";
                    if (data.json.result.salutation == 252001 || data.json.result.salutation == 236002) {
                        anrede = "Herr";
                    } else if (data.json.result.salutation == 252002 || data.json.result.salutation == 236001) {
                        anrede = "Frau";
                    } else if (data.json.result.salutation == 236003) {
                        anrede = "Firma";
                    } else if (data.json.result.salutation == 236004) {
                        anrede = "Familie";
                    } else if (data.json.result.salutation == 236005) {
                        anrede = "Eheleute";
                    }

                    $("#form_of_address option:contains(" + data.json.result.salutation + ")").attr('selected', true);
                    $('#form_of_address').val(data.json.result.salutation).change();
                    $('#form_of_address').attr("disabled", true);
                }

                if (data.json.result.mobileNumber != undefined || data.json.result.mobileNumber != null) {
                    $('#mobilenumber').val(data.json.result.mobileNumber.replace("+","00").replace(" ",""));
                    $('#mobilenumber').prop("readonly", true);
                    $('#mobilenumber').addClass("has-no-error");
                }

                if (data.json.result.street != null) {
                    var forJSON = '"' + data.json.result.street.replace(/"/g, '\\"').replace(/[\n\r]+/g, '\\n') + '"';
                    document.getElementById('street').value = unescape(data.json.result.street);

                    var forCityJSON = '"' + data.json.result.city.replace(/"/g, '\\"').replace(/[\n\r]+/g, '\\n') + '"';

                    $('#street').val(JSON.parse(forJSON)).addClass('has-no-error');
                    $('#housenumber').val(data.json.result.houseNumber).addClass('has-no-error');
                    $('#zip').val(data.json.result.zipCode).addClass('has-no-error');
                    $('#zip').removeClass("has-error");
                    $('#city').val(JSON.parse(forCityJSON)).addClass('has-no-error');
                    $('#street').prop("readonly", true);
                    $('#housenumber').prop("readonly", true);
                    $('#zip').prop("readonly", true);
                    $('#zip').css('background-color', '#eee');
                    $('#city').prop("readonly", true);
                }
            }
        });
        $('#form_of_address').change(function () {
            data = $(this).val();
            anrede = "";
            if (data == 252001 || data == 236002) {
                anrede = "Herr";

            } else if (data == 252002 || data == 236001) {
                anrede = "Frau";
            } else if (data == 236003) {
                anrede = "Firma";
            } else if (data == 236004) {
                anrede = "Familie";
            } else if (data == 236005) {
                anrede = "Eheleute";
            }
            $(this).addClass('has-no-error');
            sessionStorage.setItem("anrede", anrede);
            sessionStorage.setItem("anredeID", data);
        });
        $('.save_btn').on("click", function (e) {
            $('#modalMessageBody').html("Ihre Berechnung wird gespeichert");
            $('.progress').show();
            if (window.innerWidth < 415) {
                computedWidth = window.innerWidth - 20;
                $('#confirmationOverlay .contentBoxWrapper').css('width', computedWidth);
            } else if (window.innerWidth < 570) {
                $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
            }
            $('.contentBox > h3').html("Hinweis!");
            $('body').css('overflow', 'hidden');
            $('#confirmationOverlay').css('display', 'table').fadeIn();
            if (sessionStorage.getItem("usersession") !== null) {
                var postData = {"command": "saveCalc"};
                storageObject = dumpSessionStorageToVar();
                postData["storageData"] = storageObject;
                $.ajax({
                    url: "../ajaxtest.php",
                    method: "post",
                    data: postData,
                    success: function (content) {
                        data = JSON.parse(content);
                        sessionStorage.setItem("contractID", data.contractID);
                        $('body').css('overflow', 'auto');
                        $('#confirmationOverlay').fadeOut();
                    },
                    error: function () {
                        $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                        $('#closeOverlay').on('click', function () {
                            window.location.href = "/?article_id=7&clang=1";
                        });
                    }
                });
            } else {
                sessionStorage.setItem("wayofcicle", "wayofcicle21");
                window.location.href = "?article_id=17&clang=1";
            }
            return false;
        });
        if (is_mobile) {
            $('#mobileSubmitButton').on('click', function () {
                $('#userform').submit()
            });
        }

        $('.finish_btn').on('click', function () {
            $('#birthday').attr("disabled", false);
            $('#birthmonth').attr("disabled", false);
            $('#birthyear').attr("disabled", false);
            $('#form_of_address').attr("disabled", false);
            var fromString = JSON.stringify($('#userform').serializeAssoc());
            sessionStorage.setItem("personalDataFilled", fromString);
            if(fromString["mobilenumber"] !== undefined){
                fromString["mobilenumber"] = fromString["mobilenumber"].replace(/\s/g,'');
            }
            sessionStorage.setItem("personalDataFilled", fromString);
        });
        $('#sameAddress').removeClass("has-no-error");
        $('#sameAddress').change(function () {
            if (this.checked) {
                $('#personalStreet').val($('#street').val()).addClass('has-no-error');
                $('#personalHousenumber').val($('#housenumber').val()).addClass('has-no-error');
                $('#personalzip').val($('#zip').val()).addClass('has-no-error');
                $('#personalcity').val($('#city').val()).addClass('has-no-error');
                if($('#addressAdding').val() != ""){
                    $('#personalAddressAdding').val($('#addressAdding').val()).addClass('has-no-error');
                }

            } else {
                $('#personalStreet').val('').removeClass('has-no-error');
                $('#personalHousenumber').val('').removeClass('has-no-error');
                $('#personalAddressAdding').val('').removeClass('has-no-error');
                $('#personalzip').val('').removeClass('has-no-error');
                $('#personalcity').val('').removeClass('has-no-error');
            }
        });
    }
})();

function dataRefill() {
    var personalDataFilled = JSON.parse(sessionStorage.getItem("personalDataFilled"));
    $.each(personalDataFilled, function (id, v) {
        if (v !== "") {
            if ($('#' + id).is("input") && !$('#' + id).is(':checkbox')) {

                $('#' + id).val(decodeURI(v));
            } else if ($('#' + id).is("select")) {
                $("#" + id + " option:contains(" + v + ")").prop('selected', true);
                console.log(v);
                $('#' + id).val(decodeURI(v)).change();
            }
            $('#' + id).addClass('has-no-error');
        }
    });
}

function twoDigitDate(d) {
    return ((d.getDate()).toString().length == 1) ? "0" + (d.getDate()).toString() : (d.getDate()).toString();
};

function twoDigitMonth(d) {
    return ((d.getMonth() + 1).toString().length == 1) ? "0" + (d.getMonth() + 1).toString() : (d.getMonth() + 1).toString();
};

function toUnicode(str) {
    return str.split('').map(function (value, index, array) {
        var temp = value.charCodeAt(0).toString(16).toUpperCase();
        if (temp.length > 2) {
            return '\\u' + temp;
        }
        return value;
    }).join('');
}
