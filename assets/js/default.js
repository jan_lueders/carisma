var is_mobile;


function checkUser() {
    if (sessionStorage.getItem('usersession')) {
        $('.regButtonMenu').html(sessionStorage.getItem('username'));
        $('.loginButtonMenu').hide();
        $('.loginButtonMenu').html("Logout");
        $('.loginButtonMenu').show();
        $('.loginButtonMenu').attr("href","");
        $('.regButtonMenu').attr("href","");
        $('.regButtonMenu').on('click', function(e){
            e.preventDefault();
        });
        $('.loginButtonMenu').on('click', function(e){
            e.preventDefault();
            postData = {
                "logout": "logout",
                "username": sessionStorage.getItem('username'),
                "sessionkey": sessionStorage.getItem('usersession')
            };
            $.ajax({
                url: "../login.php",
                method: "post",
                data: postData,
                success: function(content) {
                    $('.loginButtonMenu').attr("href","?article_id=17&clang=1");
                    $('.regButtonMenu').attr("href","?article_id=18&clang=1");
                    $('.regButtonMenu').html("Registrierung");
                    $('.loginButtonMenu').html("Login");
                    sessionStorage.removeItem('username');
                    sessionStorage.removeItem('usersession');
                    $('.loginButtonMenu').off();
                    deleteUserSessionStorage();
                }
            });
        })
    }
};


Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

$(function historyBack() {
    $('a[href="#back"]').click(function(e) {
        e.preventDefault();
        history.back();
    });
});

$(function checkNaviIsOpen() {
    var elem = document.getElementById("navigationOverlay");
    if (elem.className === "open") {
        document.getElementById('burger-menu-icon-close').click();
    }
});

// adds tab href to url + opens tab based on hash on page load
// from https://stackoverflow.com/a/12138756
$(function hashTabJump() {
    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');
    $('.nav-tabs a').click(function(e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop() || $('html').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });
});
/* for events page  */
/*$(function tabHighlightsList() {
    function update() {
        var index = $('.nav-tabs li.active').index();
        $('.fourthImage li').removeClass('active').eq(index).addClass('active');
    }

    $(window).on('hashchange', update);
    update();
});*/
(function burgerNavigation() {
    var burgerMenuIcon, burgerMenuIconClose, navigationOverlay;

    function burgerMenuClickHandling(e) {
        // change click button style
        if (this.id === 'burger-menu-icon') {
            navigationOverlay.className = 'open';
            var $mask = document.createElement('div');
            $mask.id = 'navigationMask';
            $mask.className = 'open';
            document.querySelector('#navigationOverlay').parentNode.appendChild($mask);
        } else {
            navigationOverlay.className = 'close';
            var activeTimeout = setTimeout(function() {
                navigationOverlay.className = ''
            }, 300);
            var $mask = document.querySelector('#navigationMask');
            $mask.parentNode.removeChild($mask);
        }
    }

    $(function() {
        burgerMenuIcon = document.getElementById('burger-menu-icon');
        burgerMenuIconClose = document.getElementById('burger-menu-icon-close');
        navigationOverlay = document.getElementById('navigationOverlay');
        burgerMenuIcon.addEventListener('click', burgerMenuClickHandling);
        burgerMenuIconClose.addEventListener('click', burgerMenuClickHandling);
    });
})();
(function calculator() {
    var midMobile = window.innerWidth <= 1599;
    var xxl = window.innerWidth <= 1886;
    var mobile = window.innerWidth <= 768;

    var $wrapper = $('#insuranceJumbotron .content-wrapper');
    var $intro = $("#insuranceJumbotron .intro");
    var $calculator = $("#insuranceJumbotron .calculator");
    var $result = $("#insuranceJumbotron .result");
    $('a[data-js-action="toggle"]').click(function toggleRechner(e) {
        e.preventDefault();
        var url = window.location.href;
        var params = url.split('?');
        var buttonId = "";
        var idArray = ["toggle38CalcFirst", "toggle38CalcFirstMobile", "toggle38CalcFirst2"];
        var allParams = params[1].split("&");

        if (idArray.indexOf($(this).attr('id')) >= 0) {
            $(this).hide();
            $('#tarifBtn').hide();
            if (midMobile) {
                $('.calculator div:last').css('position', 'relative').css('width', '100%');
            }

        } else if ($(this).attr('id') === "clearBackButton" || $(this).attr('id') === "toggleButton") {
            var mobile = window.innerWidth <= 768;
            $(this).hide();

            if (midMobile && !mobile) {
                $('#toggle38CalcFirst2').css('display', '');
            } else if (mobile) {
                $('#toggle38CalcFirstMobile').css('display', '');
                if (allParams[0] !== "article_id=38") {
                    if (isChrome()) {
                        setTimeout(function() {
                            window.scrollTo(0, $("#tarifeBoxWrapper").offset().top);
                        }, 100);
                    } else {
                        window.scrollTo(0, $("#tarifeBoxWrapper").offset().top);
                    }
                }
            } else {
                $('.lastButtonDiv > a:nth-child(1)').css('display', '');
            }
            if (allParams[0] == "article_id=32") {

                /*var vehData = sessionStorage.getItem("insuranceID").split(",");
                var carString = decodeURI(vehData[1] + " " + vehData[2] + ", Baujahr " + vehData[3] + ", " + vehData[4] + ", " + vehData[5] + "");

                var sel1 = JSON.parse(sessionStorage.getItem('#sel1'));
                var vehicleTypeID = sessionStorage.getItem("vehicleTypeID");

                if(vehicleTypeID == 202001){
                    console.log("if 1");
                    document.getElementById("sel1").selectedIndex = 1;
                }else if(vehicleTypeID == 202002){
                    console.log("if 2");
                    document.getElementById("sel1").selectedIndex = 2;
                }else if(vehicleTypeID == 202003){
                    console.log("if 3");
                    document.getElementById("sel1").selectedIndex = 3;
                }else if(vehicleTypeID == 202004){
                    console.log("if 4");
                    document.getElementById("sel1").selectedIndex = 4;
                }else if(vehicleTypeID == 202005){
                    console.log("if 5");
                    document.getElementById("sel1").selectedIndex = 5;
                }

                $('#sel2').html('<option value="' + vehData[1] + '">' + vehData[1] + '</option>').change();
                $('#sel3').html('<option value="' + vehData[3] + '" >' + vehData[3] + '</option>').change();
                $('#sel4').html('<option value="' + vehData[2] + '">' + vehData[2] + '</option>').change();
                $('#sel4 option[value="' + vehData.join() + '"]').attr('selected', 'selected');
                $('#sel5').html('<option value="' + vehData[2] + '">' + carString + '</option>').change();
                $('#sel5 option[value="' + vehData.join() + '"]').attr('selected', 'selected');*/

                $('#clearBackButton').css('display', 'none');
                //$('#clearButton').html("Fahrzeugdaten ändern").css('padding-top', '7%');
                //$('#clearButton').css('display', '');
            }
        }

        $wrapper.toggleClass('calculator-visible');

        if (params.length > 1) {
            //$('.mtb').css('top',0);
            $('#toggleButton').css('display', '');


            if (allParams[0] == "article_id=38") {
                $('a[data-js-action="clear"]').click();
                startTarifRechner();
                $('#clearBackButton').hide();
                $('#toggleButton').hide();
                $('#tarifBtn').css('display', '');
                //$('.result.desktop').css("opacity","0.3");
                //$('#insuranceJumbotron .calculator form').css('padding', '75px 20px');
                $('#tarifBtn').on('click', function() {
                    $('.rechnerForm').submit();
                });
            }
        }
    });
    $('a[data-js-action="clear"]').click(function clearFields(e) {
        e.preventDefault();

        if (sessionStorage.getItem("Fahrzeugdaten2") != null) {
            sessionStorage.removeItem("Fahrzeugdaten2");
            sessionStorage.removeItem("anforderungLi");
            sessionStorage.removeItem("deductible");
            sessionStorage.removeItem("premium");
            sessionStorage.removeItem("Anforderung");
            sessionStorage.removeItem("kindofselbstbehalt");
            sessionStorage.removeItem("kindofselbstbehaltText");
            sessionStorage.removeItem("tarif");
            sessionStorage.removeItem("tarifID");
        }


        $('#insuranceJumbotron #sel1').empty();
        $('#insuranceJumbotron #sel2').empty();
        $('#insuranceJumbotron #sel3').empty();
        $('#insuranceJumbotron #sel4').empty();
        $('#insuranceJumbotron #sel5').empty();
        $('select').prop("disabled", false);
        $('#toggleButton').hide();
        if (midMobile && !xxl) {
            $('#insuranceJumbotron #clearButton').attr('onclick', "none");
            $('#insuranceJumbotron #clearButton').hide();
            $('#insuranceJumbotron #tarifBtn').show();

        } else {
            //$('#insuranceJumbotron #clearButton').attr('onclick', "none");
            $('#insuranceJumbotron #tarifBtn').show();
        }
        $('#clearButton').css('display', 'none');
        $('#clearBackButton').html("zurück").css('padding-top', '7%');
        if (midMobile && xxl && !mobile) {
            $('#clearBackButton').css('display', 'none');
        } else {
            $('#clearBackButton').css('display', '');
        }
        //startTarifRechner();
    });

})();

var vehType = "";
$(document).ready(function() {
    var url = window.location.href;
    var params = url.split('?');

    function check_mobile() {
        is_mobile = window.innerWidth <= 768;
    }

    window.addEventListener('resize', check_mobile);
    check_mobile();

    var usersession = readCookie('usersession');
    usersession = null;
    if (usersession != null) {
        var userData = usersession.split("%26");
        var username = userData[1].replace("%40", "@");
        sessionStorage.setItem("usersession", userData[0]);
        sessionStorage.setItem("username", username);
        username = userData[1].replace("%40", "@");
        postData = {
            "command": "getInputData",
            "username": username,
            "sessionkey": userData[0]
        };
        $.ajax({
            url: "../ajaxtest.php",
            method: "post",
            data: postData,
            success: function(content) {
                data = JSON.parse(content);
                sessionStorage.setItem("prename", data.json.result.prename);
                sessionStorage.setItem("lastname", data.json.result.lastname);
                checkUser();
            }
        });
    }

    if (params.length > 1) {
        var allParams = params[1].split("&");
        var cicleData = allParams[0].split("=");
        var cicleId = parseInt(cicleData[1]);
        if ((allParams.length > 2 && allParams[(allParams.length - 1)] == "lastpage=login") || (allParams.length > 2 && allParams[(allParams.length - 1)] == "lastpage=reg")) {
            params = allParams[2].split("=");
            if (params.length > 1) {
                console.log(params[2]);
                sessionStorage.setItem("username", params[2]);
                sessionStorage.setItem("usersession", params[1]);
                username = params[2];
                usersession = params[1];

                if(allParams[(allParams.length - 1)] == "lastpage=login"){
                    $('#modalMessageBody').html('Sie haben sich erfolgreich eingeloggt. Sie können nun beliebig Berechnungen speichern.');
                }else if(allParams[(allParams.length - 1)] == "lastpage=reg"){
                    $('#modalMessageBody').html('Sie haben sich erfolgreich registiert. Sie können nun beliebig Berechnungen speichern.');
                }
                $('.progress').hide();
                if (window.innerWidth < 415) {
                    computedWidth = window.innerWidth - 20;
                    $('#confirmationOverlay .contentBoxWrapper').css('width', computedWidth);
                } else if (window.innerWidth < 570) {
                    $('#confirmationOverlay .contentBoxWrapper').css('width', 'auto');
                }
                $('body').css('overflow', 'hidden');
                $('#confirmationOverlay').css('display', 'table').fadeIn();
                document.getElementById('closeOverlay').addEventListener("click", function(event) {
                    $('body').css('overflow', 'auto');
                    $('#confirmationOverlay').fadeOut();
                });
            }
        }else if(allParams.length > 2 && allParams[(allParams.length - 1)] == "restart=1" && allParams[0] === "article_id=1"){
            deleteSessionStorage();
        }
        checkUser();
        if (cicleId != 17 && cicleId != 18) {
            sessionStorage.setItem("wayofcicle", "wayofcicle" + cicleData[1]);
        }
        if (allParams[0] === "article_id=32" || allParams[0] === "article_id=38") {
            id = allParams[0].split("=");
            var vehData = "";
            if (id[1] == "32") {
                sessionStorage.removeItem("tarif");
                var allCarData = JSON.parse(sessionStorage.getItem('#sel5'));
                var vehData = sessionStorage.getItem("insuranceID").split(",");
                var carData = "";
                $.each(allCarData.json.result, function(i, v) {
                    if (v.idVehicle == vehData[0]) {
                        carData = v;
                    }
                });
                var carString = "<strong>" + decodeURI(vehData[1] + " " + vehData[2] + ", Baujahr " + vehData[3] + ", " + vehData[4] + ", " + vehData[5] + "") + "</strong>";
                $('#customerCar').html(carString);
                $('._car').html(decodeURI(vehData[1] + " " + vehData[2]));
                $('._carvalue').html(parseInt(carData.vehicleValue).formatMoney(0, ",", "."));
                $('._carbau').html(decodeURI(vehData[3] + ", " + vehData[4] + ", " + vehData[5] + ""));
                if (sessionStorage.getItem("vehicleTypeID") === "202002") {
                    if (is_mobile) {
                        $('#basixBoxMobile').html("Motorrad Komfort");
                        $('#komfortBoxMobile').html("Motorrad Premium");
                    } else {
                        $('#basixBox').html("Motorrad Komfort");
                        $('#komfortBox').html("Motorrad Premium");
                        if (window.innerWidth < 1600) {
                            $('#basixBox').css('font', 'normal 41px/35px "Monserrat-SemiBold"');
                            $('#komfortBox').css('font', 'normal 41px/35px "Monserrat-SemiBold"');
                        } else {
                            $('#basixBox').css('font', 'normal 39px/35px "Monserrat-SemiBold"');
                            $('#komfortBox').css('font', 'normal 39px/35px "Monserrat-SemiBold"');
                        }
                    }
                } else if (sessionStorage.getItem("vehicleTypeID") === "202003") {
                    if (is_mobile) {
                        $('#basixBoxMobile').html("Traktoren Komfort");
                        $('#komfortBoxMobile').html("Traktoren Premium");
                    } else {
                        $('#basixBox').html("Traktoren Komfort");
                        $('#komfortBox').html("Traktoren Premium")
                        if (window.innerWidth < 1600) {
                            $('#basixBox').css('font', 'normal 41px/35px "Monserrat-SemiBold"');
                            $('#komfortBox').css('font', 'normal 41px/35px "Monserrat-SemiBold"');
                        } else {
                            $('#basixBox').css('font', 'normal 39px/35px "Monserrat-SemiBold"');
                            $('#komfortBox').css('font', 'normal 39px/35px "Monserrat-SemiBold"');
                        }
                    }
                }
                var postData = {
                    "command": "premiums",
                    "vehId": vehData[0],
                    "vehValue": carData.vehicleValue
                };

                getPremiums(postData);

                if (allParams[2] == "restart=1#calculator") {
                    //$('a[data-js-action="toggle"]').trigger("click");
                    $('select').prop('disabled', 'disabled');
                    if (is_mobile) {
                        if (isChrome()) {
                            setTimeout(function() {
                                var hash = window.location.hash;
                                window.location.hash = "";
                                window.location.hash = "sel1";
                                $('#toggle38CalcFirstMobile').hide();
                            }, 300);
                        } else {
                            $('#sel5').focus();
                        }
                    }
                }

                startTarifRechner(true);
                checkBusinessHeight();

                var midView = window.innerWidth < 1600;
                var mobile = window.innerWidth < 768;
                var is_safari = navigator.userAgent.indexOf("Safari") > -1;

                if (midView && !mobile) {
                    $('.intro').height(0);
                    $('#clearBackButton').css('margin-top', '10px');
                } else if (midView && mobile) {
                    var xtraMobile = window.innerWidth < 375;
                    if ((isChrome() || is_safari) && !xtraMobile) {
                        $('.intro').css('min-height', '780px');
                        //$('.intro').height('765px');
                    } else if (isChrome() && xtraMobile) {

                        if (window.innerWidth > 359 && window.innerWidth < 375) {
                            $('.intro').height('740px');
                        } else {
                            $('.intro').height('675px');
                        }
                    } else {
                        if (window.innerWidth > 359 && window.innerWidth < 375) {
                            $('.intro').height('740px');
                        } else {
                            $('.intro').height('675px');
                        }
                    }
                    $('#insuranceJumbotron > div > div > div.calculator > div:nth-child(2)').css('width', '100%').css('position', 'relative');
                }
                if (window.innerWidth > 768 && window.innerWidth < 1600) {

                    $('#toggle38CalcFirst2').trigger("click");
                    $('#toggleButton').hide();
                }

                $('a').on("click", function(e) {
                    if (sessionStorage.getItem("tarif") != null) {
                        sessionStorage.removeItem("tarif");
                    }
                    parentId = e.currentTarget.parentElement.className;
                    if (parentId == "basix") {
                        parentId = "Basis";
                    } else if (parentId == "comfort") {
                        parentId = "Komfort";
                    } else if (parentId == "premium") {
                        parentId = "Premium";
                    } else {
                        var params = $(this).attr('href').split("&");
                        if (params.length >= 2) {
                            var tarifArray = params[2].split("=");
                            parentId = tarifArray[1];
                        }
                    }
                    sessionStorage.setItem("tarif", parentId);
                });

            } else if (id[1] == "38") {
                sessionStorage.setItem("offers", "true");
                var randomCarArray = ["17940,Alfa Romeo,Spider (916),2003,Cabriolet 2-türig,239", "4864,Abarth,750 Zagato,1964,Coupé,41"];
                getMultiplePremiums(randomCarArray);
                //$('.notValidHoverButton > td').css("opacity", "0.3");
                if (is_mobile) {
                    $('a[data-js-action="toggle"]').trigger("click");
                    if ((window.innerWidth > 413 && window.innerWidth < 768) && (isChrome() || is_safari)) {
                        $('.carbox').css('height', '150px');
                    }
                } else if (window.innerWidth > 768 && window.innerWidth < 1600) {
                    $('#toggle38CalcFirst2').trigger("click");
                }
            }
        } else if (allParams[0] === "article_id=1" || allParams[0] === "article_id=0") {
            if(sessionStorage.getItem("insuranceID") == null || sessionStorage.getItem("insuranceID") == undefined){
                sessionStorage.removeItem("Fahrzeugdaten2");
                sessionStorage.removeItem("anforderungLi");
                sessionStorage.removeItem("deductible");
                sessionStorage.removeItem("premium");
                sessionStorage.removeItem("Anforderung");
                sessionStorage.removeItem("kindofselbstbehalt");
                sessionStorage.removeItem("kindofselbstbehaltText");
                sessionStorage.removeItem("tarif");
                sessionStorage.removeItem("TarifID");
            }

            startTarifRechner();
        } else if (allParams[0] === "article_id=26") {
            $('.mt p').css('font', 'normal 28px/44px Roboto-Light');
        }
    } else {
        sessionStorage.setItem("wayofcicle", "wayofcicle1");
        checkUser();
        startTarifRechner();
    }
});

function tarifAjax(postData, selector) {
    $.ajax({
        url: "../ajaxtest.php",
        method: "post",
        data: postData,
        success: function(content) {
            if (selector === "#sel1" && !content.match(/json/)) {
                content = "{\"json\":{\"success\":\"true\",\"result\":[{\"ID\":102001,\"name\":\"VN\",\"masterID\":\"102000\",\"masterName\":\"H\\u00E4ufigster Nutzerkreis\"},{\"ID\":102002,\"name\":\"(Ehe-) Partner in h\\u00E4uslicher Gemeinschaft\",\"masterID\":\"102000\",\"masterName\":\"H\\u00E4ufigster Nutzerkreis\"},{\"ID\":102003,\"name\":\"Kinder des VN\",\"masterID\":\"102000\",\"masterName\":\"H\\u00E4ufigster Nutzerkreis\"},{\"ID\":102004,\"name\":\"Familienkreis\",\"masterID\":\"102000\",\"masterName\":\"H\\u00E4ufigster Nutzerkreis\"},{\"ID\":102005,\"name\":\"andere\",\"masterID\":\"102000\",\"masterName\":\"H\\u00E4ufigster Nutzerkreis\"},{\"ID\":105001,\"name\":\"Historisches Kennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105002,\"name\":\"Normales schwarzes Kennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105003,\"name\":\"Saisonkennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105004,\"name\":\"06er Rotes Dauerkennzeichen (H\\u00E4ndler)\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105005,\"name\":\"07er Rotes Dauerkennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105006,\"name\":\"ohne amtl.Kennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105007,\"name\":\"englisches Kennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105008,\"name\":\"franz\\u00F6sisches Kennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105009,\"name\":\"Wechselkennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105010,\"name\":\"Kurzkennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":105011,\"name\":\"Gr\\u00FCnes Kennzeichen\",\"masterID\":\"105000\",\"masterName\":\"Kennzeichenart\"},{\"ID\":106001,\"name\":\"Eigennutzung\",\"masterID\":\"106000\",\"masterName\":\"Verwendungszweck\"},{\"ID\":106002,\"name\":\"Vermietung an Selbstfahrer\",\"masterID\":\"106000\",\"masterName\":\"Verwendungszweck\"},{\"ID\":106003,\"name\":\"Vermietung mit Chauffeur\",\"masterID\":\"106000\",\"masterName\":\"Verwendungszweck\"},{\"ID\":106004,\"name\":\"AlltagsPKW Nein - aber Car-Sharing\",\"masterID\":\"106000\",\"masterName\":\"Verwendungszweck\"},{\"ID\":106005,\"name\":\"AlltagsPKW Nein - aber \\u00D6PNV\",\"masterID\":\"106000\",\"masterName\":\"Verwendungszweck\"},{\"ID\":107001,\"name\":\"ja\",\"masterID\":\"107000\",\"masterName\":\"Leasing\"},{\"ID\":107002,\"name\":\"nein\",\"masterID\":\"107000\",\"masterName\":\"Leasing\"},{\"ID\":108001,\"name\":\"vor mehr als 5 Jahren\",\"masterID\":\"108000\",\"masterName\":\"Kaufjahr\"},{\"ID\":108002,\"name\":\"vor 4 Jahren bis 5 Jahre\",\"masterID\":\"108000\",\"masterName\":\"Kaufjahr\"},{\"ID\":108003,\"name\":\"vor weniger als 3 Jahren\",\"masterID\":\"108000\",\"masterName\":\"Kaufjahr\"},{\"ID\":109001,\"name\":\"abschlie\\u00DFbare Einzel-\\\/Doppelgarage\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109002,\"name\":\"private Sammelgarage bis 10 Fahrzeuge (Eigentum \\\/ angemietet und ohne gewerbliche T\\u00E4tigkeiten) ggf. fragen, wieviele Personen regelm\\u00E4\\u00DFig Zugang haben)\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109003,\"name\":\"private Sammelgarage 11-20 Fahrzeuge (Eigentum oder angemietet und ohne gewerbliche T\\u00E4tigkeiten) ggf. fragen, wieviele Personen regelm\\u00E4\\u00DFig Zugang haben)\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109004,\"name\":\"private Sammelgarage mehr als 20 Fahrzeuge (Eigentum oder angemietet und ohne gewerbliche T\\u00E4tigkeiten) ggf. fragen, wieviele Personen regelm\\u00E4\\u00DFig Zugang haben)\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109005,\"name\":\"H\\u00E4ndler (Ausstellungshalle)\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109006,\"name\":\"reservierter Stellplatz Tief-\\\/ Sammelgarage ohne Gitter-Box\\\/K\\u00E4fig\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109007,\"name\":\"reservierter Stellplatz Tief- \\\/ Sammelgarage mit Gitter-Box\\\/K\\u00E4fig\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109008,\"name\":\"offenes Carport auf eigenem Grundst\\u00FCck\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109009,\"name\":\"Carport mit Sektionaltor auf eigenem Grundst\\u00FCck\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109010,\"name\":\"Reihen-Carport Wohnkomplex\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109011,\"name\":\"offener privater Stellplatz vor Wohnhaus\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109012,\"name\":\"\\u00F6ffentliches Parkhaus\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":109013,\"name\":\"umfriedetes Grundst\\u00FCck\",\"masterID\":\"109000\",\"masterName\":\"Abstellplatz\"},{\"ID\":110001,\"name\":\"bis 1.000 p.a.\",\"masterID\":\"110000\",\"masterName\":\"KM-Leistung\"},{\"ID\":110002,\"name\":\"bis 2.500 p.a.\",\"masterID\":\"110000\",\"masterName\":\"KM-Leistung\"},{\"ID\":110003,\"name\":\"bis 5.000 p.a.\",\"masterID\":\"110000\",\"masterName\":\"KM-Leistung\"},{\"ID\":110004,\"name\":\"bis 10.000 p.a.\",\"masterID\":\"110000\",\"masterName\":\"KM-Leistung\"},{\"ID\":110005,\"name\":\"bis 12.500 p.a.\",\"masterID\":\"110000\",\"masterName\":\"KM-Leistung\"},{\"ID\":110006,\"name\":\"bis 15.000 p.a.\",\"masterID\":\"110000\",\"masterName\":\"KM-Leistung\"},{\"ID\":110007,\"name\":\"bis 17.500 p.a.\",\"masterID\":\"110000\",\"masterName\":\"KM-Leistung\"},{\"ID\":110008,\"name\":\"bis 20.000 p.a.\",\"masterID\":\"110000\",\"masterName\":\"KM-Leistung\"},{\"ID\":110009,\"name\":\"\\u003E 20.000 p.a.\",\"masterID\":\"110000\",\"masterName\":\"KM-Leistung\"},{\"ID\":111001,\"name\":\"Z\\u00DCRS-Zone 1\",\"masterID\":\"111000\",\"masterName\":\"Z\\u00DCRS-Zone\"},{\"ID\":111002,\"name\":\"Z\\u00DCRS-Zone 2\",\"masterID\":\"111000\",\"masterName\":\"Z\\u00DCRS-Zone\"},{\"ID\":112001,\"name\":\"Begleitendes Fahren mit 17\",\"masterID\":\"112000\",\"masterName\":\"Fahreralter\"},{\"ID\":112002,\"name\":\"Begleitendes Fahren bis 22\",\"masterID\":\"112000\",\"masterName\":\"Fahreralter\"},{\"ID\":112003,\"name\":\"Fahrer 4 Jahre FE\",\"masterID\":\"112000\",\"masterName\":\"Fahreralter\"},{\"ID\":112004,\"name\":\"Fahrer zwischen 23 und 25\",\"masterID\":\"112000\",\"masterName\":\"Fahreralter\"},{\"ID\":112005,\"name\":\"alle Fahrer \\u003E 35\",\"masterID\":\"112000\",\"masterName\":\"Fahreralter\"},{\"ID\":112006,\"name\":\"alle Fahrer \\u003E 45\",\"masterID\":\"112000\",\"masterName\":\"Fahreralter\"},{\"ID\":112007,\"name\":\"alle Fahrer \\u003E 55\",\"masterID\":\"112000\",\"masterName\":\"Fahreralter\"},{\"ID\":112008,\"name\":\"Fahrer \\u003E 70\",\"masterID\":\"112000\",\"masterName\":\"Fahreralter\"},{\"ID\":113001,\"name\":\"Rechnung\",\"masterID\":\"113000\",\"masterName\":\"Zahlungsart\"},{\"ID\":113002,\"name\":\"Lastschrift\",\"masterID\":\"113000\",\"masterName\":\"Zahlungsart\"},{\"ID\":114001,\"name\":\"j\\u00E4hrlich\",\"masterID\":\"114000\",\"masterName\":\"Zahlungsweise\"},{\"ID\":114002,\"name\":\"halbj\\u00E4hrlich\",\"masterID\":\"114000\",\"masterName\":\"Zahlungsweise\"},{\"ID\":114003,\"name\":\"viertelj\\u00E4hrlich\",\"masterID\":\"114000\",\"masterName\":\"Zahlungsweise\"},{\"ID\":116001,\"name\":\"ja\",\"masterID\":\"116000\",\"masterName\":\"Garantiesummen\\\/Deckungssummen (KH)\"},{\"ID\":116002,\"name\":\"nein\",\"masterID\":\"116000\",\"masterName\":\"Garantiesummen\\\/Deckungssummen (KH)\"},{\"ID\":117001,\"name\":\"ja\",\"masterID\":\"117000\",\"masterName\":\"Schmerzensgeld\"},{\"ID\":117002,\"name\":\"nein\",\"masterID\":\"117000\",\"masterName\":\"Schmerzensgeld\"},{\"ID\":118003,\"name\":\"M\\u00E4rz\",\"value\":\"3\",\"masterID\":\"118000\",\"masterName\":\"Monat von\"},{\"ID\":118004,\"name\":\"April\",\"value\":\"4\",\"masterID\":\"118000\",\"masterName\":\"Monat von\"},{\"ID\":118005,\"name\":\"Mai\",\"value\":\"5\",\"masterID\":\"118000\",\"masterName\":\"Monat von\"},{\"ID\":118006,\"name\":\"Juni\",\"value\":\"6\",\"masterID\":\"118000\",\"masterName\":\"Monat von\"},{\"ID\":118007,\"name\":\"Juli\",\"value\":\"7\",\"masterID\":\"118000\",\"masterName\":\"Monat von\"},{\"ID\":118008,\"name\":\"August\",\"value\":\"8\",\"masterID\":\"118000\",\"masterName\":\"Monat von\"},{\"ID\":118009,\"name\":\"September\",\"value\":\"9\",\"masterID\":\"118000\",\"masterName\":\"Monat von\"},{\"ID\":118010,\"name\":\"Oktober\",\"value\":\"10\",\"masterID\":\"118000\",\"masterName\":\"Monat von\"},{\"ID\":119004,\"name\":\"April\",\"value\":\"4\",\"masterID\":\"119000\",\"masterName\":\"Monat bis\"},{\"ID\":119005,\"name\":\"Mai\",\"value\":\"5\",\"masterID\":\"119000\",\"masterName\":\"Monat bis\"},{\"ID\":119006,\"name\":\"Juni\",\"value\":\"6\",\"masterID\":\"119000\",\"masterName\":\"Monat bis\"},{\"ID\":119007,\"name\":\"Juli\",\"value\":\"7\",\"masterID\":\"119000\",\"masterName\":\"Monat bis\"},{\"ID\":119008,\"name\":\"August\",\"value\":\"8\",\"masterID\":\"119000\",\"masterName\":\"Monat bis\"},{\"ID\":119009,\"name\":\"September\",\"value\":\"9\",\"masterID\":\"119000\",\"masterName\":\"Monat bis\"},{\"ID\":119010,\"name\":\"Oktober\",\"value\":\"10\",\"masterID\":\"119000\",\"masterName\":\"Monat bis\"},{\"ID\":119011,\"name\":\"November\",\"value\":\"11\",\"masterID\":\"119000\",\"masterName\":\"Monat bis\"},{\"ID\":200001,\"name\":\"150\",\"value\":\"150\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200002,\"name\":\"300\",\"value\":\"300\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200003,\"name\":\"500\",\"value\":\"500\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200004,\"name\":\"1.000\",\"value\":\"1000\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200005,\"name\":\"2.500\",\"value\":\"2500\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200006,\"name\":\"5.000\",\"value\":\"5000\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200007,\"name\":\"7.500\",\"value\":\"7500\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200008,\"name\":\"10.000\",\"value\":\"10000\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200009,\"name\":\"20.000\",\"value\":\"20000\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200010,\"name\":\"30.000\",\"value\":\"30000\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200011,\"name\":\"50.000\",\"value\":\"50000\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200012,\"name\":\"100.000\",\"value\":\"100000\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":200013,\"name\":\"150.000\",\"value\":\"150000\",\"masterID\":\"200000\",\"masterName\":\"Selbstbehalt-Werte\"},{\"ID\":202001,\"name\":\"Fahrzeug\",\"masterID\":\"202000\",\"masterName\":\"Fahrzeuggruppe\"},{\"ID\":202002,\"name\":\"Krad\",\"masterID\":\"202000\",\"masterName\":\"Fahrzeuggruppe\"},{\"ID\":202003,\"name\":\"Traktor\",\"masterID\":\"202000\",\"masterName\":\"Fahrzeuggruppe\"}]},\"html\":\"<option value='0'><\/option><option value='202001,abschlie\\u00DFbare Einzel-\\\/Doppelgarage'>PKW<\/option><option value='202002,abschlie\\u00DFbare Einzel-\\\/Doppelgarage'>Motorr\u00e4der<\/option><option value='202003,abschlie\\u00DFbare Einzel-\\\/Doppelgarage'>Traktoren<\/option>\"}";
            }
            sessionStorage.setItem(selector, content);
            data = JSON.parse(content);
            $(selector).html(data.html);
        },
        error: function(error) {
            console.log('ERROR');
            console.log(postData);
            console.log(selector);
            console.log(error);
        }
    });
}

function getMultiplePremiums(randomCarArray) {
    var randomCarArr = randomCarArray;
    var midXXL = window.innerWidth <= 1600;

    var randomId = Math.floor(Math.random() * 2);

    var firstCar = randomCarArr[randomId].split(",");
    var carValues = ['11.900', '56.000'];
    $('._carvalue').html(carValues[randomId]);
    $('._car').html(decodeURI(firstCar[1] + " " + firstCar[2]));
    $('._carbau').html(decodeURI(firstCar[3] + ", " + firstCar[4] + ", " + firstCar[5] + ""));
    var postData = {
        "command": "premiums",
        "vehId": firstCar[0],
        "vehValue": 10000
    };
    getPremiums(postData);
    var carString = decodeURI(firstCar[1] + " " + firstCar[2] + ", Baujahr " + firstCar[3] + ", " + firstCar[4] + ", " + firstCar[5]);
    if (carString.length > 44 && carString.length < 60 && !is_mobile) {
        carString = "<strong>" + decodeURI(firstCar[1] + " " + firstCar[2] + ", Baujahr " + firstCar[3] + ", " + firstCar[4] + ",<br /> " + firstCar[5]) + "</strong>";
    } else {
        carString = "<strong>" + decodeURI(firstCar[1] + " " + firstCar[2] + ", Baujahr " + firstCar[3] + ", " + firstCar[4] + ", " + firstCar[5]) + "</strong>";
    }

    $('.customerCar').html(carString);
    //var postData = {"command": "premiums", "vehId": firstCar[0], "vehValue": 10000};
    //getPremiums(postData);
    //$('._car').html(decodeURI(firstCar[1] + " " + firstCar[2]));
    //$('._carvalue').html("10.000");
    //$('._carbau').html(decodeURI(firstCar[3] + ", " + firstCar[4] + ", " + firstCar[5] + " PS"));
    checkBusinessHeight();

}

function selectItemByValue(elmnt, value) {
    for (var i = 0; i < elmnt.options.length; i++) {
        //console.log(elmnt.options[i]);
        if (elmnt.options[i].value == value)
            elmnt.selectedIndex = i;
    }
}

function getPremiums(postData) {
    var mobile = window.innerWidth <= 768;
    var xxl = window.innerWidth <= 1954;
    var xxxl = window.innerWidth <= 2560;
    var midXXL = window.innerWidth <= 1880;
    var mid = window.innerWidth <= 1600;
    console.log(postData);
    $.ajax({
        url: "../ajaxtest.php",
        method: "post",
        data: postData,
        success: function(content) {

            var overlayCookie = readCookie("page32Overlay");

            var data = JSON.parse(content);
            if((sessionStorage.getItem("vehicleTypeID") == 202005) || (sessionStorage.getItem("vehicleTypeID") == 202001 && data.premium == undefined) ){
                var newPremium = JSON.parse(JSON.stringify(data.json.result[0]));
                var comfort = data.json.result[0];
                var basic = data.json.result[1];
                var newDeduc = JSON.parse(JSON.stringify(data.json.result[0].deductibles[1]));
                newPremium.insuranceGroup = "Premium";
                newPremium.insuranceId = 10827;
                /*newDeduc.insuranceTypeName = "Allrisk";
                newDeduc.insuranceTypeId = 205004;
                newPremium.deductibles[2] = newDeduc;*/
                data.premium = (newPremium.totalGrossPremium+30);
                data.json.result[0] = newPremium;
                data.json.result[1] = comfort;
                data.json.result[2] = basic;
                content = JSON.stringify(data.json.result);
                sessionStorage.setItem("FakePremium","yes");
            }
            sessionStorage.setItem("getPremiums", content);
            var baseli = "",
                comfortli = "",
                premiumli = "";
            var countSpan = 0;
            var premiumLiArray = [];
            var premiumIdObject = {
                "Premium": 0,
                "Comfort": 1,
                "Basic": 2
            };
            $.each(data.json.result, function(i, v) {
                if (v.insuranceGroup == "Premium") {
                    premiumIdObject.Premium = i;
                    $.each(v.deductibles, function(i, va) {
                        if (va.insuranceTypeName == "Allrisk" && sessionStorage.getItem("vehicleTypeID") != 202005) {
                            premiumLiArray[0] = '<li><h3>' + va.insuranceTypeName + '</h3>mit ' + va.name + ' Euro Selbstbeteiligung Schutz gegen alle Gefahren, die nicht ausdrücklich ausgeschlossen sind, mit Beweislastumkehr zu Gunsten des Versicherten <br /><span style="font-family: Roboto-Bold"> beinhaltet:</span></li>';
                        } else if (va.insuranceTypeName == "Vollkasko") {
                            premiumLiArray[1] = '<li><h3>' + va.insuranceTypeName + '</h3>mit ' + va.name + ' Euro Selbstbeteiligung</li>';
                        } else {
                            premiumLiArray[2] = '<li><h3>' + va.insuranceTypeName + '</h3><span style="font-family: Roboto-Bold">inkl. Vandalismus-Schutz</span><br />mit ' + va.name + ' Euro Selbstbeteiligung</li>';
                        }
                    });
                    premiumLiArray[3] = '<li><h3>Haftpflicht</h3>100 Millionen Euro pauschale Deckungssumme, bis zu 15 Millionen Euro pro geschädigter Person</li>';
                    premiumLiArray[4] = '<li><h3>Fahrerschutz - <br /> Versicherung</h3>bis zu 15 Millionen Euro Schutz für Fahrer des eigenen Fahrzeugs</li>';
                    countSpan++;

                }
                if (v.insuranceGroup == "Comfort") {
                    premiumIdObject.Comfort = i;
                    $.each(v.deductibles, function(i, va) {
                        if (va.insuranceTypeName == "Vollkasko") {
                            comfortli += '<li><h3>' + va.insuranceTypeName + '</h3>mit ' + va.name + ' Euro Selbstbeteiligung</li>';
                        } else {
                            comfortli += '<li><h3>' + va.insuranceTypeName + '</h3><span style="font-family: Roboto-Bold">inkl. Vandalismus-Schutz</span><br />mit ' + va.name + ' Euro Selbstbeteiligung</li>';
                        }
                    });
                    comfortli += '<li><h3>Haftpflicht</h3>100 Millionen Euro pauschale Deckungssumme, bis zu 15 Millionen Euro pro geschädigter Person</li>';
                    countSpan++;
                }
                if (v.insuranceGroup == "Basic") {
                    premiumIdObject.Basic = i;
                    $.each(v.deductibles, function(i, va) {
                        baseli += '<li><h3>' + va.insuranceTypeName + '</h3><span style="font-family: Roboto-Bold">inkl. Vandalismus-Schutz</span><br />mit ' + va.name + ' Euro Selbstbeteiligung</li>';
                    });
                    baseli += '<li><h3>Haftpflicht</h3>100 Millionen Euro pauschale Deckungssumme, bis zu 15 Millionen Euro pro geschädigter Person</li>';
                    countSpan++;
                }
            });
            if (mobile) {
                $(".tarifGroupDesktop").hide();
            }
            if (!data.hasOwnProperty("basic")) {
                $(".basix").css("display", "none");
                $('.col_basis').css("display", "none");
                if (mobile) {
                    $(".basis").css("display", "none");
                }
            } else {
                $('.baseli').html(baseli);
                $('.insurancePrice1').html(data.basic.formatMoney(0, ',', '.') + " €");
                $('.basicHref').attr("href", "?article_id=15&clang=1&tarif=Basis");
                $('.basicHref').on('click', function(e) {
                    e.preventDefault();
                    sessionStorage.setItem("TarifID", 10829);
                    if (data.json.result[premiumIdObject.Basic].calculationParameters[5].ID == 109001) {
                        sessionStorage.setItem("garage", " Abschließbare private Garage, private Tief- oder Sammelgarage mit bis zu 10 Stellplätzen oder privaten, nicht öffentlich zugänglicher Carport.");
                    } else {
                        sessionStorage.setItem("garage", data.json.result[premiumIdObject.Basic].calculationParameters[5].name);
                    }
                    sessionStorage.setItem("garageID", data.json.result[premiumIdObject.Basic].calculationParameters[5].ID);
                    if(overlayCookie != null){
                        document.location.href = "?article_id=15&clang=1&tarif=Basis";
                    }else{
                        showRechnerOverlay("?article_id=15&clang=1&tarif=Basis");
                    }

                });
            }
            if (!data.hasOwnProperty("comfort")) {
                $(".comfort").css("display", "none");
                $('.col_komfort').css("display", "none");
            } else {
                $('.comfortli').html(comfortli);
                $('.insurancePrice2').html(data.comfort.formatMoney(0, ',', '.') + " €");
                $('.komfortHref').attr("href", "?article_id=15&clang=1&tarif=Komfort");
                $('.komfortHref').on('click', function(e) {
                    e.preventDefault();
                    sessionStorage.setItem("TarifID", 10828);
                    if (data.json.result[premiumIdObject.Comfort].calculationParameters[5].ID == 109001) {
                        sessionStorage.setItem("garage", " Abschließbare private Garage, private Tief- oder Sammelgarage mit bis zu 10 Stellplätzen oder privaten, nicht öffentlich zugänglicher Carport.");
                    } else {
                        sessionStorage.setItem("garage", data.json.result[premiumIdObject.Comfort].calculationParameters[5].name);
                    }
                    sessionStorage.setItem("garageID", data.json.result[premiumIdObject.Comfort].calculationParameters[5].ID);
                    if(overlayCookie != null){
                        document.location.href = "?article_id=15&clang=1&tarif=Komfort";
                    }else{
                        showRechnerOverlay("?article_id=15&clang=1&tarif=Komfort");
                    }

                });
            }

            if (data.hasOwnProperty("premium") !== true) {
                $(".premium").css("display", "none");
                $('.col_premium').css("display", "none");
                if (!mobile) {
                    $(".comfort").css("width", '33%');
                    $(".basix").css("width", '33%');
                } else {
                    $('#insuranceJumbotron').css('padding-bottom', '0px');
                }
                $('tarifbox.premium.center').hide();
            } else {
                $('.premiumli').html(premiumLiArray.join(""));
                if (!mobile) {
                    $(".basix").css("width", '33%');
                    $(".basix").css("min-width", '');
                } else {
                    $('#insuranceJumbotron').css('padding-bottom', '0px');
                }
                $(".premium").css("display", "");
                $('.col_premium').css("display", "");
                if(sessionStorage.getItem("vehicleTypeID") == 202005){
                    data.premium = (data.premium+30);
                }
                $('.insurancePrice3').html(data.premium.formatMoney(0, ',', '.') + "&nbsp;€");
                $('.premiumHref').attr("href", "?article_id=15&clang=1&tarif=Premium");
                $('.premiumHref').on('click', function(e) {
                    e.preventDefault();
                    sessionStorage.setItem("TarifID", 10827);
                    if (data.json.result[premiumIdObject.Premium].calculationParameters[5].ID == 109001) {
                        sessionStorage.setItem("garage", " Abschließbare private Garage, private Tief- oder Sammelgarage mit bis zu 10 Stellplätzen oder privaten, nicht öffentlich zugänglicher Carport");
                    } else {
                        sessionStorage.setItem("garage", data.json.result[premiumIdObject.Premium].calculationParameters[5].name);
                    }
                    sessionStorage.setItem("garageID", data.json.result[premiumIdObject.Premium].calculationParameters[5].ID);
                    if(overlayCookie != null){
                        document.location.href = "?article_id=15&clang=1&tarif=Premium";
                    }else{
                        showRechnerOverlay("?article_id=15&clang=1&tarif=Premium");
                    }
                });
            }
            $('#spannedRow').attr("colspan", countSpan);
            checkBusinessHeight();
        }
    });

}

// Check High on Page 32 and 38 only to gell things in a row
function checkBusinessHeight() {
    var url = window.location.href;
    var params = url.split('?');
    var allParams = params[1].split("&");
    var midView = window.innerWidth < 1600;
    var beforeMobile = window.innerWidth > 768;
    var mobile = window.innerWidth < 768;
    var xtraMobile = window.innerWidth < 376;
    var is_safari = navigator.userAgent.indexOf("Safari") > -1;

    if (!midView) {
        $('.intro').height($('#insuranceJumbotron > div > div > div.result.desktop').height());
        if ($('.calculator').attr('display') !== 'none') {
            $('.calculator').height($('#insuranceJumbotron > div > div > div.result.desktop').height());
        }
    } else if (midView && !mobile && beforeMobile) {
        if ($('#insuranceJumbotron > div.calculator').height() !== null) {
            $('.intro').height($('#insuranceJumbotron > div.calculator').height() - 170);
        }
        if (window.innerWidth > 875 && window.innerWidth < 1028) {
            $('#toggleButton').css("font", 'normal 17px/30px "Roboto-Medium"');
            introHeight = $('.intro').height();
            if (introHeight < 590) {
                introHeight = 620;
            }
            $('.intro').css("height", introHeight);
        }
        if (allParams[0] !== "article_id=32") {
            $('.intro').css("min-height", 470);
            $('.intro').css("height", 470);
        }
    } else if (midView && mobile && !beforeMobile) {
        $('.mtb').css('top', '0px');
        $('#insuranceJumbotron').css('padding-bottom', '0px');
        if (allParams[0] !== "article_id=32") {
            console.log(xtraMobile);
            if (!xtraMobile && mobile && isChrome()) {
                $('.intro').css('min-height', '538px');
            } else if (xtraMobile && mobile && isChrome()) {
                $('.intro').css('min-height', '495px');
            } else {
                if (isChrome()) {
                    $('.intro').css('min-height', '730px');
                } else {
                    $('.intro').css('min-height', '510px');
                }
            }
        } else {
            if (!xtraMobile && mobile && isChrome()) {
                $('.intro').css('min-height', '545px');
                if (window.innerWidth > 414 && window.innerWidth < 768) {
                    $('.intro').css('min-height', '735px');
                }
            } else if (xtraMobile && mobile && isChrome()) {
                $('.intro').css('min-height', '470px');
            } else {
                if (isChrome()) {
                    $('.intro').css('min-height', '730px');
                } else {
                    //$('.intro').css('min-height','820px');
                }
            }
        }
    } else if (!midView && !mobile && beforeMobile) {
        $('.intro').height($('#insuranceJumbotron > div > div > div.result.desktop').height());
    } else if (midView && !mobile && !beforeMobile) {
        if (allParams[0] != "article_id=32") {
            $('.intro').css('min-height', '545px');
            $('#insuranceJumbotron .carbox').height("auto");
            $('#tarifeBoxWrapper .tarifbox38').height("auto");
            if (window.innerWidth > 414 && window.innerWidth < 768) {
                $('.intro').css('min-height', '735px');
            }
        } else {
            $('.intro').height("auto");
        }
    }

    window.addEventListener('resize', checkBusinessHeight);
    window.addEventListener('load', checkBusinessHeight);
}



function getUrlVars() {
    var vars = [],
        hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function startTarifRechner(restart = false) {
    $('#tarifBtn').prop('disabled', true);
    var postData = {
        "command": "login"
    };
    tarifAjax(postData, "#sel1");

    $('#sel1').on('change',function() {
        var id = "";
        if(sessionStorage.getItem("vehicleTypeID") != undefined){
            id = sessionStorage.getItem("vehicleTypeID");
        }else{
            id = $(this).val();
        }
        var realId = id;
        sessionStorage.setItem("vehicleTypeID", realId);
        var postData = {
            "command": "group",
            "id": id
        };
        tarifAjax(postData, "#sel2");
        sessionStorage.setItem("garage", "abschließbare Einzel-/Doppelgarage");
    });
    $('#sel2').on('change',function() {
        var id = $(this).val();
        vehType = $("#sel1 :selected").val();
        var jetzt = new Date(),
            j = jetzt.getFullYear();
        var content = "<option></option>";
        for (i = j; i >= 1910; i--) {
            content += "<option value='" + i + "'>" + i + "</option>";
        }
        sessionStorage.setItem("jahreszahlen", content);
        $('#sel3').append(content);
    });
    $('#sel3').on('change',function() {
        var group = $("#sel1").val();
        var realGroup = group.split(",");
        var vehType = $("#sel2 :selected").val();
        var id = $(this).val();
        var postData = {
            "command": "vehType",
            "id": id,
            "vehType": vehType,
            "vehGroupId": realGroup[0]
        };
        tarifAjax(postData, "#sel4");
    });
    $('#sel4').on('change',function() {

        sessionStorage.setItem("#sel4sel", $(this).val());
        var data = $(this).val().split(",");
        var group = $("#sel1").val();
        var realGroup = group.split(",");
        var postData = {
            "command": "vehList",
            "vehModel": data[1],
            "vehClass": data[0],
            "yearofconstruct": $('#sel3 :selected').val(),
            "brand": $("#sel2 :selected").val(),
            "vehGroupId": realGroup[0]
        };
        tarifAjax(postData, "#sel5");
    });
    $('#sel5').on('change',function() {
        var id = $(this).val();
        if (sessionStorage.getItem("wayofcicle") !== "wayofcicle32") {
            sessionStorage.setItem("insuranceID", id);
        }
        $('#tarifBtn').prop('disabled', false);
    });
    console.log("Rechner gestartet");
    if(restart){
        refillDropDowns();
    }
}
// DetailBox auf der Tarifübersicht-Seite // id:32
(function detailBoxes() {
    var $triggers = [].slice.call(document.querySelectorAll('#insuranceJumbotron .open-trigger'));
    var $trigger_all = document.querySelector('#insuranceJumbotron .open-trigger-all');
    if ($triggers.length === 0) return;
    $triggers.forEach(function($trigger) {
        $trigger.addEventListener('click', function(e) {
            e.preventDefault();
            this.parentNode.parentNode.classList.add('open');
            this.style.display = 'none';
            if (document.querySelectorAll('#insuranceJumbotron .tarifbox.open').length == 3) {
                $trigger_all.style.display = 'none';
            }
        });
    });
    $trigger_all.addEventListener('click', function(e) {
        e.preventDefault();
        $triggers.forEach(function(trigger) {
            trigger.click();
        });
    });
})();


// Switch from one opened panel to another (faq, etc.)
function gotoPanel(target) {
    var $t = $(target);
    $t.closest('.panel-group').find('div[aria-expanded=true]').collapse('hide');
    $t.collapse('show');
}

// Layout hacks:
$(function() {
    // - Download underlines alle 1 Pixel hoch.
    $('.mt a').wrapInner('<span/>');

    function setButtonsSameHeight() {
        var $btns = $('.btn, .link_home');
        var maxHeight = 0;
        $btns.each(function(idx, btn) {
            var $btn = $(btn);
            if ($btn.height() > maxHeight) {
                maxHeight = Math.max(maxHeight, $btn.height());
            }
        });
        $btns.each(function(idx, btn) {
            var $btn = $(btn);
            var h = $btn.height();
            if (h > 0) {
                var p = (maxHeight - h) * 0.5;
                if (p > 0) {
                    var $span = $('<span/>');
                    $span.css({
                        display: 'block',
                        paddingTop: Math.floor(p),
                        paddingBottom: Math.ceil(p)
                    });
                    $btn.wrapInner($span);
                }
            }
        });
    }

    function setButtonsSameWidth() {
        var $btns = $('.btn, .link_home');
        var maxWidth = 0;
        $btns.each(function(idx, btn) {
            var $btn = $(btn);
            if ($btn.width() > maxWidth) {
                maxWidth = Math.max(maxWidth, $btn.width());
            }

        });

        $btns.each(function(idx, btn) {
            var $btn = $(btn);
            $btn.css('width', Math.floor(maxWidth) + " !important");
        });

        // Erst ausführen wenn alles absolut endgültig und komplett gerendert ist:
        if (document.readyState === 'complete') {
            //setButtonsSameHeight();
            setButtonsSameWidth();
        } else {
            $(document).on('readystatechange', function() {
                if (document.readyState === 'complete') {
                    //setButtonsSameHeight();
                    setButtonsSameWidth();
                }
            });
        }
    }
});


// needed for Sessionhandling
function readCookie(name) {
    var cookiename = name + "=";
    var ca = document.cookie.split(';');
    console.log("in read Cookie");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(cookiename) == 0) return c.substring(cookiename.length, c.length);
    }

    return null;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

var collapsibleElem, collapsibleElems = {},
    offsetTopPos = 0,
    openElement = null;

function avoidPageScroll() {
    if (openElement == null) {
        openElement = this;
    } else {

        if (this === openElement) {
            openElement = null;
        } else {
            for (key in collapsibleElem) {
                if (collapsibleElem[key] === this) {
                    offsetTopPos = collapsibleElems[key].elemOffset;
                    window.scrollTo(0, offsetTopPos + 20);
                    openElement = this;
                    break;
                }
            }
        }
    }
}

// HTML document has been completely loaded and parsed, without waiting for stylesheets, images, and subframes to finish loading
document.addEventListener("DOMContentLoaded", function(event) {
    collapsibleElem = document.querySelectorAll('h4.panel-title');

    for (var key in collapsibleElem) {
        if (typeof collapsibleElem[key] != 'object') {
            break;
        }

        function setSelectedValue(selectObj, valueToSet) {
            for (var i = 0; i < selectObj.options.length; i++) {
                if (selectObj.options[i].text == valueToSet) {
                    selectObj.options[i].selected = true;
                    return;
                }
            }
        }

        collapsibleElem[key].addEventListener('click', avoidPageScroll);
        collapsibleElems[key] = {
            elemOffset: collapsibleElem[key].offsetTop
        };
    }
});

function isChrome() {
    var isChromium = window.chrome,
        winNav = window.navigator,
        vendorName = winNav.vendor,
        isOpera = winNav.userAgent.indexOf("OPR") > -1,
        isIEedge = winNav.userAgent.indexOf("Edge") > -1,
        isIOSChrome = winNav.userAgent.match("CriOS");

    if (isIOSChrome) {
        return true;
    } else if (
        isChromium !== null &&
        typeof isChromium !== "undefined" &&
        vendorName === "Google Inc." &&
        isOpera === false &&
        isIEedge === false
    ) {
        return true;
    } else {
        return false;
    }
}

function deleteSessionStorage(){
    while (sessionStorage.length > 3) {
        for(var i=0, len=sessionStorage.length; i<len; i++) {
            var key = sessionStorage.key(i);
            if(key != "#sel1" && key != "username" && key != "usersession"){
                sessionStorage.removeItem(key);
            }
        }
    }
}

function delete_cookie( name ) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function deleteUserSessionStorage(){
    while (sessionStorage.length > 1) {
        for(var i=0, len=sessionStorage.length; i<len; i++) {
            var key = sessionStorage.key(i);
            if(key != "#sel1"){
                sessionStorage.removeItem(key);
            }
        }
    }
    var url = window.location.href;
    var params = url.split('?');
    var allParams = params[1].split("&");
    if(allParams[0] != "article_id=1"){
        window.location.href = "?article_id=1&clang=1";
    }
}

function showRechnerOverlay(link){
    if(window.innerWidth < 415){
        computedWidth = window.innerWidth-20;
        $('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
    }else if (window.innerWidth < 570){
        $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
    }
    $('body').css('overflow','hidden');
    $('#confirmationOverlay').show();
    $('#confirmationOverlay').css('display', 'table');
    $('#closeOverlay').on('click', function () {
        $('#confirmationOverlay').hide();
        window.location.href = link;
    });
    $('#closeOverlay').show();
    $('#notShowAgain').on('change', function(){
        if(this.checked){
            setCookie("page32Overlay", "noShow", 365);
        }else{
            delete_cookie("page32Overlay");
        }
    });
}

function refillDropDowns(){
    console.log();
    var vehicleTypeID = decodeURI(sessionStorage.getItem("vehicleTypeID"));

    if(vehicleTypeID == 202001){
        console.log(document.getElementById("sel1").options);
        document.getElementById("sel1").selectedIndex = "1";
        console.log(JSON.stringify(document.getElementById("sel1").options));
    }else if(vehicleTypeID == 202002){
        console.log("if 2");
        document.getElementById("sel1").selectedIndex = 2;
    }else if(vehicleTypeID == 202003){
        console.log("if 3");
        document.getElementById("sel1").selectedIndex = 3;
    }else if(vehicleTypeID == 202004){
        console.log("if 4");
        document.getElementById("sel1").selectedIndex = 4;
    }else if(vehicleTypeID == 202005){
        console.log("if 5");
        document.getElementById("sel1").selectedIndex = 5;
    }


    $('#sel5').on('change', function() {
        var id = $('#sel5 :selected').val();
        sessionStorage.setItem("insuranceID", id);
        $('.rechnerForm').submit();
    });
}