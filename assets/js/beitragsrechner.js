/**
 * Created by Jan Lueders on 09.10.17.
 */
Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
(function tarifRechnerPageCheck(){

    window.history.forward(1);

    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };

    var is_mobile;
    function check_mobile() {
        is_mobile = window.innerWidth <= 768;
    }
    window.addEventListener('resize', check_mobile);
    check_mobile();

    //var driverMatchinArray = { 112005 : "Alle Fahrer zwischen 35 und 76 Jahren",112006 :"Alle Fahrer zwischen 45 und 76 Jahren",112007 : "Alle Fahrer zwischen 55 und 76 Jahren",112009 : "Alle Fahrer zwischen 25 und 76 Jahren"};
    var driverMatchinArray = {112004: "Alle Fahrer mindestens 23 und jünger 76 Jahren",112005 : "Alle Fahrer mindestens 35 und jünger 76 Jahren",
              112006 :"Alle Fahrer mindestens 45 und jünger 76 Jahren",
              112007 : "Alle Fahrer mindestens 55 und jünger 76 Jahren",
              112009 : "Alle Fahrer mindestens 25 und jünger 76 Jahren"};
    var errorMessages = {"ORA-20100": "Es besteht bereits ein Vertrag mit diesen Daten und EVB Nummer"};

    var url = window.location.href;
    var params = url.split('?');
    var allParams = params[1].split("&");
    var headLineString = "";
    if(allParams[0] == "article_id=15"){

        window.onload = function () {
            if (typeof history.pushState === "function") {
                history.pushState("jibberish", null, null);
                window.onpopstate = function () {
                    history.pushState('newjibberish', null, null);
                };
            }
            else {
                var ignoreHashChange = true;
                window.onhashchange = function () {
                    if (!ignoreHashChange) {
                        ignoreHashChange = true;
                        window.location.hash = Math.random();
                    }
                    else {
                        ignoreHashChange = false;
                    }
                };
            }
        };

        var matchingObject = {100001:"Alle Fahrer mindestens 25 und jünger 76 Jahre",100002: "Alle Fahrer mindestens 23 und jünger 76 Jahre"};
        var carData = JSON.parse(sessionStorage.getItem("#sel5"));
        if(carData.json.result[0].vehicleClassID == 100001 || carData.json.result[0].vehicleClassID == 100002){
            $('#driversAge').html("Alle Fahrer mindestens 25 und jünger 76 Jahre");

        }else{
            $('#driversAge').html("Alle Fahrer mindestens 23 und jünger 76 Jahren");
        }

        if(sessionStorage.getItem("Anforderung") != null){
            if(sessionStorage.getItem("Anforderungen") === "nichterfüllt"){
                $('#beitragsrechner_step1 button').parent().hide();
                $('#beitragsrechner_step1 #notice').show();
            }else{
                $('#beitragsrechner_step1 button').parent().show();
                $('#beitragsrechner_step1 #notice').hide();
                $('#beitragsrechner_step1 .radio_yes').attr("checked","checked");
            }
        }
        $('#forwardButton').css("display",'none');
        $('input[name="optradio"]').on('click',function(e){
            id = $(this).attr("id");
            if(id == "radioBtnActive"){
                $('#forwardButton').css("display",'inline-block');
            }else{
                $('#forwardButton').css("display",'none');
            }
        });

        carport = sessionStorage.getItem("garage");
        var carportString = carport.replace("\\/", "").replace("\\u00DF","ß");
        $('#carport').html(" "+carportString);
        vehData = sessionStorage.getItem("insuranceID").split(",");

        var tarif = [];
        if(allParams.length > 1){
            var tarif = allParams[2].split("=");
            if(sessionStorage.getItem("tarif") != null){
                tarif[1] = sessionStorage.getItem("tarif");
            }else{
                sessionStorage.setItem("tarif",tarif[1]);
            }
        }else{
            tarif[1] = sessionStorage.getItem("tarif");
        }


        if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
            if(tarif[1] == "Basis" || tarif[1] == "Basic"){
                tarif[1] = "Komfort";
            }else if(tarif[1] == "Komfort" || tarif[1] == "Comfort"){
                tarif[1] = "Premium";
            }
        }
        if(vehData[4] == " "){
            headLineString = decodeURI(tarif[1]+"-Tarif-Angebot für meinen "+vehData[1]+" "+vehData[2]+" von Baujahr "+vehData[3]+" mit"+vehData[5]);
        }else{
            headLineString = decodeURI(tarif[1]+"-Tarif "+vehData[1]+" "+vehData[2]+", Baujahr "+vehData[3]+", "+vehData[4]+", "+vehData[5]);
        }
        $('#headlineData').html(headLineString);

    }else if(allParams[0] == "article_id=19"){
        includeJS('assets/js/19.js');
    }else if(allParams[0] == "article_id=20"){
        includeJS('assets/js/20.js');
    }else if(allParams[0] === "article_id=21"){
        includeJS('assets/js/21.js');
    }else if(allParams[0] === "article_id=22"){
        includeJS('assets/js/22.js');
    }else if(allParams[0] === "article_id=23"){
        includeJS('assets/js/23.js');
    }else if(allParams[0] === "article_id=18"){

        if(allParams.length > 1){
          console.log(allParams[2]);
          if(allParams[2] == "login=exists"){
            if(window.innerWidth < 415){
                        var computedWidth = window.innerWidth-20;
                        $('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
                    }else if (window.innerWidth < 570){
                        $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
                    }
                    $('#modalMessageBody').html("Sie haben bereits einen Account bei CARISMA. Bitte gehen Sie zur Login-Seite");
                    $('#confirmationOverlay > div > div > div > div').hide();
                    $('#confirmationOverlay').show();
                    $('#confirmationOverlay').css('display','table');
                    $('#closeOverlay').on('click', function(){
                sessionStorage.setItem("tmp-email",$('input[name="e-mail-adresse"]').val().replace(" ","").replace("%20",""));
                            $('#confirmationOverlay').hide();
                window.location.href ="?article_id=17&clang=1";
                    });
          }
        }

        if(sessionStorage.getItem("wayofcicle") !== null){
            $('#redirectfield').val(sessionStorage.getItem("wayofcicle"));
            var siteId = sessionStorage.getItem("wayofcicle").replace("wayofcicle","");
            $('.link_back').attr('href','?article_id='+siteId+'&amp;clang=1');
        }

        if(sessionStorage.getItem("wayoforiginalcicle") !== null){
            $('#redirectfield').val(sessionStorage.getItem("wayoforiginalcicle"));
            var siteId = sessionStorage.getItem("wayofcicle").replace("wayoforiginalcicle","");
            $('.link_back').attr('href','?article_id='+siteId+'&amp;clang=1');
        }

        $('input[name="e-mail-adresse"]').focusout(function(){
            sessionStorage.setItem("username",$('input[name="e-mail-adresse"]').val());
        });
        $('form[name="registerForm"]').on("submit", function(e){
            e.preventDefault();
            var username = $('input[name="e-mail-adresse"]').val();
            sessionStorage.setItem("username",username.replace(" ","").replace("%20",""));
            sessionStorage.setItem("vorname",$('input[name="vorname"]').val().replace(" ","").replace("%20",""));
            sessionStorage.setItem("nachname",$('input[name="nachname"]').val().replace(" ","").replace("%20",""));
        });
    }else if(allParams[0] === "article_id=17"){
      if(sessionStorage.getItem("tmp-email") != undefined){
        $('#emailadresse').val(sessionStorage.getItem("tmp-email"));
      }
        if(sessionStorage.getItem("triedLogin") == null){
            sessionStorage.setItem("triedLogin",0);
        }
        if (sessionStorage.getItem("triedLogin") >= 2){
            $('.g-recaptcha').show();
        }
        if(allParams[2] == "login=false"){
            if(sessionStorage.getItem("triedLogin") >= 2){
                $('.g-recaptcha').show();
            }
            if(window.innerWidth < 415){
                console.log(window.innerWidth-20);
                computedWidth = window.innerWidth-20;
                    $('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
                }else if (window.innerWidth < 570){
                    $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
                }
            $('body').css('overflow','hidden');
            $('#confirmationOverlay').css('display','table').fadeIn();
            document.getElementById('closeOverlay').addEventListener("click", function(event) {
                $('body').css('overflow','auto');
                  $('#confirmationOverlay').fadeOut();
          });
        }

        if(sessionStorage.getItem("wayofcicle") !== null){
            $('#redirectfield').val(sessionStorage.getItem("wayofcicle"));
            var siteId = sessionStorage.getItem("wayofcicle").replace("wayofcicle","");
            $('.link_back').attr('href','?article_id='+siteId+'&amp;clang=1');
        }

        if(sessionStorage.getItem("wayoforiginalcicle") !== null){
            $('#redirectfield').val(sessionStorage.getItem("wayoforiginalcicle"));
            var siteId = sessionStorage.getItem("wayofcicle").replace("wayoforiginalcicle","");
            $('.link_back').attr('href','?article_id='+siteId+'&amp;clang=1');
        }

        $('input[name="e-mail-adresse"]').focusout(function(){
            sessionStorage.setItem("username",$('input[name="e-mail-adresse"]').val());
        });

        $('#loginBtn').on('click', function(){
            countLogin = parseInt(sessionStorage.getItem("triedLogin"));
              sessionStorage.setItem("triedLogin",countLogin+1);
            if(sessionStorage.getItem("triedLogin") == null){
                sessionStorage.setItem("triedLogin",1);
            }
            if (sessionStorage.getItem("triedLogin") >= 2){
                $('.g-recaptcha').show();
            }

            if(sessionStorage.getItem("triedLogin") >= 2 && $('.g-recaptcha').css('display') !== "none" && grecaptcha.getResponse() !== ""){
                $('form[name="loginWindowForm"]').submit();
            }else if(sessionStorage.getItem("triedLogin") < 2){
                $('form[name="loginWindowForm"]').submit();
            }
        });

        $('form[name="loginWindowForm"]').on("submit", function(e){
            sessionStorage.setItem("username",$('input[name="e-mail-adresse"]').val());
        });
    }else if(allParams[0] === "article_id=36"){

        t = new Date();
        today = t.getDate()+"."+(t.getMonth()+1)+"."+t.getFullYear();
        $('#todayDate').html("Datum: "+today);

        window.onload = disableBack();
        window.onpageshow = function(evt) { if (evt.persisted) disableBack() }

        fahrzeugdaten = JSON.parse(sessionStorage.getItem("Fahrzeugdaten2"));
        if(fahrzeugdaten["grund_der_versicherung"] == "change"){
            $('#change').show();
            $("#new").hide();
        }
        tarif = sessionStorage.getItem("tarif");
        var salutation = sessionStorage.getItem("anrede");
        lastname = sessionStorage.getItem("lastname");
        evbNumber = sessionStorage.getItem("evbNumber");
        validUntil = sessionStorage.getItem("validUntil");
        if(salutation == "Herr"){
            salutation = "r "+salutation;
        }else{
            salutation = " "+salutation;
        }
        $('#salutation').html(salutation);
        $("#lastname").html(lastname);
        $(".evbNumber").html(evbNumber);
        $("#gueltigbis").html(validUntil);
        $('#gueltigvon').html(getDate());
        deleteSessionStorage();

    }else if(allParams[0] === "article_id=37"){

        $('#resendBtn').on('click', function(){
            email = $('input[name="e-mail-adresse"]').val();
            postData = {"resendformname": "resendformname","e-mail-adresse": email};
            $.ajax({
                url: "../login.php",
                data: postData,
                method: "post",
                success: function(json){
                    data = JSON.parse(json);
                    if(data.success){
                        $('form[name="passlostWindowForm"]').css("display","none");
                        $('#thankyoudiv').css('display','block');
                    }
                }
            });
        });
    }
})();

function disableBack() { window.history.forward() }

function dumpSessionStorageToVar(){
    var storageObject = {};

    for(var i=0, len=sessionStorage.length; i<len; i++) {
        var key = sessionStorage.key(i);
        var value = sessionStorage[key];
        storageObject[key] = value;
    }
    return storageObject;
}

function deleteSessionStorage(){

    for(var i=0, len=sessionStorage.length; i<len; i++) {
        var key = sessionStorage.key(i);
        if(key != "username" && key !== "usersession"){
            sessionStorage.removeItem(key);
        }
    }
}

$.fn.serializeAssoc = function() {
    var data = {};
    $.each( this.serializeArray(), function( key, obj ) {
        var a = obj.name.match(/(.*?)\[(.*?)\]/);
        if(a !== null)
        {
            var subName = a[1];
            var subKey = a[2];

            if( !data[subName] ) {
                data[subName] = [ ];
            }

            if (!subKey.length) {
                subKey = data[subName].length;
            }

            if( data[subName][subKey] ) {
                if( $.isArray( data[subName][subKey] ) ) {
                    data[subName][subKey].push( obj.value );
                } else {
                    data[subName][subKey] = [ ];
                    data[subName][subKey].push( obj.value );
                }
            } else {
                data[subName][subKey] = obj.value;
            }
        } else {
            if( data[obj.name] ) {
                if( $.isArray( data[obj.name] ) ) {
                    data[obj.name].push( obj.value );
                } else {
                    data[obj.name] = [ ];
                    data[obj.name].push( obj.value );
                }
            } else {
                data[obj.name] = obj.value;
            }
        }
    });
    return data;
};

function getDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    today = dd + '.' + mm + '.' + yyyy;
    return today;
}

function getPremiums(postData) {
    $.ajax({
        url: "../ajaxtest.php",
        method: "post",
        data: postData,
        success: function (content) {
            var data = JSON.parse(content);
            if(sessionStorage.getItem("vehicleTypeID") == 202005){
                var newPremium = JSON.parse(JSON.stringify(data.json.result[0]));
                var comfort = data.json.result[0];
                var basic = data.json.result[1];
                var newDeduc = JSON.parse(JSON.stringify(data.json.result[0].deductibles[1]));
                newPremium.insuranceGroup = "Premium";
                newPremium.insuranceId = 10827;
                /*newDeduc.insuranceTypeName = "Allrisk";
                newDeduc.insuranceTypeId = 205004;
                newPremium.deductibles[2] = newDeduc;*/
                data.premium = newPremium.totalGrossPremium;
                data.json.result[0] = newPremium;
                data.json.result[1] = comfort;
                data.json.result[2] = basic;
                content = JSON.stringify(data.json.result);
            }
            sessionStorage.setItem("getPremiums",content);
            var premiumheader = "",comfortheader = "",basicheader = "";
            var baseli = "",comfortli = "",premiumli = "";
            $.each(data.json.result, function(i,v){
                if(v.insuranceGroup === "Premium"){
                    var premiumLiArray = [];
                    premiumheader += '<th class="col_premium">Premium</th>';
                    $.each(v.deductibles, function(i,va){
                        if (va.insuranceTypeName == "Allrisk" && sessionStorage.getItem("vehicleTypeID") != 202005) {
                            premiumLiArray[0] = '<li><h3>' + va.insuranceTypeName + '</h3>mit ' + va.name + ' Euro Selbstbeteiligung Schutz gegen alle Gefahren, die nicht ausdrücklich ausgeschlossen sind, mit Beweislastumkehr zu Gunsten des Versicherten <br /><span style="font-family: Roboto-Bold"> beinhaltet:</span></li>';
                        } else if (va.insuranceTypeName == "Vollkasko") {
                            premiumLiArray[1] = '<li><h3>' + va.insuranceTypeName + '</h3>mit ' + va.name + ' Euro Selbstbeteiligung</li>';
                        } else {
                            premiumLiArray[2] = '<li><h3>' + va.insuranceTypeName + '</h3><span style="font-family: Roboto-Bold">inkl. Vandalismus-Schutz</span><br />mit ' + va.name + ' Euro Selbstbeteiligung</li>';
                        }
                    });
                    premiumli = premiumLiArray.join("");
                    i++;
                }
                if(v.insuranceGroup === "Comfort"){
                    comfortheader += '<th class="col_komfort">Komfort</th>';
                    $.each(v.deductibles, function(i,va){
                        comfortli += '<li><h3>'+va.insuranceTypeName+'</h3><strong>inkl. Vandalismus-Schutz</strong><br />mit '+va.name+' Euro Selbstbeteiligung</li>';
                    });
                    i++;
                }
                if(v.insuranceGroup === "Basic"){
                    basicheader += '<th class="col_basis">Base</th>';
                    $.each(v.deductibles, function(i,va){
                        baseli += '<li><h3>'+va.insuranceTypeName+'</h3><strong>inkl. Vandalismus-Schutz</strong><br />mit '+va.name+' Euro Selbstbeteiligung</li>';
                    });
                }
            });
            if(!data.hasOwnProperty("basic")){
                $(".basix").css("display","none");
                $('#spannedRow').attr("colspan",data.length);
            }else{
                $('#baseli').append(baseli);
                $('#insurancePrice1').html(data.basic.formatMoney(2, ',', '.')+"€");
            }
            if(!data.hasOwnProperty("comfort")){
                $(".comfort").css("display","none");
                $('#spannedRow').attr("colspan",data.length);
            }else{
                $('#comfortli').append(comfortli);
                $('#insurancePrice2').html(data.comfort.formatMoney(2, ',', '.')+"€");
            }
            if(!data.hasOwnProperty("premium")){
                $(".premium").css("display","none");
                $('#spannedRow').attr("colspan",data.length);
            }else{
                $('#premiumli').append(premiumli);
                if(sessionStorage.getItem("vehicleTypeID") == 202005){
                    data.premium = (data.premium+30);
                }
                $('#insurancePrice3').html(data.premium.formatMoney(2, ',', '.')+"€");
            }
        }
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function includeJS(incFile) {
    document.write('<script type="text/javascript" src="'+ incFile+ '"></script>');
}

function tausender(zahl) {
    var i;
    var j=0;
    var ergebnis="";

    i=zahl.length;
    while (i >= 0) {
        ergebnis=zahl.substr(i,1)+ergebnis;
        j++;
        if (j==4) {
            ergebnis="."+ergebnis;
            j=0;
        }
        i--;
    }
    return ergebnis;
}

function onChangeMacOs(selectorElement){
    var isMacLike = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i)?true:false;
    if(isMacLike){
        console.log("Value "+selectorElement.value);
        if(selectorElement.id !== "erstzulassung" && selectorElement.id !== "erstzulassung_monat" && selectorElement.id !== "erstzulassung_tag"){
            if(selectorElement.value !== ""){
                $('#'+selectorElement.id).removeClass('has-error');
                $('#'+selectorElement.id).addClass('has-no-error');
                $('#'+selectorElement.id+"-error").remove();
            }else{
                $('#'+selectorElement.id).removeClass('has-no-error');
            }
        }else{
            var today = new Date();
            var twentyNineYears = new Date(today.getFullYear() - 30, today.getMonth(), 1, 0, 0, 0, 0);
            var twentyEightYears = new Date(today.getFullYear() - 28, today.getMonth(), 1, 0, 0, 0, 0);
            var cNumberObject = {"JAN": "0","FEB":"1","MAR":"2","APR":"3","MAY":"4","JUN":"5","JUL":"6","AUG":"7","SEP":"8","OCT":"9","NOV":"10","DEC":"11"};
            var erstzulassung = $('#erstzulassung :selected').val();
            var erstzulassung_monat = $('#erstzulassung_monat :selected').val();
            var erstzulassung_tag = $('#erstzulassung_tag :selected').val();
            var compareDate = new Date(erstzulassung, cNumberObject[erstzulassung_monat], erstzulassung_tag, 0, 0, 0, 0);

            if(compareDate > twentyEightYears){
                $('#historischesKennzeichen').attr('disabled',true);
                $('#historischesKennzeichenSaison').attr('disabled',true);
            }else if (compareDate <= twentyNineYears){
                console.log("in else if");
                $('#historischesKennzeichen').attr('disabled',false);
                $('#historischesKennzeichenSaison').attr('disabled',false);
            }

            if(selectorElement.id == "erstzulassung"){
                if(selectorElement.value !== "" && $('#erstzulassung_monat :selected').val() == ""){
                    $('#'+selectorElement.id).css('border', '1px solid #4DA42C');
                }else if(selectorElement.value !== "" && $('#erstzulassung_monat :selected').val() !== ""){
                    $('#'+selectorElement.id).css('border', '1px solid #4DA42C');
                    $('#'+selectorElement.id).addClass('has-no-error');
                    $('#erstzulassung_monat').css('border', '1px solid #4DA42C');
                }else if(selectorElement.value === ""){
                    $('#'+selectorElement.id).css('border', '1px solid #CCC');
                    $('#'+selectorElement.id).removeClass('has-no-error');
                }
            }else if(selectorElement.id == "erstzulassung_monat"){
                if(selectorElement.value !== "" && $('#erstzulassung :selected').val() == ""){

                    $('#'+selectorElement.id).css('border', '1px solid #4DA42C');
                }else if(selectorElement.value !== "" && $('#erstzulassung :selected').val() !== ""){

                    $('#'+selectorElement.id).css('border', '1px solid #4DA42C');
                    $('#'+selectorElement.id).addClass('has-no-error');
                    $('#erstzulassung').css('border', '1px solid #4DA42C');
                }else if(selectorElement.value === ""){

                    $('#'+selectorElement.id).css('border', '1px solid #CCC');
                    $('#'+selectorElement.id).removeClass('has-no-error');
                }
            }else if(selectorElement.id == "erstzulassung_tag"){
                if(selectorElement.value !== "" && $('#erstzulassung :selected').val() == "" && $('#erstzulassung_monat :selected').val() == ""){

                    $('#'+selectorElement.id).css('border', '1px solid #4DA42C');
                }else if(selectorElement.value !== "" && $('#erstzulassung :selected').val() !== "" && $('#erstzulassung_monat :selected').val() !== ""){

                    $('#'+selectorElement.id).css('border', '1px solid #4DA42C');
                    $('#'+selectorElement.id).addClass('has-no-error');
                    $('#erstzulassung').css('border', '1px solid #4DA42C');
                }else if(selectorElement.value === ""){

                    $('#'+selectorElement.id).css('border', '1px solid #CCC');
                    $('#'+selectorElement.id).removeClass('has-no-error');
                }
            }
        }
    }
}
