/**
 * Created by root on 15.11.17.
 */
window.onload = function () {
	if (typeof history.pushState === "function") {
	    history.pushState("jibberish", null, null);
	    window.onpopstate = function () {
	        history.pushState('newjibberish', null, null);           
	    };
	}
	else {
	    var ignoreHashChange = true;
	    window.onhashchange = function () {
	        if (!ignoreHashChange) {
	            ignoreHashChange = true;
	            window.location.hash = Math.random();                
	        }
	        else {
	            ignoreHashChange = false;   
	        }
	    };
	}
};

(function () {

    checkCalculationCount();

	tarifIdObject = {"BasisBtn": 10829,"ComfortBtn":10828,"PremiumBtn":10827};
	
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	var is_mobile = window.innerWidth <= 768;
	if(isChrome){
		$('.upgrade_btn').css('margin-left','10%');
	}else{
		$('.upgrade_btn').css('margin-left','8%');
	}
	
    var totalGrossPremium = 0;
    var grossPremiumComprehensive = 0;
    var grossPremiumLiability = 0;
    var name = "";
    var data = "";
    var PremiumData = "";
    var ComfortData ="";
    var BasicData = "";

    var tarif = sessionStorage.getItem("tarif");
    var vehicleTypeID = sessionStorage.getItem("vehicleTypeID");
    if(tarif === "Basic"){
    	tarif = "Basis";
    }
    if(tarif === "Comfort"){
    	tarif = "Komfort";
    }
    $('#wizard1').attr("href", "?article_id=32&clang=1&restart=1");
    $('#wizard2').attr("href", "?article_id=19&clang=1&tarif=" + tarif);
    var vehData = sessionStorage.getItem("insuranceID").split(",");
    var headLineString = "Ihr individuelles CARISMA "+tarif+"-Tarif Paket";
    if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
    	if(tarif == "Basis" || tarif == "Basic"){
    		headLineString = "Ihr individuelles CARISMA Komfort-Tarif Paket";
    	}else if(tarif == "Komfort" || tarif == "Comfort"){
    		headLineString = "Ihr individuelles CARISMA Premium-Tarif Paket";
    	}
    }
    $('#headlineData').html(headLineString);
    $("#teilkasko").hide();
    $("#vollkasko").hide();
    $("#allrisk").hide();
    $("#allriskMobile").hide();
    $("#Comfort").hide();
    $("#Premium").hide();
    $('#ComfortMobile').hide();
    $('#PremiumMobile').hide();
    var url = window.location.href;
    var params = url.split('?');
    var allParams = params[1].split("&");
    if (sessionStorage.getItem("usersession") !== null) {
        $('form[name="tarifberechnung"]').attr('action', '?article_id=21&clang=1&session=' + sessionStorage.getItem("usersession"));
        $('form[name="tarifberechnung_mobil"]').attr('action', '?article_id=21&clang=1&session=' + sessionStorage.getItem("usersession"));
    }else if(params.length > 1){
    	if ((allParams.length > 2 && allParams[(allParams.length - 1)] == "lastpage=login") || (allParams.length > 2 && allParams[(allParams.length - 1)] == "lastpage=reg")) {
            urlParams = allParams[2].split("=");
            if (urlParams.length > 1) {
                sessionStorage.setItem("username", urlParams[2]);
                username = urlParams[2];
                sessionStorage.setItem("usersession", urlParams[1]);
                usersession = urlParams[1];
                
                $('form[name="tarifberechnung"]').attr('action', '?article_id=21&clang=1&session=' + sessionStorage.getItem("usersession"));
                $('form[name="tarifberechnung_mobil"]').attr('action', '?article_id=21&clang=1&session=' + sessionStorage.getItem("usersession"));
            }
        }
    }
    carRawData = JSON.parse(sessionStorage.getItem("#sel5"));
    var data = JSON.parse(window.sessionStorage.getItem("Fahrzeugdaten2"));
    //data["marktwert"] = data["marktwert_tausend"]+data["marktwert_hundert"];
    kaskoText = sessionStorage.getItem("kinofselbstbehaltText").split(",");
    if(kaskoText.length == 1){
    	kaskoText[0] = kaskoText[0].replaceAll("SB", "").replaceAll("Teilkasko", "");
    	$('.teilkaskoPrice').html(kaskoText[0]);
    	if(is_mobile){
    		$('#teilkaskoMobile').show();
    	}else{
    		$('#teilkasko').show();
    	}
    }else if(kaskoText.length == 2){
    	kaskoText[0] = kaskoText[0].replaceAll("SB", "").replaceAll("Vollkasko", "");
    	kaskoText[1] = kaskoText[1].replaceAll("SB", "").replaceAll("Teilkasko", "");
    	$('.teilkaskoPrice').html(kaskoText[1]);
    	$('.vollkaskoPrice').html(kaskoText[0]);
    	if(is_mobile){
    		$('#teilkaskoMobile').show();
        	$('#vollkaskoMobile').show();
    	}else{
    		$('#teilkasko').show();
        	$('#vollkasko').show();
    	}
    }else if(kaskoText.length == 3){
    	kaskoText[0] = kaskoText[0].replaceAll("SB", "").replaceAll("Allrisk", "");
    	kaskoText[1] = kaskoText[1].replaceAll("SB", "").replaceAll("Vollkasko", "");
    	kaskoText[2] = kaskoText[2].replaceAll("SB", "").replaceAll("Teilkasko", "");
    	$('.teilkaskoPrice').html(kaskoText[2]);
    	$('.vollkaskoPrice').html(kaskoText[1]);
    	$('.allRiskPrice').html(kaskoText[0]);
    	if(is_mobile){
    		$('#teilkaskoMobile').show();
        	$('#vollkaskoMobile').show();
        	$('#allriskMobile').show();
    	}else{
    		$('#teilkasko').show();
        	$('#vollkasko').show();
        	$('#allrisk').show();
    	}
    }

    if(data.saison_start != "0" && data.saison_ende != "0"){

        var monthObject = {"01":"Januar", "02":"Februar","03":"März","04":"April","05":"Mai","06":"Juni","07":"Juli","08":"August","09":"September","10":"Oktober","11":"November","12":"Dezember"};

        $('#saisonstart').html(monthObject[data.saison_start]);
        $('#saisonende').html(monthObject[data.saison_ende]);
        $('#saisonprice').show();
    }

    var baseTarifID = sessionStorage.getItem("TarifID");
    var insuranceData = JSON.parse(sessionStorage.getItem("getPremiums"));
    tarifIds = [];

    var kilometerNeeded = "no";

    if(sessionStorage.getItem("vehicleTypeID") == 202005){
        $.each(insuranceData, function (i, v) {
            tarifIds.push({"name": v.insuranceGroup, "value": v.insuranceGroupID, "deductible": v.deductibles});
        });
        $.each(insuranceData, function(i,v){
            if(v.calculationParameters[6].ID == 110002 && v.insuranceGroupID == baseTarifID ){
                kilometerNeeded = "yes";
            }
        });
    }else{

        var resData = null;
        if(insuranceData.json == undefined){
            resData = insuranceData;
        }else{
            resData = insuranceData.json.result;
        }

        $.each(resData, function (i, v) {
            tarifIds.push({"name": v.insuranceGroup, "value": v.insuranceGroupID, "deductible": v.deductibles});
            if(v.calculationParameters[6].ID == 110002 && v.insuranceGroupID == baseTarifID ){
                kilometerNeeded = "yes";
            }
        });
    }

    if(sessionStorage.getItem("FakePremium") != undefined){
        kilometerNeeded = "yes";
    }

    if (vehicleTypeID == 202002 || vehicleTypeID == 202003) {
        $('#fahrerschutzversicherungsDiv').hide();
    }else if(baseTarifID == "10827" || baseTarifID == 10827){
    	$('#mit_fahrschutz').prop("checked","checked");
    }
    if(vehicleTypeID == 202001 && parseInt(vehicleTypeID) == 100001){
		$('#fahrerschutzversicherungsDiv').hide();
	}
    
    var postData = {"command": "premium", 
    				"premiumData": data, 
    				"deductible": tarifIds, 
    				"vehicleId": vehData[0],
    				"vehicleClassID": vehicleTypeID,
    				"kilometerNeeded": kilometerNeeded,
    				"baseTarifID":baseTarifID};

    sortArray = ["Basic", "Comfort", "Premium"];
    $('#progressModal').modal('show');
    $.ajax({
        url: "../ajaxtest.php",
        method: "post",
        data: postData,
        success: function (content) {

            var data = JSON.parse(content);
            if(sessionStorage.getItem("vehicleTypeID") == 202005 || (sessionStorage.getItem("FakePremium") == "yes" && sessionStorage.getItem("tarif") === "Premium") ){
                var newData = JSON.parse(JSON.stringify(data));
                newData.json.Premium = JSON.parse(JSON.stringify(data.json.Comfort));
                console.log(newData.json.Premium);
                newData.json.Premium.result[0].insuranceGroup = "Premium";
                content = JSON.stringify(newData);
                data = JSON.parse(JSON.stringify(newData));
            }
            sessionStorage.setItem("premium", content);

            tarifName = sessionStorage.getItem("tarif");
            if (tarifName == "Komfort") {
                tarifName = "Comfort";
            } else if (tarifName == "Basis") {
                tarifName = "Basic";
            }
            //console.log(tarifName);
            var BasicData = undefined;
            var ComfortData = undefined;
            var PremiumData = undefined;
            if(data.json.hasOwnProperty("Premium")){
            	if(data.json.Premium.success != "false"){
                	PremiumData = data.json.Premium;
                }else{
                	PremiumData = undefined;
                }
            }
            if(data.json.hasOwnProperty("Comfort")){
            	if(data.json.Comfort.success != "false"){
                	ComfortData = data.json.Comfort;
                }else{
                	ComfortData = undefined;
                }
            }
            if(data.json.hasOwnProperty("Basic")){
            	if(data.json.Basic.success != "false"){
                	BasicData = data.json.Basic;
                }else{
                	BasicData = undefined;
                }
            }
            
            if(vehicleTypeID == 202002 || vehicleTypeID == 202003){
            	PremiumData = ComfortData;
            	ComfortData = undefined;
            }
            
            var getPremiums = JSON.parse(sessionStorage.getItem("getPremiums"));
            
            if (tarifName == "Basic") {
                if (ComfortData != undefined || ComfortData != null) {
                	var price = ""; 
                	if(is_mobile){
                		price = ComfortData.result[0].totalGrossPremium.formatMoney(2, ',', '.')+" Euro"; 
                	}else{
                		price = ComfortData.result[0].totalGrossPremium.formatMoney(2, ',', '.');
                	}
                	$('.comfortGrossPrice').html(price);
                    if (is_mobile) {
                        $('#ComfortMobile').show();
                    } else {
                        $('#Comfort').show();
                    }
                }else if(vehicleTypeID != 202002 && vehicleTypeID != 202003){
                	var price = getPremiums.comfort.formatMoney(2, ',', '.'); 
                	if(is_mobile){
                		price = price+" Euro"; 
                	}
                	$('.comfortGrossPrice').html(price);
                    if (is_mobile) {
                        $('#ComfortMobile').show();
                    } else {
                        $('#Comfort').show();
                    }
                }
                if (PremiumData !== undefined) {
                	var price = "";
                    if(sessionStorage.getItem("vehicleTypeID") == 202005){
                        PremiumData.result[0].totalGrossPremium = PremiumData.result[0].totalGrossPremium+30;
                    }
                	if(is_mobile){
                		price = PremiumData.result[0].totalGrossPremium.formatMoney(2, ',', '.')+" Euro"; 
                		$('#PremiumMobile').show();
                	}else{
                		$('#Premium').show();
                		price = PremiumData.result[0].totalGrossPremium.formatMoney(2, ',', '.');
                	}
                	$('.premiumGrossPrice').html(price);
                }else{
                	if(data.json.hasOwnProperty("Premium")){
                	    if(sessionStorage.getItem("vehicleTypeID") == 202005){
                            getPremiums.premium = getPremiums.premium+30;
                        }
                		if(is_mobile){
                    		price = getPremiums.premium.formatMoney(2, ',', '.')+" Euro"; 
                    		$('#PremiumMobile').show();
                    	}else{
                    		$('#Premium').show();
                    		price = getPremiums.premium.formatMoney(2, ',', '.');
                    	}
                    	$('.premiumGrossPrice').html(price);
                	}
                }
            } else if (tarifName == "Comfort") {
                if (PremiumData != undefined) {
                    if(sessionStorage.getItem("vehicleTypeID") == 202005){
                        PremiumData.result[0].totalGrossPremium = PremiumData.result[0].totalGrossPremium+30;
                    }
                    $('.premiumGrossPrice').html(PremiumData.result[0].totalGrossPremium.formatMoney(2, ',', '.'));
                    $('#vollkasko').show();
                    //$('#allrisk').show();
                    if (is_mobile) {
                        $('#PremiumMobile').show();
                    } else {
                        $('#Premium').show();
                    }
                    if(vehicleTypeID == 202002 || vehicleTypeID == 202003){
                    	$('#Premium').hide();
                    }
                }else{
                	if(data.json.hasOwnProperty("Premium")){
                        if(sessionStorage.getItem("vehicleTypeID") == 202005){
                            getPremiums.premium = getPremiums.premium+30;
                        }
                	    if(is_mobile){
                    		price = getPremiums.premium.formatMoney(2, ',', '.')+" Euro"; 
                    		$('#PremiumMobile').show();
                    	}else{
                    		$('#Premium').show();
                    		price = getPremiums.premium.formatMoney(2, ',', '.');
                    	}
                    	$('.premiumGrossPrice').html(price);
                	}
                }
            } else if (tarifName == "Premium") {
            	$('#vollkasko').show();
            	$('#teilkasko').show();
            	$('#allrisk').show();
            }

            if(data.json[tarifName].result[0].deviatingDeductibles.length > 0){
                var deviatingDeductibles = data.json[tarifName].result[0].deviatingDeductibles[0];

                $('.glasbruchPrice').html(" "+deviatingDeductibles.decivatingDeductibleName+" Euro");
                if (is_mobile) {
                    $('#glasbruchMobile').show();
                } else {
                    $('#glasbruch').show();
                }
            }

            totalGrossPremium = data.json[tarifName].result[0].totalGrossPremium.formatMoney(2, ',', '.');
            grossPremiumComprehensive = data.json[tarifName].result[0].grossPremiumComprehensive.formatMoney(2, ',', '.');
            grossPremiumLiability = data.json[tarifName].result[0].grossPremiumLiability.formatMoney(2, ',', '.');
            $.each(data.json[tarifName].result[0].calculationParameters, function(i,v){
            	if(v.masterName == "Zahlungsweise" && v.name == "jährlich"){
            		name = v.name;
            	}
            });
            sessionStorage.setItem("Amount", totalGrossPremium);
            $('.versichprojahr').html(totalGrossPremium);
            $('#haftprojahr').html(grossPremiumLiability + " Euro");
            $('#kaskoprojahr').html(grossPremiumComprehensive + " Euro");
            $('#haftprojahrMobile').html(grossPremiumLiability + " Euro");
            $('#kaskoprojahrMobile').html(grossPremiumComprehensive + " Euro");
            $('.paytime').html(name);
            
            if(data.json[tarifName].result[0].hasOwnProperty('grossPremiumDriverProtection') && vehicleTypeID != 202002 && vehicleTypeID != 202003){
            	grossPremiumDriverProtection = data.json[tarifName].result[0].grossPremiumDriverProtection.formatMoney(2, ',', '.');
            	if(is_mobile){
            		$('#fahrerschutzMobileAmount').html(grossPremiumDriverProtection+" Euro");
            		$('#fahrerschutzMobileP').show();
            	}else{
            		$('#fahrerschutzAmount').html(grossPremiumDriverProtection+" Euro");
            		$('#fahrerschutz').show();
            	}
            }else{
            	$('#fahrerschutz').hide();
            }
            
            //$('#premiuminsurance').html(data.json[tarifName].result[0].totalGrossPremium.formatMoney(2, ',', '.') + " Euro");
                        
            if(sessionStorage.getItem('wayoforiginalcicle') === "bankofcycle20"){
        		$('.save_btn').trigger('click');
        		sessionStorage.setItem("wayofcicle",'wayofcicle20');
        		sessionStorage.setItem("wayoforiginalcicle", "wayofcicle21");
        	}
            
            
            var vehicleClassID = 0;
            var carData = JSON.parse(sessionStorage.getItem("#sel5"));
            $.each(carData.json.result, function(i,v){
        		if(v.idVehicle == vehData[0]){
        			vehicleClassID = v.vehicleClassID;
        		}
        	});
            
            if(sessionStorage.getItem("vehicleTypeID") === "202001" && parseInt(vehicleClassID) == 100001){
        		$('#fahrerschutzversicherungsDiv').hide();
        	}
            $('#progressModal').modal('hide');
        }
    });
    $('.redirectBtn').on('click', function () {
    	if(sessionStorage.getItem('usersession') === null){
    		sessionStorage.setItem("wayofcicle", "wayofcicle21");
    		sessionStorage.setItem("wayoforiginalcicle", "wayofcicle21");
            $('#closeOverlay').show();
            $('#modalMessageBody').html("Um die gewählte Funktion ausführen zu können, müssen Sie sich bei CARISMA registrieren. Wenn Sie schon registriert sind, gehen Sie bitte zum Login.");
            $('.progress').hide();
            if(window.innerWidth < 415){
                computedWidth = window.innerWidth-20;
                $('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
            }else if (window.innerWidth < 570){
                $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
            }
            $('.contentBox > h3').html("Hinweis!");
            $('body').css('overflow','hidden');
            $('#closeOverlay').html("zum Login");
            $('#hiddenRegButton').show();
            $('#cancelOverlay').show();
            $('#closeOverlay').on('click', function(e){
                e.preventDefault();
                $('#hiddenRegButton').hide();
                window.location.href = "?article_id=17&clang=1";
            });
            $('#hiddenRegButton').on('click', function(e){
                e.preventDefault();
                $('#hiddenRegButton').hide();
                window.location.href = "?article_id=17&clang=1";
            });
            $('#cancelOverlay').on('click', function(){
                $('#hiddenRegButton').hide();
                $('#confirmationOverlay').css('display','table').fadeOut();
            });
            $('#confirmationOverlay').css('display','table').fadeIn();
    	}else{
            window.location.href = '?article_id=21&clang=1&session=' + sessionStorage.getItem("usersession");
        }
    });
    $('.save_btn').unbind('click');
    $('.save_btn').off("click");
    $('.save_btn').on("click", function () {
        if (sessionStorage.getItem("usersession") !== null) {
        	$('#modalMessageBody').html("Ihre Berechnung wird gespeichert und das Berechnungsergebnis wird Ihnen per E-Mail zugesandt.");
			$('.progress').show();
        	if(window.innerWidth < 415){
        		computedWidth = window.innerWidth-20;
    			$('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
    		}else if (window.innerWidth < 570){
    			$('#confirmationOverlay .contentBoxWrapper').css('width','auto');
    		}
    		$('#closeOverlay').hide();
            $('.contentBox > h3').html("Hinweis!");
            $('body').css('overflow','hidden');
        	$('#confirmationOverlay').css('display','table').fadeIn();
            var postData = {"command": "saveCalc"};
            storageObject = dumpSessionStorageToVar();
            postData["storageData"] = storageObject;
            $.ajax({
                url: "../ajaxtest.php",
                method: "post",
                data: postData,
                success: function (content) {
                    data = JSON.parse(content);
                    sessionStorage.setItem("contractId", data.contractID);
                    $('.progress').hide();
		            $('body').css('overflow','auto');
	    			$('#confirmationOverlay').fadeOut();
                    $('#modalMessageBody').html("");

                },
                error: function(){
                	$('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                	$('#closeOverlay').on('click', function(){
                		window.location.href = "/?article_id=7&clang=1";
                	});
                }
            });
        } else {
        	sessionStorage.setItem('wayofcicle','bankofcycle20');
        	sessionStorage.setItem("wayoforiginalcicle", "bankofcycle20");
        	$('#modalMessageBody').html("Um die gewählte Funktion ausführen zu können, müssen Sie sich bei CARISMA registrieren. Wenn Sie schon registriert sind, gehen Sie bitte zum Login.");
            $('#closeOverlay').show();
            $('.progress').hide();
            if(window.innerWidth < 415){
                computedWidth = window.innerWidth-20;
                $('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
            }else if (window.innerWidth < 570){
                $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
            }
            $('.contentBox > h3').html("Hinweis!");
            $('body').css('overflow','hidden');
            $('#closeOverlay').html("zum Login");
            $('#hiddenRegButton').show();
            $('#closeOverlay').on('click', function(){
                window.location.href = "?article_id=17&clang=1";
            });
            $('#hiddenRegButton').on('click', function(){
                window.location.href = "?article_id=18&clang=1";
            });
            $('#cancelOverlay').on('click', function(){
                $('#cancelOverlay').hide();
                $('#hiddenRegButton').hide();
                $('#closeOverlay').hide();
                $('body').css('overflow','auto');
                $('#confirmationOverlay').fadeOut();
            });
            $('#cancelOverlay').show();
            $('#confirmationOverlay').css('display','table').fadeIn();
        }
    });

    $('.upgrade_btn').on('click', function (e) {
        e.preventDefault();
        
        var vehData = sessionStorage.getItem("insuranceID").split(",");
        var vehicleTypeID = parseInt(sessionStorage.getItem("vehicleTypeID"));

        if(vehicleTypeID != 202002 && vehicleTypeID != 202003){
        	var result = getDeductible(tarifIdObject[$(this).attr("id")],vehData[0],$(this).attr("id").replace("Btn",""));
            if(result) {
                sessionStorage.setItem("TarifID", tarifIdObject[$(this).attr("id")]);
                if($(this).attr("id").replace("Btn","") == "Comfort"){
                    sessionStorage.setItem("tarif", "Komfort");
                }else if($(this).attr("id").replace("Btn","") == "Premium"){
                    sessionStorage.setItem("tarif", "Premium");
                }
            }else{
                $('#cancelOverlay').hide();
                $('#closeOverlay').hide();
                $('body').css('overflow','auto');
                $('#confirmationOverlay').fadeOut();
            }
        }else{
            /*if($(this).attr("id") == "ComfortBtn"){
                var result = getDeductible(tarifIdObject["ComfortBtn"],vehData[0],"Comfort");
                if(result) {
                    sessionStorage.setItem("TarifID",tarifIdObject["ComfortBtn"]);
                    sessionStorage.setItem("tarif",$(this).attr("id").replace("Btn", ""));
                }else{
                    $('body').css('overflow','auto');
                    $('#confirmationOverlay').fadeOut();
                }
            }*/

        	if($(this).attr("id") == "PremiumBtn"){
                var result = getDeductible(tarifIdObject["ComfortBtn"],vehData[0],"Premium");
                if(result) {
                    sessionStorage.setItem("TarifID",tarifIdObject["ComfortBtn"]);
                    sessionStorage.setItem("tarif",$(this).attr("id").replace("Btn", ""));
                }else{
                    $('#cancelOverlay').hide();
                    $('#hiddenRegButton').hide();
                    $('#closeOverlay').hide();
                    $('body').css('overflow','auto');
                    $('#confirmationOverlay').fadeOut();
                }
        	}
        }
        
        if($(this).attr("id").replace("Btn","") == "Premium" && vehicleTypeID != 202002 && vehicleTypeID != 202003){
        	$('#mit_fahrschutz').prop("checked",true);
        }
        if(is_mobile){
        	$('#'+$(this).attr("id").replace("Btn","")+"Mobile").hide();
        }else{
        	$('#'+$(this).attr("id").replace("Btn","")).hide();
        }
        if(window.innerWidth < 415){
    		computedWidth = window.innerWidth-20;
			$('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
		}else if (window.innerWidth < 570){
			$('#confirmationOverlay .contentBoxWrapper').css('width','auto');
		}
        $('#cancelOverlay').show();
        $('.contentBox > h3').html("Hinweis!");
        $('body').css('overflow','hidden');
    	$('#confirmationOverlay').css('display','table').fadeIn();
    });
})();

function getDeductible(tarifId,vehicleId,preButton){

    var idTarif = 0;
    var vehicleGroupId = parseInt(sessionStorage.getItem("vehicleTypeID"));

    if(vehicleGroupId == 202005 && tarifId == 10827){
        idTarif = 10828;
    }else{
        idTarif = tarifId;
    }

	$.ajax({
        url: '../ajaxtest.php',
        method: "post",
        data: {"command": "deductable", "insuranceId": idTarif, "vehicleId": vehicleId},
        success: function (content) {
            var data = JSON.parse(content);

            /*if(sessionStorage.getItem("vehicleTypeID") == 202005){
                var newData = JSON.parse(JSON.stringify(data));
                $.each(newData.json.result, function(i,v){
                    var newDeduc = JSON.parse(JSON.stringify(newData.json.result[i].full));
                    newDeduc.insuranceTypeName = "Allrisk";
                    newDeduc.insuranceTypeId = 205004;
                    newData.json.result[i].allRisk = newDeduc;
                });
                content = JSON.stringify(newData);
                data = JSON.parse(JSON.stringify(newData));
            }*/
            sessionStorage.setItem("deductible", content);
            $('#modalMessageBody').html("");
            $('#closeOverlay').show();

            contentHtml = "";
            $('#selbstbehalt_paket').html('<option value="0"></option>');
            $.each(data.json.result, function (i, v) {
                if(v.hasOwnProperty('allRisk')){
                	contentHtml += "<option value='" + v.partial.deductibleID + "," + v.full.deductibleID + ","+ v.allRisk.deductibleID +"'>" + v.allRisk.insuranceTypeName + " "+ v.allRisk.deductibleName +" Euro SB, " + v.full.insuranceTypeName + " " + v.full.deductibleName + " Euro SB, " + v.partial.insuranceTypeName + " " + v.partial.deductibleName + " Euro SB</option>";
                }else if (v.hasOwnProperty('full')) {
                    contentHtml += "<option value='" + v.partial.deductibleID + "," + v.full.deductibleID + "'>" + v.full.insuranceTypeName + " " + v.full.deductibleName + " Euro SB, " + v.partial.insuranceTypeName + " " + v.partial.deductibleName + " Euro SB</option>";
                }else {
                    contentHtml += "<option value='" + v.partial.deductibleID + "'>" + v.partial.insuranceTypeName + " " + v.partial.deductibleName + " Euro SB</option>";
                }
            });
            $('#selbstbehalt_paket').append(contentHtml);

            $('#selbstbehalt_paket').on('change', function () {
                sessionStorage.setItem("kinofselbstbehalt", $('#'+this.id+" :selected").val());
                if(this.value !== ""){
                	sessionStorage.setItem("kinofselbstbehaltText", $('#'+this.id+" :selected").text());
                }
            });
			$('#cancelOverlay').on('click', function(){
			    $('#deductibleDiv').hide();
                $('#'+preButton).show();
                $('body').css('overflow','auto');
                $('#cancelOverlay').hide();
                $('#closeOverlay').hide();
                $('#confirmationOverlay').fadeOut();
            });
			$('#deductibleDiv').show();
			$('#closeOverlay').on('click', function(){
                $('#cancelOverlay').hide();
                $('#closeOverlay').hide();
				var fsChecked = true;
				 
				if(!$('#mit_fahrschutz').prop('checked') || !$('#ohne_fahrschutz').prop('checked')){
					fsChecked = true;
				}else{
					fsChecked = false;
				}
				if(sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003"){
					fsChecked = true;
				}
				sessionStorage.setItem('TarifID',idTarif);
				if($('#selbstbehalt_paket :selected').val() != "0" && $('#selbstbehalt_paket :selected').val() != "" && fsChecked){
					$('#deductibleDiv').hide();
					$('#modalMessageBody').html("Ihr Upgrade wird berechnet");
					$('.progress').show();
					var vehData = sessionStorage.getItem("insuranceID").split(",");
					data = JSON.parse(sessionStorage.getItem("Fahrzeugdaten2"));
					data["selbstbehalt_paket"] = sessionStorage.getItem("kinofselbstbehalt");
					if($('#mit_fahrschutz').prop('checked')){
						data["optradio"] = "yes";
						$('#fahrerschutz').show();
					}else{
						data["optradio"] = "no";
						$('#fahrerschutz').hide();
					}
					sessionStorage.setItem("Fahrzeugdaten2",JSON.stringify(data));
				    insuranceData = JSON.parse(sessionStorage.getItem("getPremiums"));

                    var kilometerNeeded = "no";
                    var baseTarifID = sessionStorage.getItem("TarifID");

				    tarifIds = [];
				    if(sessionStorage.getItem("vehicleTypeID") == 202005){
                        $.each(insuranceData, function (i, v) {
                            tarifIds.push({"name": v.insuranceGroup, "value": v.insuranceGroupID, "deductible": v.deductibles});
                        });

                        $.each(insuranceData, function(i,v){
                            if(v.calculationParameters[6].ID == 110002 && v.insuranceGroupID == baseTarifID ){
                                kilometerNeeded = "yes";
                            }
                        });
                    }else{
                        $.each(insuranceData.json.result, function(i,v){
                            if(v.calculationParameters[6].ID == 110002 && v.insuranceGroupID == baseTarifID ){
                                kilometerNeeded = "yes";
                            }
                        });

                        $.each(insuranceData.json.result, function (i, v) {
                            tarifIds.push({"name": v.insuranceGroup, "value": v.insuranceGroupID, "deductible": v.deductibles});
                        });
                    }

				    kaskoText = sessionStorage.getItem("kinofselbstbehaltText").split(",");
				    if(kaskoText.length == 1){
				    	kaskoText[0] = kaskoText[0].replaceAll("SB", "").replaceAll("Teilkasko", "");
				    	$('.teilkaskoPrice').html(kaskoText[0]);
				    	if(is_mobile){
				    		$('#teilkaskoMobile').show();
				    	}else{
				    		$('#teilkasko').show();
				    	}
				    }else if(kaskoText.length == 2){
				    	kaskoText[0] = kaskoText[0].replaceAll("SB", "").replaceAll("Vollkasko", "");
				    	kaskoText[1] = kaskoText[1].replaceAll("SB", "").replaceAll("Teilkasko", "");
				    	$('.teilkaskoPrice').html(kaskoText[1]);
				    	$('.vollkaskoPrice').html(kaskoText[0]);
				    	if(is_mobile){
				    		$('#teilkaskoMobile').show();
				        	$('#vollkaskoMobile').show();
				    	}else{
				    		$('#teilkasko').show();
				        	$('#vollkasko').show();
				    	}
				    }else if(kaskoText.length == 3){
				    	kaskoText[0] = kaskoText[0].replaceAll("SB", "").replaceAll("Allrisk", "");
				    	kaskoText[1] = kaskoText[1].replaceAll("SB", "").replaceAll("Vollkasko", "");
				    	kaskoText[2] = kaskoText[2].replaceAll("SB", "").replaceAll("Teilkasko", "");
				    	$('.teilkaskoPrice').html(kaskoText[2]);
				    	$('.vollkaskoPrice').html(kaskoText[1]);
				    	$('.allRiskPrice').html(kaskoText[0]);
				    	if(is_mobile){
				    		$('#teilkaskoMobile').show();
				        	$('#vollkaskoMobile').show();
				        	$('#allriskMobile').show();
				    	}else{
				    		$('#teilkasko').show();
				        	$('#vollkasko').show();
				        	$('#allrisk').show();
				    	}
				    }
				    $('#'+preButton).hide();
				    sessionStorage.setItem("tarif",preButton);
				    var postData = {"command": "premium", "premiumData": data, "deductible": tarifIds, "vehicleId": vehData[0],"kilometerNeeded": kilometerNeeded};
					
					$.ajax({
				        url: "../ajaxtest.php",
				        method: "post",
				        data: postData,
				        success: function (content) {
				        	tarifName = sessionStorage.getItem("tarif");
				        	var data = JSON.parse(content);
                            if(sessionStorage.getItem("vehicleTypeID") == 202005){
                                var newData = JSON.parse(JSON.stringify(data));
                                newData.json.Premium = JSON.parse(JSON.stringify(data.json.Comfort));
                                newData.json.Premium.result[0].insuranceGroup = "Premium";
                                content = JSON.stringify(newData);
                                data = JSON.parse(JSON.stringify(newData));
                            }
                            sessionStorage.setItem("premium", content);

				            if (tarifName == "Basis") {
				                tarifName = "Basic";
				            }else if(tarifName == "Komfort"){
				            	tarifName = "Comfort";
				            }
				            
				            var BasicData = undefined;
				            var ComfortData = undefined;
				            var PremiumData = undefined;
				            
				            var vehicleTypeID = sessionStorage.getItem("vehicleTypeID");
				            
				            if(data.json.hasOwnProperty("Premium")){
				            	if(data.json.Premium.success != "false"){
				                	PremiumData = data.json.Premium;
				                }else{
				                	PremiumData = undefined;
				                }
				            }
				            if(data.json.hasOwnProperty("Comfort")){
				            	if(data.json.Comfort.success != "false"){
				                	ComfortData = data.json.Comfort;
				                }else{
				                	ComfortData = undefined;
				                }
				            }
				            if(data.json.hasOwnProperty("Basic")){
				            	if(data.json.Basic.success != "false"){
				                	BasicData = data.json.Basic;
				                }else{
				                	BasicData = undefined;
				                }
				            }
	   
				            if(vehicleTypeID == 202002 || vehicleTypeID == 202003){
				            	PremiumData = ComfortData;
				            	ComfortData = undefined;
				            }
				            
				            var getPremiums = JSON.parse(sessionStorage.getItem("getPremiums"));
				            
				            if (tarifName == "Basic") {
				                if (ComfortData != undefined || ComfortData != null) {
				                	var price = ""; 
				                	if(is_mobile){
				                		price = ComfortData.result[0].totalGrossPremium.formatMoney(2, ',', '.')+" Euro"; 
				                	}else{
				                		price = ComfortData.result[0].totalGrossPremium.formatMoney(2, ',', '.');
				                	}
				                	$('.comfortGrossPrice').html(price);
				                    if (is_mobile) {
				                        $('#ComfortMobile').show();
				                    } else {
				                        $('#Comfort').show();
				                    }
				                }else if(vehicleTypeID != 202002 && vehicleTypeID != 202003){
				                	var price = getPremiums.comfort.formatMoney(2, ',', '.'); 
				                	if(is_mobile){
				                		price = price+" Euro"; 
				                	}
				                	$('.comfortGrossPrice').html(price);
				                    if (is_mobile) {
				                        $('#ComfortMobile').show();
				                    } else {
				                        $('#Comfort').show();
				                    }
				                }
				                if (PremiumData !== undefined) {
				                	var price = "";
                                    if(sessionStorage.getItem("vehicleTypeID") == 202005){
                                        PremiumData.result[0].totalGrossPremium = PremiumData.result[0].totalGrossPremium+30;
                                    }
				                	if(is_mobile){
				                		price = PremiumData.result[0].totalGrossPremium.formatMoney(2, ',', '.')+" Euro"; 
				                		$('#PremiumMobile').show();
				                	}else{
				                		$('#Premium').show();
				                		price = PremiumData.result[0].totalGrossPremium.formatMoney(2, ',', '.');
				                	}
				                	$('.premiumGrossPrice').html(price);
				                }else{
				                	if(data.json.hasOwnProperty("Premium")){
				                		if(is_mobile){
				                    		price = getPremiums.premium.formatMoney(2, ',', '.')+" Euro"; 
				                    		$('#PremiumMobile').show();
				                    	}else{
				                    		$('#Premium').show();
				                    		price = getPremiums.premium.formatMoney(2, ',', '.');
				                    	}
				                    	$('.premiumGrossPrice').html(price);
				                	}
				                }
				            } else if (tarifName == "Comfort") {
				                if (PremiumData !== undefined) {
                                    if(sessionStorage.getItem("vehicleTypeID") == 202005){
                                        PremiumData.result[0].totalGrossPremium = PremiumData.result[0].totalGrossPremium+30;
                                    }
				                    $('.premiumGrossPrice').html(PremiumData.result[0].totalGrossPremium.formatMoney(2, ',', '.'));
				                    $('#teilkasko').show();
				                    $('#vollkasko').show();
				                    if (is_mobile) {
				                        $('#PremiumMobile').show();
				                    } else {
				                        $('#Premium').show();
				                    }
				                    if(vehicleTypeID == 202002 || vehicleTypeID == 202003){
				                    	$('#Premium').hide();
				                    }
				                }else{
				                	if(data.json.hasOwnProperty("Premium")){
				                		if(is_mobile){
				                    		price = getPremiums.premium.formatMoney(2, ',', '.')+" Euro"; 
				                    		$('#PremiumMobile').show();
				                    	}else{
				                    		$('#Premium').show();
				                    		price = getPremiums.premium.formatMoney(2, ',', '.');
				                    	}
				                		$('#teilkasko').show();
				                		$('#vollkasko').show();
				                		$('#allrisk').hide();
				                    	$('.premiumGrossPrice').html(price);
				                	}
				                }
				            } else if (tarifName == "Premium") {
				            	$('#vollkasko').show();
				            	$('#teilkasko').show();
				            	$('#allrisk').show();
				            	
				            	$('#Comfort').hide();
				            }
				            totalGrossPremium = data.json[tarifName].result[0].totalGrossPremium.formatMoney(2, ',', '.');
				            grossPremiumComprehensive = data.json[tarifName].result[0].grossPremiumComprehensive.formatMoney(2, ',', '.');
				            grossPremiumLiability = data.json[tarifName].result[0].grossPremiumLiability.formatMoney(2, ',', '.');
				            var payTimename = data.json[tarifName].result[0].calculationParameters[9].name;
				            sessionStorage.setItem("Amount", totalGrossPremium);
				            $('.versichprojahr').html(totalGrossPremium);
				            $('#haftprojahr').html(grossPremiumLiability + " Euro");
				            $('#kaskoprojahr').html(grossPremiumComprehensive + " Euro");
				            $('#haftprojahrMobile').html(grossPremiumLiability + " Euro");
				            $('#kaskoprojahrMobile').html(grossPremiumComprehensive + " Euro");
				            $('.paytime').html(payTimename);
				            if(data.json[tarifName].result[0].hasOwnProperty('grossPremiumDriverProtection') && $('#mit_fahrschutz').prop('checked')){
				            	grossPremiumDriverProtection = data.json[tarifName].result[0].grossPremiumDriverProtection.formatMoney(2, ',', '.');
				            	if(is_mobile){
				            		$('#fahrerschutzMobileAmount').html(grossPremiumDriverProtection+" Euro");
				            		$('#fahrerschutzMobileP').show();
				            	}else{
				            		$('#fahrerschutzAmount').html(grossPremiumDriverProtection+" Euro");
				            		$('#fahrerschutz').show();
				            	}
				            }else{
				            	$('#fahrerschutz').hide();
				            }
				            var tarif = sessionStorage.getItem("tarif");
				            if(tarif === "Comfort"){
				            	tarif = "Komfort";
				            }
				            if(tarif === "Basic"){
				            	tarif = "Basis";
				            }
				            var headLineString = "Ihr individuelles CARISMA "+tarif+"-Tarif Paket";
				            if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
				            	if(tarif == "Basis" || tarif == "Basic"){
				            		var headLineString = "Ihr individuelles CARISMA Komfort-Tarif Paket";
				            	}else if(tarif == "Komfort" || tarif == "Comfort"){
				            		var headLineString = "Ihr individuelles CARISMA Premium-Tarif Paket";
				            	}
				            }
				            
				            $('#headlineData').html(headLineString);
				            
				            $('.progress').hide();
				            $('body').css('overflow','auto');
			    			$('#confirmationOverlay').fadeOut();

				        }
				    });
				}
			});
        }
    });
}

function checkCalculationCount(){
    if(sessionStorage.getItem("countCalc") == undefined){
        sessionStorage.setItem("countCalc",1);
    }else{
        var countedCalc = parseInt(sessionStorage.getItem("countCalc"));
        countedCalc++;
        if(countedCalc > 5 && sessionStorage.getItem("usersession") == undefined){
            $('#modalMessageBody').html("Um mehr als fünf Berechnungen durchzuführen, müssen Sie sich bei CARISMA registrieren. Wenn Sie schon registriert sind, gehen Sie bitte zum Login.");
            $('#closeOverlay').show();
            $('.progress').hide();
            if(window.innerWidth < 415){
                computedWidth = window.innerWidth-20;
                $('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
            }else if (window.innerWidth < 570){
                $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
            }
            $('.contentBox > h3').html("Hinweis!");
            $('body').css('overflow','hidden');
            $('#closeOverlay').html("zum Login");
            $('#hiddenRegButton').show();
            $('#closeOverlay').on('click', function(){
                $('#hiddenRegButton').hide();
                window.location.href = "?article_id=17&clang=1";
            });
            $('#closeOverlay').on('click', function(){
                $('#hiddenRegButton').hide();
                window.location.href = "?article_id=18&clang=1";
            });
            $('#cancelOverlay').on('click', function(){
                $('#cancelOverlay').hide();
                $('#hiddenRegButton').hide();
                $('body').css('overflow','auto');
                $('#confirmationOverlay').fadeOut();
            });
            $('#cancelOverlay').show();
            $('#confirmationOverlay').css('display','table').fadeIn();
        }else{
            sessionStorage.setItem("countCalc",countedCalc);
        }
    }
}