'use strict';
/**
 * Created by root on 15.11.17.
 */
window.onload = function () {
    if (typeof history.pushState === "function") {
        
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
            if(window.innerWidth < 415){
                var computedWidth = window.innerWidth-20;
                $('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
            }else if (window.innerWidth < 570){
                $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
            }
            $('#modalMessageBody').html("Bitte benutzen Sie die Naviagtionselemente auf der Seite .");
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display','table');
            $('#closeOverlay').on('click', function(){
                    $('#confirmationOverlay').hide();
            });
        };
    }
    else {
        //console.log("hasChange");
        var ignoreHashChange = true;
        window.onhashchange = function () {
            
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
            }
            else {
                ignoreHashChange = false;
            }
        };
    }
};


(function() {
    window.history.forward(1);
    HTMLElement.prototype.hasClass = function(cls) {
        var i;
        var classes = this.className.split(" ");
        for(i = 0; i < classes.length; i++) {
            if(classes[i] == cls) {
                return true;
            }
        }
        return false;
    };
    var url = window.location.href;
    var params = url.split('?');
    var allParams = params[1].split("&");
    var setDelay = false;

    if(allParams.length > 2){
        var paddern1 = /\w\=\d*/;
        var paddern2 = /\u\=\w*\@\w*\.\w{2,4}/;
        if(paddern1.test(allParams[4]) && validateEmail(allParams[3])){
            //startTarifRechner();
            var username = allParams[3].split("=");
            var sessionId = allParams[2].split("=");
            var contractId = allParams[4].split("=");
            setDelay = true;
            $('#progressModal').modal("show");
            sessionStorage.setItem('usersession',sessionId[1]);
            sessionStorage.setItem('username',username[1]);
            sessionStorage.setItem('constractID',contractId[1]);
            $.ajax({
                url: '../ajaxtest.php',
                method: "post",
                data: {"command": "getCalc","contractId": contractId[1], "sessionId": sessionId[1], "username": username[1]},
                async: false,
                success: function (content) {
                    var fahrzeugDaten = JSON.parse('{"herstellerschluesselnummer":"","typschluesselnummer":"","selbstbehalt_paket":"","optradio":"","geplante_zulassung":"","grund_der_versicherung":"","erstzulassung_monat":"","erstzulassung":"","tacho_einheit":"","tacho_stand":"","fahrleistung":"","marktwert":"","wiederherstellungswert":"","custom_type":"","kennzeichen":"","saison_start":"","fahreralter":"","leasing":"","kaufjahr":""}');
                    //var personalData = JSON.parse('{"form_of_address":"","firstname":"","lastname":"","gebdate":"","street":"","zip":"","city":"","personalStreet":"","personalzip":"","personalcity":""}');
                    var loadedUserData = JSON.parse(content);
                    if(loadedUserData.success != "false"){
                        var vehData = loadedUserData.vehicle;
                        sessionStorage.setItem("FullCarData",JSON.stringify(vehData));
                        //console.log(loadedUserData);
                        var calcData = loadedUserData.calculationResult.calculationParameters;
                        var decutibles = loadedUserData.calculationResult.deductibles;
                        var yearOfConstruct = 0;
                        var fullContructionDate = 1970;
                        if(vehData.yearOfContruction == "-"){
                            fullContructionDate = vehData.firstYearOfConstruction+"-"+vehData.lastYearOfConstruction;
                            sessionStorage.setItem("firstYear",vehData.firstYearOfConstruction);
                        }else{
                            yearOfConstruct = vehData.yearOfContruction;
                            var headlineYear = new Date(yearOfConstruct);
                            fullContructionDate = headlineYear.getFullYear();
                            sessionStorage.setItem("firstYear",fullContructionDate);
                        }

                        sessionStorage.setItem("backupVehicleClassID",vehData.vehicleClassID);
                        sessionStorage.setItem('vehicleTypeID',vehData.vehGroup);
                        sessionStorage.setItem('Amount',loadedUserData.calculationResult.totalGrossPremium);
                        sessionStorage.setItem('insuranceID',vehData.vehicleID+
                                               ','+vehData.manufacturer+","+vehData.vehicleType+
                                               ','+vehData.vehicleBody.replace("\\u005Cu00E9","é")+
                                               ','+fullContructionDate+
                                               ','+vehData.HP);
                        if(loadedUserData.calculationResult.insuranceGroup === "Comfort"){
                            sessionStorage.setItem('tarif',"Komfort");
                        }else if(loadedUserData.calculationResult.insuranceGroup === "Basic"){
                            sessionStorage.setItem('tarif',"Basis");
                        }else if(loadedUserData.calculationResult.insuranceGroup === "Premium"){
                            sessionStorage.setItem('tarif',"Premium");
                        }

                        var tarif = sessionStorage.getItem("tarif");
                        var vehData2 = sessionStorage.getItem("insuranceID").split(",");
                        var realVehicleType = '"'+vehData2[2].replace(/"/g,'\\"').replace(/[\n\r]+/g,'\\n')+'"';
                        vehData2[2] = JSON.parse(realVehicleType).replace("\\/", "/");;
                        if (vehData2[4] == " ") {
                            var realVehicleType = '"' + vehData2[2].replace(/"/g, '\\"').replace(/[\n\r]+/g, '\\n') + '"';
                            headLineString = decodeURI(tarif[1] + "-Tarif-Angebot für meinen " + vehData2[1] + " " + decodeURI(vehData2[2]) + " von Baujahr " + vehData2[3] + " mit" + vehData2[5] + " PS");
                        } else {
                            console.log(vehData2[4]);
                            vehData2[4] = vehData2[4].replace("t\\u005Cu00FCrig", "türig");
                            vehData2[3] = vehData2[3].replace("t\\u005Cu00FCrig", "türig");
                            headLineString = decodeURI(tarif + "-Tarif " + vehData2[1] + " " + vehData2[2] + ", Baujahr " + vehData2[4] + ", " + vehData2[3] + ", " + vehData2[5] + " PS");
                        }
                        $('#headlineData').html(headLineString);

                        var today = new Date();
                        var fullYear = today.getFullYear();
                        var erstzulassung_jahr = '<option value="0"><option>';
                        if(sessionStorage.getItem("firstYear") == null ){
                            for(var i = vehData2[3]; i <= fullYear; i++){
                                erstzulassung_jahr += '<option value="'+i+'">'+i+'</option>';
                            }
                            $('#erstzulassung').html(erstzulassung_jahr);
                            $('#erstzulassung').children('option:first').remove();
                        }else{
                            for(var i = sessionStorage.getItem("firstYear"); i <= fullYear; i++){
                                erstzulassung_jahr += '<option value="'+i+'">'+i+'</option>';
                            }
                            $('#erstzulassung').html(erstzulassung_jahr);
                            $('#erstzulassung').eq(2).remove();
                        }

                        if(loadedUserData.calculationResult.useRemanufactValue){
                            fahrzeugDaten.reusecheckbox = "on";
                            fahrzeugDaten.wiederherstellungswert = vehData.useRemanufactValue;
                        }

                        sessionStorage.setItem('TarifID',loadedUserData.calculationResult.insuranceGroupID);
                        sessionStorage.setItem('garage',calcData[5].name);
                        sessionStorage.setItem('anredeID',loadedUserData.vehicle.genderID);

                        var fahrzeugDatenFormData = [];
                        var kilometerNeeded = false;
                        $.each(calcData, function(i,v){
                            if(v.masterName == "Kennzeichenart"){
                                fahrzeugDatenFormData.push(v.ID);
                            }
                            if(v.masterName == "Leasing"){
                                fahrzeugDatenFormData.push(v.ID);
                            }
                            if(v.masterName == "Kaufjahr"){
                                fahrzeugDatenFormData.push(v.ID);
                            }
                            if(v.masterName == "Fahreralter"){
                                fahrzeugDatenFormData.push(v.ID);
                            }
                            if(v.masterName == "KM-Leistung"){
                                fahrzeugDatenFormData.push(v.ID);
                                kilometerNeeded = true;
                            }
                            if(v.masterName == "Schmerzensgeld"){
                                fahrzeugDatenFormData.push(v.ID);
                            }
                            if(v.masterName == "Zustandsnote"){
                                fahrzeugDatenFormData.push(String(v.ID).split("12000"));
                            }
                        });

                        fahrzeugDaten.marktwert = loadedUserData.calculationResult.vehicleValue;
                        fahrzeugDaten.tacho_stand = loadedUserData.vehicle.milage;
                        fahrzeugDaten.kennzeichen = fahrzeugDatenFormData[0];
                        fahrzeugDaten.leasing = fahrzeugDatenFormData[1];
                        fahrzeugDaten.kaufjahr = fahrzeugDatenFormData[2];

                        if(kilometerNeeded){
                            fahrzeugDaten.fahrleistung = fahrzeugDatenFormData[3];
                            fahrzeugDaten.fahreralter = fahrzeugDatenFormData[4];
                        }else{
                            fahrzeugDaten.fahreralter = fahrzeugDatenFormData[3];
                        }


                        fahrzeugDaten.custom_type = fahrzeugDatenFormData[(fahrzeugDatenFormData.length-1)][1];
                        if(vehData.milageMetricID == "794001"){
                            fahrzeugDaten.tacho_einheit = "km";
                        }else{
                            fahrzeugDaten.tacho_einheit = "mi";
                        }
                        if(fahrzeugDatenFormData[(fahrzeugDatenFormData.length-2)] == "ja"){
                            fahrzeugDaten.optradio = "yes";
                        }
                        var deductiblesString = [];
                        $.each(decutibles, function(i,v){
                            deductiblesString.push(v.ID);
                        });

                        if(vehData.initialRegistration != '' || vehData.initialRegistration != '-'){
                            var monthObject = {"JAN":"01","FEB":"02","MAR":"03","APR":"04","MAY":"05","JUN":"06","JUL":"07","AUG":"08","SEP":"09","OCT":"10","NOV":"11","DEC":"12"};
                            var splittedDate = vehData.initialRegistration.split("-");
                            
                           // console.log(splittedDate[1],monthObject[splittedDate[1]])
                            fahrzeugDaten.erstzulassung_monat = monthObject[splittedDate[1]];

                            if(splittedDate[2] < 99){
                                splittedDate[2] = "19"+splittedDate[2];
                            }else if(splittedDate[2] == "00"){
                                splittedDate[2] = "20"+splittedDate[2];
                            }

                            fahrzeugDaten.erstzulassung = splittedDate[2];

                            //console.log(fahrzeugDaten.erstzulassung_monat,fahrzeugDaten.erstzulassung);
                        }
                        fahrzeugDaten.selbstbehalt_paket = deductiblesString.join(",");
                        sessionStorage.setItem('Fahrzeugdaten2',JSON.stringify(fahrzeugDaten));
                        //sessionStorage.setItem('personalDataFilled',JSON.stringify(personalData));

                        getDeductible(loadedUserData.calculationResult.insuranceGroupID,vehData.vehicleID,true,loadedUserData.calculationResult.vehicleValue);
                    }else{

                    }
                    
                }
            });
        }
    }

    $('#allapproved').append(sessionStorage.getItem("anforderungLi"));

    var OSName="Unknown OS";
    if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
    else if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
    else if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
    else if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
    var driverMatchinArray = {112005 : "Alle Fahrer mindestens 35 und jünger 76 Jahren",
                              112006 :"Alle Fahrer mindestens 45 und jünger 76 Jahren",
                              112007 : "Alle Fahrer mindestens 55 und jünger 76 Jahren",
                              112009 : "Alle Fahrer mindestens 25 und jünger 76 Jahren"};
    if(isChrome()){
        $('.zustandsDiv').css('width','');
    }

    var vehData = sessionStorage.getItem("insuranceID").split(",");
    var tarifId = sessionStorage.getItem("TarifID");
    var kvsDetailList = JSON.parse(sessionStorage.getItem('#sel1'));
    var leasingDropDown = [];
    var kaufjahrDropDownArray = [];
    var kaufjahrDropdown = "<option value=''></option>";
    var driversYearDropDown = "";
    var kilometerLeistungDropDown = "<option value=''></option>";
    var kilometerLeistungDropDownArray = [];
    var baseTarifID = sessionStorage.getItem("TarifID");
    var tarif = sessionStorage.getItem("tarif");
    var insuranceData = JSON.parse(sessionStorage.getItem("getPremiums"));
    var kilometerNeeded = "no";
    var carData = JSON.parse(sessionStorage.getItem("#sel5"));
    var vehicleClassID = 0;

    if(carData.json.result.length < 1){
        vehicleClassID = sessionStorage.getItem("backupVehicleClassID");
    }else{
        $.each(carData.json.result, function(i,v){
            if(v.idVehicle == vehData[0]){
                vehicleClassID = v.vehicleClassID;
            }
        });
    }

    if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003" ) {
        $('#fahrerschutzversicherungsDiv').hide();
    }

    if(sessionStorage.getItem("vehicleTypeID") === "202001" && parseInt(vehicleClassID) == 100001){
        $('#fahrerschutzversicherungsDiv').hide();
    }

    $('#wizard1').attr("href", "?article_id=32&clang=1&restart=1");

    var realVehicleType = '"'+vehData[2].replace(/"/g,'\\"').replace(/[\n\r]+/g,'\\n')+'"';
    vehData[2] = JSON.parse(realVehicleType).replace("\\/", "/");

    var headLineString = "";
    if (vehData[4] == " ") {
        if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
            if(tarif[1] == "Basis" || tarif[1] == "Basic"){
                tarif[1] = "Komfort";
            }else if(tarif[1] == "Komfort" || tarif[1] == "Comfort"){
                tarif[1] = "Premium";
            }
        }
        headLineString = decodeURI(tarif[1]+"-Tarif-Angebot für meinen "+vehData[1]+" "+vehData[2]+" von Baujahr "+vehData[3]+" mit"+vehData[5]+" PS");
    } else {
        if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
            if(tarif == "Basis" || tarif == "Basic"){
                tarif = "Komfort";
            }else if(tarif == "Komfort" || tarif == "Comfort"){
                tarif = "Premium";
            }
        }
        vehData[4] = vehData[4].replace("t\\u005Cu00FCrig", "türig");
        vehData[3] = vehData[3].replace("t\\u005Cu00FCrig", "türig");

        headLineString = decodeURI(tarif + "-Tarif " + vehData[1] + " " + vehData[2] + ", Baujahr " + vehData[4] + ", " + vehData[3] + ", " + vehData[5]+" PS");
    }
    $('#headlineData').html(headLineString);
    if(tarifId == "10827" || tarifId == 10827){
        $('#mit_fahrschutz').prop("checked","checked");
    }
    //console.log(setDelay);
    if(!setDelay){

        getDeductible(tarifId,vehData[0],false,0);

        var fullYear = new Date().getFullYear();

        var erstzulassung_jahr = '<option value="0"><option>';
        if(sessionStorage.getItem("firstYear") == null ){
            for(var i = vehData[3]; i <= fullYear; i++){
                erstzulassung_jahr += '<option value="'+i+'">'+i+'</option>';
            }
            $('#erstzulassung').html(erstzulassung_jahr);
            $('#erstzulassung').children('option:first').remove();
        }
    }
    $('#selbstbehalt_paket').on('change', function () {
        sessionStorage.setItem("kinofselbstbehalt", $('#'+this.id+" :selected").val());
        if(this.value !== ""){
            sessionStorage.setItem("kinofselbstbehaltText", $('#'+this.id+" :selected").text());
        }
    });

    var today = new Date();
    var twentyNineYears = new Date(today.getFullYear() - 30, today.getMonth(), 1, 0, 0, 0, 0);
    var twentyEightYears = new Date(today.getFullYear() - 28, today.getMonth(), 1, 0, 0, 0, 0);

    var cNumberObject = {"JAN": 0,"FEB":1,"MAR":2,"APR":3,"MAY":4,"JUN":5,"JUL":6,"AUG":7,"SEP":8,"OCT":9,"NOV":10,"DEC":11};

    $('#erstzulassung, #erstzulassung_monat ,#erstzulassung_tag').on('change', function(){
        var erstzulassung = $('#erstzulassung :selected').val();
        var erstzulassung_monat = $('#erstzulassung_monat :selected').val();
        var erstzulassung_tag = $('#erstzulassung_tag :selected').val();
        var compareDate = new Date(erstzulassung, cNumberObject[erstzulassung_monat], erstzulassung_tag, 0, 0, 0, 0);

        if(compareDate > twentyEightYears){
            $('#historischesKennzeichen').hide();
            $('#historischesKennzeichenSaison').hide();
        }else if (compareDate <= twentyNineYears){
            $('#historischesKennzeichen').show();
            $('#historischesKennzeichenSaison').show();
        }
    });

    var d = new Date();
    d.setMonth(d.getMonth() + 2);

    $('#geplante_zulassung').datepicker({
        format: 'dd.mm.yyyy',
        daysOfWeekDisabled: [0, 6],
        todayBtn: "linked",
        language: "de",
        autoclose: true,
        startDate: new Date(),
        /*endDate: d,*/
        orientation: 'bottom'
    }).on('changeDate', function (e) {

        if ($('#geplante_zulassung').val() == "") {
            $('#geplante_zulassung').addClass('has-error');
            $('#geplante_zulassung').removeClass('has-no-error');
        } else {
            $('#geplante_zulassung').addClass('has-no-error');
            $('#geplante_zulassung').removeClass('has-error');
            if($('#geplante_zulassung-error').length > 0){
                $('#geplante_zulassung-error').remove();
            }
        }
    }).keyup(function () {
        if ($('#geplante_zulassung').val() == "") {
            $('#geplante_zulassung').removeClass('has-no-error');
        }else{
            if(isChrome() && $('#erstzulassung-error').length > 0){
                $('#erstzulassung-error').remove();
            }
        }
    });

    $('input[name="custom_type"]').on('click',function(e){
        if(this.value == 4){
            $('#notice').show();
            $('#underNotice').hide();
        }else{
            $('#notice').hide();
            $('#underNotice').show();
        }
    });

    var marktwert_tausend = '<option value=""></option>';
    var marktwert_hundert = '<option value=""></option><option value="000">000</option>';
    for(var i = 1; i <= 999; i++){
        marktwert_tausend += '<option value="'+i+'">'+i+'</option>';
        if(i.toString().match(/00$/g)){
            marktwert_hundert += '<option value="'+i+'">'+i+'</option>';
        }
    }
    $('#marktwert_tausend').html(marktwert_tausend);
    $('#marktwert_hundert').html(marktwert_hundert);

    $('#wiederherstellungswert_tausend').html(marktwert_tausend);
    $('#wiederherstellungswert_hundert').html(marktwert_hundert);

    $('#marktwert_tausend').on('change',checkWHD);
    $('#marktwert_hundert').on('change',checkWHD);

    $('#wiederherstellungswert_tausend').on('change',checkWHD);
    $('#wiederherstellungswert_hundert').on('change',checkWHD);

    var evilDropDownObject = { '03':'<option value="04">April</option><option value="05">Mai</option><option value="06">Juni</option><option value="07">Juli</option><option value="08">August</option><option value="09">September</option><option value="10">Oktober</option><option value="11">November</option>',
                                '04':'<option value="05">Mai</option><option value="06">Juni</option><option value="07">Juli</option><option value="08">August</option><option value="09">September</option><option value="10">Oktober</option><option value="11">November</option>',
                                '05':'<option value="06">Juni</option><option value="07">Juli</option><option value="08">August</option><option value="09">September</option><option value="10">Oktober</option><option value="11">November</option>',
                                '06':'<option value="07">Juli</option><option value="08">August</option><option value="09">September</option><option value="10">Oktober</option><option value="11">November</option>',
                                '07':'<option value="08">August</option><option value="9">September</option><option value="10">Oktober</option><option value="11">November</option>',
                                '08':'<option value="09">September</option><option value="10">Oktober</option><option value="11">November</option>',
                                '09':'<option value="10">Oktober</option><option value="11">November</option>',
                                '10':'<option value="11">November</option>'};

    $('#saison_start').change(function(){
        var selValue = $('#saison_start :selected').val();
        $('#saison_ende').attr('disabled',false);
        var dropdownOptions = evilDropDownObject[selValue];
        $('#saison_ende').html(dropdownOptions);
    });

    $('#grund_der_versicherung').change(function () {
        if ($(this).val() == "change") {
            $('#fahrzeugkennzeichen_row').css('display', 'block');
            $('#fin_row').css('display', 'block');
            //$('#nofincheckbox_row').css('display', 'block');
            $('#typschluesselnummer_row').css('display', 'block');
            $('#herstellerschluesselnummer_row').css('display', 'block');
            $('#changeTextFloating').show();
            /*$('#nofincheckbox').on('change', function(e){
                console.log(this);
            });*/
        } else {
            $('#fahrzeugkennzeichen_row').css('display', 'none');
            $('#fin_row').css('display', 'none');
            //$('#nofincheckbox_row').css('display', 'none');
            $('#typschluesselnummer_row').css('display', 'none');
            $('#herstellerschluesselnummer_row').css('display', 'none');
            $('#changeTextFloating').hide();
        }
    });
    if(window.innerWidth > 768){
        $('#herstellerschluesselnummer').on('focusout', function(){
            if($('#typschluesselnummer').hasClass('has-error')){
                $('#typschluesselnummer-error').remove();
            }
        });

        $('#typschluesselnummer').on('focusout', function(){
            if($('#herstellerschluesselnummer').hasClass('has-error')){
                $('#herstellerschluesselnummer-error').remove();
            }
        });
    }
    if(sessionStorage.getItem("vehicleTypeID") == 202005 || sessionStorage.getItem("FakePremium") == "yes"){
        $.each(insuranceData, function(i,v){
            if(v.calculationParameters[6].ID == 110002 && v.insuranceGroupID == baseTarifID ){
                kilometerNeeded = "yes";
                sessionStorage.setItem("kilometerNeeded","yes");
            }

        });
    }else{
        $.each(insuranceData.json.result, function(i,v){
            if(v.calculationParameters[6].ID == 110002 && v.insuranceGroupID == baseTarifID ){
                kilometerNeeded = "yes";
                sessionStorage.setItem("kilometerNeeded","yes");
            }

        });
    }

    var kilometerLeistungDropDownIDObject = {};
    $.each(kvsDetailList.json.result, function (i, v) {
        if (v.masterID == "110000") {
            if (v.ID > 110000 && v.ID < 110009 ) {
                kilometerLeistungDropDownArray.push(v.ID);

                kilometerLeistungDropDownIDObject[v.ID] = v.name;
            }
        }
        if (v.masterID == "107000") {
            leasingDropDown.push("<option value='" + v.ID + "'>" + v.name + "</option>");
        }
        var carData = JSON.parse(sessionStorage.getItem("#sel5"));
        if (v.masterID == "112000") {
            if (v.ID == 112005 || v.ID == 112006 || v.ID == 112007) {
                driversYearDropDown += "<option value='" + v.ID + "'>" + driverMatchinArray[v.ID] + "</option>";
            }
        }
        if (v.masterID == "108000") {
            if (v.ID == 108001 || v.ID == 108002 || v.ID == 108003) {
                kaufjahrDropDownArray.push("<option value='" + v.ID + "'>" + v.name + "</option>");
            }
        }
    });
    kilometerLeistungDropDownArray.sort();
    if(kilometerLeistungDropDownArray[2] < kilometerLeistungDropDownArray[3]){
        kilometerLeistungDropDownArray[4] = kilometerLeistungDropDownArray[2];
        kilometerLeistungDropDownArray[2] = undefined;
        kilometerLeistungDropDownArray.slice(2);
        kilometerLeistungDropDownArray.filter(function(val){return val});
    }
    $.each(kilometerLeistungDropDownArray, function(i,v){
        if(v != undefined){
            kilometerLeistungDropDown += "<option value='" + v + "'>" + kilometerLeistungDropDownIDObject[v] + "</option>";
        }
    });

    var fullLeasingDropDown = "<option value=''></option>" + leasingDropDown.reverse().join("");
    $('#leasing').html(fullLeasingDropDown);

    if(vehicleClassID == 100001 || vehicleClassID == 100002){
        var matchingObject = {100002: "Alle Fahrer mindestens 25 und jünger 76 Jahre"};
        $('#fahreralter').html("<option value=''></option><option value='112009'>"+matchingObject[100002]+"</option>"+driversYearDropDown);
    }else{
        $('#fahreralter').html("<option value=''></option><option value='112004'>Alle Fahrer mindestens 23 und jünger 76 Jahren</option><option value='112009'>Alle Fahrer mindestens 25 und jünger 76 Jahren</option>"+driversYearDropDown);
    }

    $('#fahrleistung').html(kilometerLeistungDropDown);
    if(kilometerNeeded === "no"){
        $('#kilometerLeistung').hide();
    }
    if(sessionStorage.getItem("FakePremium") == "yes" && sessionStorage.getItem("TarifID") == "10827"){
        $('#kilometerLeistung').show();
    }
    kaufjahrDropdown += kaufjahrDropDownArray.reverse();
    $('#kaufjahr').html(kaufjahrDropdown);
    $('#kennzeichen').change(function () {
        if ($(this).val() == 105003 || $(this).val() == 105012) {
            $('#saisonstart').css('display', 'block');
            $('#saisonende').css('display', 'block');
        } else {
            $('#saisonstart').css('display', 'none');
            $('#saisonende').css('display', 'none');
        }
    });
    $('#submitButton').click(function () {
        var fromString = $('form[name="Fahrzeugdaten2"]').serializeAssoc();
        if($('#grund_der_versicherung :selected').val() === "change"){
            var akz = /[a-zöüäA-ZÖÜÄ]{1,3}\-[a-zöüäA-ZÖÜÄ]{1,2}[0-9]{1,4}[hH]{0,1}/g;

            if(akz.test($('#fahrzeugkennzeichen').val())){
                var splittedAkz = $('#fahrzeugkennzeichen').val().replace(/$\H/,"").replace(/$\h/,"").split("-");
                var partTwoAkz = splittedAkz[1].split(/[0-9]{1,4}/);
                var partThreeAkz = splittedAkz[1].split(/[A-ZÖÜÄ]{1,2}/);
                var partOne = "";
                var partTwo = "";
                var partThree = "";
                if (splittedAkz[0].length < 3) {
                    var blankFields = "";
                    for (var i = 3, j = splittedAkz[0].length; j < i; j++) {
                        blankFields += " ";
                    }
                    partOne = splittedAkz[0] + blankFields;
                } else {
                    partOne = splittedAkz[0];
                }
                if (partTwoAkz[0].length < 2) {
                    for (var i = 2, j = partTwoAkz[0].length; j < i; j++) {
                        partTwo = partTwoAkz[0] + " ";
                    }
                } else {
                    partTwo = partTwoAkz[0];
                }
                if (partThreeAkz[1].length < 4) {
                    var blankFields = "";
                    for (var i = 4, j = partThreeAkz[1].length; j < i; j++) {
                        blankFields += " ";
                    }
                    partThree += blankFields + partThreeAkz[1];
                } else {
                    partThree += partThreeAkz[1];
                }
                fromString["fahrzeugkennzeichen"] = partOne + partTwo + partThree;
            }else{
                return false;
            }
        }
        if(fromString["reusecheckbox"] === "on"){
            fromString["wiederherstellungswert"] = $('#wiederherstellungswert_tausend').val()+""+$('#wiederherstellungswert_hundert').val();
        }
        fromString["marktwert_tausend"][0] = $('#marktwert_tausend :selected').val();
        fromString["marktwert_hundert"][0] = $('#marktwert_hundert :selected').val();
        sessionStorage.setItem("Fahrzeugdaten2", JSON.stringify(fromString));
    });

    if (sessionStorage.getItem("Fahrzeugdaten2") !== null) {
        fahrzeugDatenRefill();
    }
    $('#closeOverlay').on('click', function(){
        $('#confirmationOverlay').hide();
    });

    $('input[name="optradio"]').on('change', function(){
        if(this.id === "ohne_fahrschutz"){
            $('#modalMessageBody').html("Ich verzichte bewusst auf den umfassenden Schutz, den die Fahrerschutz-Versicherung mir und allen anderen berechtigten Fahrern meines Fahrzeugs bietet!");
            $('.team').html("Ihr CARISMA Support-Team");
            if(window.innerWidth < 375){
                $('#confirmationOverlay .contentBoxWrapper').css('width',window.innerWidth);
            }else if (window.innerWidth < 570){
                $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
            }
            $('#confirmationOverlay').show();
            $('#confirmationOverlay').css('display','table');
        }
    });

})();

function fahrzeugDatenRefill(){
    var fahrzeugdaten = JSON.parse(sessionStorage.getItem("Fahrzeugdaten2"));

    //console.log("in refill");

    $.each(fahrzeugdaten, function(id,v){
        if (v !== ""){
            if(id === "custom_type"){
                $('input:radio[name="custom_type"][value="'+v+'"]').prop('checked', true);
            }else if(id === "optradio"){
                $('input:radio[name="optradio"][value="'+v+'"]').prop('checked', true);
            }else if($('#'+id).is("input")){
                $('#'+id).val(v);
            }else if($('#'+id).is("select")){
                $("#"+id+" option:contains("+v+")").attr('selected', true);
                $('#'+id).val(v).change();
            }
            if(id == "marktwert"){
                var price = v.toString();

                var lastthree = price.length-3;
                var marktwert_hundert = price.substr(lastthree,v.length);

                var marktwert = price.replace(marktwert_hundert,"");

                $("#marktwert_tausend option:contains("+marktwert+")").attr('selected', true);
                $('#marktwert_tausend').val(marktwert).change();

                $("#marktwert_hundert option:contains("+marktwert_hundert+")").attr('selected', true);
                $('#marktwert_hundert').val(marktwert_hundert).change();

                $('#marktwert_tausend').addClass('has-no-error');
                $('#marktwert_hundert').addClass('has-no-error');
            }
            if(id == "wiederherstellungswert"){
                var price = v.toString();
                var lastthree = price.length-3;
                var wiederherstellunghundert = price.substr(lastthree,v.length);

                var wiederherstellungtausend = price.replace(wiederherstellunghundert,"");

                $("#wiederherstellungswert_tausend option:contains("+wiederherstellungtausend+")").attr('selected', true);
                $('#wiederherstellungswert_tausend').val(wiederherstellungtausend).change();

                $("#wiederherstellungswert_hundert option:contains("+wiederherstellunghundert+")").attr('selected', true);
                $('#wiederherstellungswert_hundert').val(wiederherstellunghundert).change();

                $('#reusecheckbox').prop("checked",true);
            }else if(id == "reusecheckbox"){
                $('#reusecheckbox').prop("checked",true);
            }else{
                $('#'+id).addClass('has-no-error');
            }
        }
    });
    $('#erstzulassung').find('option[selected="selected"]').each(function(){
        $(this).prop('selected', true);
    });
}

function isChrome() {
  var isChromium = window.chrome,
    winNav = window.navigator,
    vendorName = winNav.vendor,
    isOpera = winNav.userAgent.indexOf("OPR") > -1,
    isIEedge = winNav.userAgent.indexOf("Edge") > -1,
    isIOSChrome = winNav.userAgent.match("CriOS");

  if (isIOSChrome) {
    return true;
  } else if (
    isChromium !== null &&
    typeof isChromium !== "undefined" &&
    vendorName === "Google Inc." &&
    isOpera === false &&
    isIEedge === false
  ) {
    return true;
  } else {
    return false;
  }
}

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

function getDeductible(tarifId,vehicleId,reload,carValue){

    var vehData = sessionStorage.getItem("insuranceID").split(",");
    var today = new Date();
    var fullYear = today.getFullYear();
    //sessionStorage.setItem('vehicleTypeID',vehData[0]);
    var erstzulassung_jahr = '<option value="0"><option>';
    if(!reload){
        for(var i = sessionStorage.getItem("firstYear"); i <= fullYear; i++){
            erstzulassung_jahr += '<option value="'+i+'">'+i+'</option>';
        }
        $('#erstzulassung').html(erstzulassung_jahr);
        $('#erstzulassung').children('option:first').remove();
    }
    if(!vehData[3].match(/\d{4}/)){
        vehData[3] = sessionStorage.getItem("firstYear");
    }
    var idTarif = tarifId;
    if((sessionStorage.getItem("vehicleTypeID") == 202005 && idTarif == 10827) || (sessionStorage.getItem("FakePremium") == "yes" && sessionStorage.getItem("tarif") === "Premium")){
        idTarif = 10828;
    }
    var vehicleId = vehData[0];
    $.ajax({
        url: '../ajaxtest.php',
        method: "post",
        data: {"command": "deductable", "insuranceId": idTarif, "vehicleId": vehicleId},
        async: false,
        success: function (content) {
            var data = JSON.parse(content);

            /*if(sessionStorage.getItem("vehicleTypeID") == 202005 && sessionStorage.getItem("tarif") == "Premium"){
                var newData = JSON.parse(JSON.stringify(data));
                $.each(newData.json.result, function(i,v){
                    var newDeduc = JSON.parse(JSON.stringify(newData.json.result[i].full));
                    newDeduc.insuranceTypeName = "Allrisk";
                    newDeduc.insuranceTypeId = 205004;
                    newData.json.result[i].allRisk = newDeduc;
                });
                content = JSON.stringify(newData);
                data = JSON.parse(JSON.stringify(newData));
            }*/
            sessionStorage.setItem("deductible", content);

            var contentHtml = "";
            $.each(data.json.result, function (i, v) {
                if(v.hasOwnProperty('allRisk')){
                    contentHtml += "<option value='" + v.partial.deductibleID + "," + v.full.deductibleID + ","+ v.allRisk.deductibleID +"'>" + v.allRisk.insuranceTypeName + " "+ v.allRisk.deductibleName +" Euro SB, " + v.full.insuranceTypeName + " " + v.full.deductibleName + " Euro SB, " + v.partial.insuranceTypeName + " " + v.partial.deductibleName + " Euro SB</option>";
                }else if (v.hasOwnProperty('full')) {
                    contentHtml += "<option value='" + v.partial.deductibleID + "," + v.full.deductibleID + "'>" + v.full.insuranceTypeName + " " + v.full.deductibleName + " Euro SB, " + v.partial.insuranceTypeName + " " + v.partial.deductibleName + " Euro SB</option>";
                }else {
                    contentHtml += "<option value='" + v.partial.deductibleID + "'>" + v.partial.insuranceTypeName + " " + v.partial.deductibleName + " Euro SB</option>";
                }
                /*if(v.deviatingDeductibles.length > 0){
                    sessionStorage.setItem("deviatingDeductibles",JSON.stringify(v.deviatingDeductibles[0]));
                }*/
            });
            $('#selbstbehalt_paket').append(contentHtml);
            if (sessionStorage.getItem("Fahrzeugdaten2") != null) {
                var fahrzeugdaten = JSON.parse(sessionStorage.getItem("Fahrzeugdaten2"));
                $('#selbstbehalt_paket').val(fahrzeugdaten["selbstbehalt_paket"]).change();
            }
            if(reload){
                fahrzeugDatenRefill();

                var postData = {"command": "premiums", "vehId": vehicleId, "vehValue": carValue};

                $.ajax({
                    url: "../ajaxtest.php",
                    method: "post",
                    data: postData,
                    async: false,
                    success: function (content) {
                        var data = JSON.parse(content);
                        sessionStorage.setItem("getPremiums", content);
                        rebuildDropdowns(vehData);

                        // alle dropdowns von der Startseite wieder herstellen

                    }
                });
            }
        }
    });
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function rebuildDropdowns(vehData){
    var postData = {"command": "login"};
    var vehicleData = JSON.parse(sessionStorage.getItem("FullCarData"));
    $.ajax({
        url: "../ajaxtest.php",
        method: "post",
        data: postData,
        async: false,
        success: function (content) {
            sessionStorage.setItem("#sel1", content);
            var postData = {"command": "group", "id": vehicleData.vehGroup};
            $.ajax({
                url: "../ajaxtest.php",
                method: "post",
                data: postData,
                async: false,
                success: function (content) {
                    sessionStorage.setItem("#sel2", content);
                    var jetzt = new Date(),
                    j = jetzt.getFullYear();
                    var content = "<option></option>";
                    for (var i = j; i >= 1910; i--) {
                        content += "<option value='" + i + "'>" + i + "</option>";
                    }
                    sessionStorage.setItem("jahreszahlen", content);
                    var postData = {"command": "vehType", "id": vehData[3], "vehType":vehicleData.manufacturer,"vehGroupID":vehicleData.vehGroup};
                    sessionStorage.setItem("vehicleTypeID",vehicleData.vehGroup);
                    $.ajax({
                        url: "../ajaxtest.php",
                        method: "post",
                        data: postData,
                        async: false,
                        success: function (content) {
                            sessionStorage.setItem("#sel4", content);
                            var vehClass = vehData[2].split(" ");
                            var jsonData = JSON.parse(content);
                            var vehicleClass, vehicleModel = "";
                            $.each(jsonData.json.result, function(i,v){
                                if(v.series == vehClass[0]){
                                    vehicleClass = v.series;
                                    vehicleModel = v.modelSeries;
                                }
                            });

                            var postData = {
                                "command": "vehList",
                                "vehModel": vehicleModel,
                                "vehClass": vehicleClass,
                                "yearofconstruct": sessionStorage.getItem("firstYear"),
                                "brand": vehicleData.manufacturer
                            };
                            $.ajax({
                                url: "../ajaxtest.php",
                                method: "post",
                                data: postData,
                                async: false,
                                success: function (content) {
                                    sessionStorage.setItem("#sel5", content);

                                    $('#progressModal').modal("hide");
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

function checkWHD(){
    var wiederherstellung = $('#wiederherstellungswert_tausend :selected').val()+$('#wiederherstellungswert_hundert :selected').val();
    var marktwert = $('#marktwert_tausend :selected').val()+$('#marktwert_hundert :selected').val();

    var whd = parseInt(wiederherstellung);
    var mkt = parseInt(marktwert);

    if(wiederherstellung != "" && wiederherstellung > 0 && wiederherstellung != NaN){
        if(whd <= mkt){

            $('#wiederherstellungswert_hundert').removeClass("has-no-error");
            $('#wiederherstellungswert_tausend').removeClass("has-no-error");
            $('#wiederherstellungswert_hundert').addClass("has-error");
            $('#wiederherstellungswert_tausend').addClass("has-error");
            $('.reusetext').hide();
            $('#reusecheckbox').attr('checked', false);
            $('#reusecheckbox').prop('disabled', true);
            $('#completeWiederherstellungswert').hide();
        }
        if(whd > mkt){

            $('#wiederherstellungswert_tausend').addClass('has-no-error');
            $('#wiederherstellungswert_hundert').addClass('has-no-error');
            $('.reusetext').show();
            $('#reusecheckbox').prop("disabled",false);


            var whdStr = $('#wiederherstellungswert_tausend :selected').val()+"."+$('#wiederherstellungswert_hundert :selected').val();
            $('#completeWiederherstellungswert').html("Wiederherstellungswert: "+whdStr+" €");
            $('#completeWiederherstellungswert').show();
        }

        if(mkt > 99){
            var mktStr = $('#marktwert_tausend :selected').val()+"."+$('#marktwert_hundert :selected').val()+" €";
            $('#marktwert').val($('#marktwert_tausend :selected').val()+""+$('#marktwert_hundert :selected').val());
            $('#completeMarktwert').html("Marktwert: "+mktStr);
            $('#completeMarktwert').show();
            $('#marktwert_hundert').addClass("has-no-error");
            $('#marktwert_tausend').addClass("has-no-error");
        }

    }else{
        if(mkt > 99){
            $('#marktwert').val(mkt);
        }

        $('#wiederherstellungswert_hundert').removeClass("has-no-error");
        $('#wiederherstellungswert_tausend').removeClass("has-no-error");
        $('#wiederherstellungswert_hundert').removeClass("has-error");
        $('#wiederherstellungswert_tausend').removeClass("has-error");
    }
}
