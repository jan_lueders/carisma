/**
 * Created by root on 15.11.17.
 */

window.onload = function () {
    if (typeof history.pushState === "function") {
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
                    if(window.innerWidth < 415){
                        var computedWidth = window.innerWidth-20;
                        $('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
                    }else if (window.innerWidth < 570){
                        $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
                    }
                    $('#modalMessageBody').html("Bitte benutzen Sie die Naviagtionselemente auf der Seite .");
                    $('#confirmationOverlay > div > div > div > div').hide();
                    $('#confirmationOverlay').show();
                    $('#confirmationOverlay').css('display','table');
                    $('#closeOverlay').on('click', function(){
                            $('#confirmationOverlay').hide();
                    });
        };
    }
    else {
        var ignoreHashChange = true;
        window.onhashchange = function () {
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
            }
            else {
                ignoreHashChange = false;
            }
        };
    }

};

(function(){

    $('.finish_btn').attr('disabled',true);

    var OSName = "";
    if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
    var is_mobile = window.innerWidth <= 768;

    $('#overlay').attr('display','none');
    $('#furtherOk').on('click', function(){
        $('#overlay').hide();
    });
    premium = JSON.parse(sessionStorage.getItem("premium"));
    tarif = sessionStorage.getItem("tarif");
    if(sessionStorage.getItem("insuranceID")){
        vehData = sessionStorage.getItem("insuranceID").split(",");
        if (vehData[4] == " ") {
            if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
                if(tarif[1] == "Basis" || tarif[1] == "Basic"){
                    tarif[1] = "Komfort";
                }else if(tarif[1] == "Komfort" || tarif[1] == "Comfort"){
                    tarif[1] = "Premium";
                }
            }
            headLineString = decodeURI(tarif[1]+"-Tarif-Angebot für meinen "+vehData[1]+" "+vehData[2]+" von Baujahr "+vehData[3]+" mit"+vehData[5]);
        } else {
            if (sessionStorage.getItem("vehicleTypeID") === "202002" || sessionStorage.getItem("vehicleTypeID") === "202003") {
                if(tarif == "Basis" || tarif == "Basic"){
                    tarif = "Komfort";
                }else if(tarif == "Komfort" || tarif == "Comfort"){
                    tarif = "Premium";
                }
            }
            headLineString = decodeURI(tarif + "-Tarif " + vehData[1] + " " + vehData[2] + ", Baujahr " + vehData[3] + ", " + vehData[4] + ", " + vehData[5]);
        }
        $('#headlineData').html(headLineString);
    }

    if(sessionStorage.getItem('personalData') != null){
        personalDataJSON = JSON.parse(sessionStorage.getItem('personalData'));
        /*if(personalDataJSON.json.success !== "false"){
            $('#iban').val(personalDataJSON.json.result.iban);
            sessionStorage.setItem("Iban",personalDataJSON.json.result.iban);
        }*/
    }

    $('#wizard1').attr("href","?article_id=32&clang=1&restart=1");
    $('#wizard2').attr("href","?article_id=19&clang=1&tarif="+tarif);
    $('#wizard3').attr("href","?article_id=20&clang=1");
    $('#wizard4').attr("href","?article_id=21&clang=1");
    if(tarif == "Komfort"){
        tarif = "Comfort";
    }else if(tarif == "Basis"){
        tarif = "Basic";
    }
    //$('.finish_btn').attr('href')
    $('#sameperson').on('change', function(e){
        if(e.target.checked){
            if(sessionStorage.getItem("prename") !== null){
                personalDataJSON = JSON.parse(sessionStorage.getItem('personalData'));
                $('#accountholder').val(sessionStorage.getItem("prename")+" "+sessionStorage.getItem("lastname"));
                $('#accountholder').addClass('has-no-error');
            }
        }else{
            $('#accountholder').val("");
            $('#accountholder').removeClass('has-no-error');
        }
    });

    $('#takeoverIban').on('click', function(e){
        e.preventDefault();
        doneTyping();
        return false;
    });

    function doneTyping () {
        //$('#iban').attr('disabled',true);
        $('#progressModal').modal('show');
        iban = $("#iban").val().trim();
        $.ajax({
            url: '../ajaxtest.php',
            method: 'post',
            data: {
                "command": "checkIban",
                "iban": iban
            },
            success: function(data) {
                //$('#iban').attr('disabled', false);
                var result = JSON.parse(data);
                if(result.valid) {
                    sessionStorage.setItem("Iban",$('#iban').val());
                    $('#nameofbank').val(result.description);
                    $('#bic').val(result.bic);
                    sessionStorage.setItem("nameofbank",result.description);
                    sessionStorage.setItem("BIC",result.bic);
                    $('#nameofbank').addClass('has-no-error');
                    $('#bic').addClass('has-no-error');
                    console.log("success");
                    $('#progressModal').modal('hide');
                    $('.finish_btn').attr('disabled',false);
                }else{
                    $('#progressModal').modal('hide');
                }
            },
            error: function(){
                $('#iban').attr('disabled', false);
                $('#progressModal').modal('hide');
            }
        });
    }

    $('#amount').val(sessionStorage.getItem("Amount")+"€");
    $('#amount').addClass('has-no-error');
    $('#way_of_payment').addClass('has-no-error');
    $('#type_of_payment').addClass('has-no-error');
    $('.save_btn').on('click',function(){
        $('#progressModal').modal('show');
        if(sessionStorage.getItem("usersession") !== null){
            var postData = {"command": "saveCalc"};
            storageObject = dumpSessionStorageToVar();
            postData["storageData"] = storageObject;
            $.ajax({
                url: "../ajaxtest.php",
                method: "post",
                data: postData,
                success: function (content) {
                    data = JSON.parse(content);
                    sessionStorage.setItem("contractID",data.contractID);
                    $('#progressModal').modal('hide')
                    if(window.innerWidth < 415){
                            computedWidth = window.innerWidth-20;
                                    $('#confirmationOverlay .contentBoxWrapper').css('width',computedWidth);
                                }else if (window.innerWidth < 570){
                                    $('#confirmationOverlay .contentBoxWrapper').css('width','auto');
                                }
                                        $('#modalMessageBody').html("Ihre Daten wurden gespeichert.");
                        $('body').css('overflow','hidden');
                        $('#confirmationOverlay').css('display','table').fadeIn();
                        document.getElementById('closeOverlay').addEventListener("click", function(event) {
                            $('body').css('overflow','auto');
                                $('#confirmationOverlay').fadeOut();
                            });
                },
                error: function(){
                    $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                    $('#closeOverlay').on('click', function(){
                        window.location.href = "/?article_id=7&clang=1";
                    });
                }
            });
        }else{
            sessionStorage.setItem("wayofcicle","wayofcicle22");
            window.location.href = "?article_id=17&clang=1";
        }
    });
    $('.finish_btn').on('click',function(){
        sessionStorage.setItem("Iban",$('#iban').val());
        sessionStorage.setItem("way-of-payment",$('#way_of_payment :selected').val());
        sessionStorage.setItem("type-of-payment",$('#type_of_payment :selected').val());
        if($('#sameperson :checked')){
            sessionStorage.setItem("Bank-Accountholder",$('#accountholder').val());
        }else{
            sessionStorage.setItem("Bank-Accountholder",sessionStorage.getItem("prename")+" "+sessionStorage.getItem("lastname"));
        }
        //storageObject = dumpSessionStorageToVar();
        //postData["storageData"] = storageObject;
        /*$.ajax({
            url: "../ajaxtest.php",
            method: "post",
            data: postData,
            success: function (content) {
                data = JSON.parse(content);
                $('#closeOverlay').show();
                if (data.success != "true") {
                    $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                    $('#closeOverlay').on('click', function () {
                        window.location.href = "/?article_id=7&clang=1";
                    });
                } else {
                    $('#closeOverlay').on('click', function () {
                        $('#confirmationOverlay').hide();
                    });
                }
            },
            error: function () {
                $('#modalMessageBody').html("Der Vorgang war leider nicht erfolgreich, bitte wenden Sie sich an unseren Support.");
                $('#closeOverlay').on('click', function () {
                    window.location.href = "/?article_id=7&clang=1";
                });
            }
        });*/
    });
    if(is_mobile){
        $('#mobileSubmitButton').on('click', function(){
            $('#bankform').submit()
        });
    }
    $('input').each(function(i,v){
        if(v.value !== "" && v.id !== "sameperson"){
            $('#'+v.id).addClass('has-no-error');
        }
    });

})();
