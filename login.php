<?php

if(array_key_exists("loginformname",$_POST)){
	
    $urlConfig = parse_ini_file("config.ini");
    
	$email = $_POST["emailadresse"];
	$password = $_POST["pwInputA"];

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"]."get/login/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = array();
	$headers[] = "Accept: application/json";
	$headers[] = "username: ".$email;
	$headers[] = "password: ".$password;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
	error_log("===LOGIN-HEADER===".join(",",$headers));
	
	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	
	curl_close ($ch);

	$result2 = json_decode($result,true);
	
	error_log("===LOGIN-RESULT===".json_encode($result2));
	
	if(array_key_exists('redirectfield',$_POST)){
		if(strlen($_POST["redirectfield"]) > 0){
			$tmpPageid = substr($_POST["redirectfield"],-2);
			$pageId = intval($tmpPageid);
		}else{
			$pageId = 1;
		}
	}else{
		$pageId = 21;
	} 

	if($result2["success"] == "true"){

		//"angemeldet-bleiben  on
		if(isset($_POST["permloginchekbox"])){
			if($_POST["permloginchekbox"] == "on"){
				$date_of_expiry = time() + 60 * 60 * 12 ;
				setcookie ("usersession", $result2["sessiontoken"]."&".$email, $date_of_expiry, '/');
			}
		}
		header('Location: http://carisma.auto/?article_id='.$pageId.'&clang=1&session='.$result2["sessiontoken"].'='.$email."&lastpage=login");
		exit;
	}else{
		header('Location: http://carisma.auto/?article_id=17&clang=1&login=false');
		exit;
	}
	exit;
	
}else if(array_key_exists("formname",$_POST)){
	
	$email = $_POST["e-mail-adresse"];
	$password = $_POST["passwort"];
	$prename = $_POST["vorname"];
	$lastname = $_POST["nachname"];
	$salutation = $_POST["salutation"];
	
	$urlConfig = parse_ini_file("config.ini");
	
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"]."get/register/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	
	$headers = array();
	$headers[] = "Accept: application/json";
	$headers[] = "Username: ".$email;
	$headers[] = "Password: ".$password;
	$headers[] = "Prename: ".trim(json_encode($prename,JSON_UNESCAPED_SLASHES),'"');
	$headers[] = "Salutation:".$salutation;
	$headers[] = "Lastname: ".trim(json_encode($lastname,JSON_UNESCAPED_SLASHES),'"');
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
	error_log("===REGISTER-HEADER===".join(",",$headers));
	
	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	error_log($result);
	$result2 = json_decode($result,true);
	if(array_key_exists('redirectfield',$_POST)){
		if(strlen($_POST["redirectfield"]) > 0){
			$tmpPageid = substr($_POST["redirectfield"],-2);
			$pageId = intval($tmpPageid);
		}else{
			$pageId = 1;
		}
	}else{
		$pageId = 21;
	} 
	curl_close ($ch);

	if($result2["success"] == "true"){
		//"angemeldet-bleiben  on
		if(isset($_POST["permloginchekbox"])){
			if($_POST["permloginchekbox"] == "on"){
				$date_of_expiry = time() + 60 * 60 * 12 ;
				setcookie ("usersession", $result2["sessiontoken"], $date_of_expiry, '/');
			}
		}

		header('Location: http://carisma.auto/?article_id='.$pageId.'&clang=1&session='.$result2["sessiontoken"].'='.$email."&lastpage=reg");
		exit;
	}else{
		header('Location: http://carisma.auto/?article_id=18&clang=1&login=exists');
		exit;
	}
}else if(array_key_exists("resendformname",$_POST)){
	$username = $_POST["e-mail-adresse"];
	$ch = curl_init();
	
	$urlConfig = parse_ini_file("config.ini");
	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"]."get/resend/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = array();
	$headers[] = "Accept: application/json";
	$headers[] = "username: ".$username;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}

	curl_close ($ch);
	
	echo $result;
	exit;
}else if(array_key_exists("logout",$_POST)){
	$username = $_POST["username"];
	$sessionId = $_POST["session"];
	$ch = curl_init();

	$urlConfig = parse_ini_file("config.ini");
	curl_setopt($ch, CURLOPT_URL, $urlConfig["openviva"]."get/logout/");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	$headers = array();
	$headers[] = "Accept: application/json";
	$headers[] = "username: ".$username;
	$headers[] = "Sessiontoken: " . $sessionId;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}

	curl_close ($ch);

	echo $result;
	exit;
}


?>
