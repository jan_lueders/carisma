

$(document).ready(function () {
    var is_mobile;

    function check_mobile() {
        is_mobile = window.innerWidth <= 768;
    }

    window.addEventListener('resize', check_mobile);
    check_mobile();


    var infoMessageArray = {
        "mit_fahrschutz": "Bei einem selbstverschuldeten Unfall sind alle Personen, auch Mitfahrer, durch die Haftpflichtversicherung des Fahrzeugs geschützt, nur der Fahrer selbst nicht. Diese Lücke im Versicherungsschutz wird durch die Fahrerschutz-Versicherung geschlossen, die wie eine Vollkasko Versicherung für den Fahrer funktioniert. Die Fahrerschutz-Versicherung ersetzt dem Fahrer z.B. Verdienstausfall, behindertengerechte Umbaumaßnahmen von Haus, Wohnung und Fahrzeug und zahlt Hinterbliebenenrente. Die Schadensdeckung beträgt bis zu 15 Mio. EUR.",
        "ohne_fahrschutz": "Bei einem selbstverschuldeten Unfall sind alle Personen, auch Mitfahrer, durch die Haftpflichtversicherung des Fahrzeugs geschützt, nur der Fahrer selbst nicht. Diese Lücke im Versicherungsschutz wird durch die Fahrerschutz-Versicherung geschlossen, die wie eine Vollkasko Versicherung für den Fahrer funktioniert. Die Fahrerschutz-Versicherung ersetzt dem Fahrer z.B. Verdienstausfall, behindertengerechte Umbaumaßnahmen von Haus, Wohnung und Fahrzeug und zahlt Hinterbliebenenrente. Die Schadensdeckung beträgt bis zu 15 Mio. EUR.",
        "geplante_zulassung": "Bei einer Neuzulassung (Sie besitzen ein Fahrzeug ohne Versicherungsschutz z.B. nach einem Kauf oder nach einer Abmeldung und wollen dieses Fahrzeug anmelden oder erneut anmelden) tragen Sie einfach das ungefähre Datum ein, an dem SIe das Fahrzeug anmelden wollen. Bei einem Versichererwechsel muss der Tag der geplanten Zulassung zwingend exakt der Tag nach dem Ablauf der Vorversicherung sein.",
        "grund_der_versicherung": "Wenn Sie ein Fahrzeug beliebigen Alters ohne Versicherungsschutz besitzen, also z.B. nach einem Kauf oder nach einer Abmeldung und Sie dieses Fahrzeug dann versichern und anmelden (oder wieder anmelden wollen) ist dies eine Neuzulassung. Sie erhalten hier Online eine elektronische Versicherungsbestätigung (eVB), mit der Sie ihr Fahrzeug unter Vorlage Ihres Fahrzeugbriefes beim Straßenverkehrsamt zulassen können. Bei einem Versichererwechsel erhalten Sie ebenfalls eine eVB, diese wird aber vom neuen Versicherer verwendet, um Ihr Fahrzeug ganz ohne Aufwand für Sie, zu dem von Ihnen gewünschten Stichtag umzumelden. Weil die Ummeldung in diesem Fall durch den neuen Versicherer ohne die Vorlage des Fahrzeugscheins beim Strassenverkehrsamt erfolgt, benötigt dieser alle Angaben des Fahrzeugscheins, die Sie als Halter und das Fahrzeug eindeutig identifizieren. Daher benötigen wir von Ihnen hier noch das amtliche Fahrzeug-Kennzeichen, die Fahrzeug-Identifikationsnummer (FIN), die Herstellerschlüsselnummer (HSN) und die Typschlüsselnummer (TSN). Alle diese Angaben finden Sie auf Ihrem Fahrzeugschein.",
        "herstellerschluesselnummer": "Bei einem Versichererwechsel benötigen wir zur eindeutigen Identifizierung Ihres Fahrzeugs die Herstellerschlüsselnummer (HSN). Dies ist ein vierstelliger Zahlencode, der in Ihrem gültigen Fahrzeugschein auf der Seite mit dem Halternamen im mittleren Teil oben unter Schlüsselnummer 2.1. steht. Bei älteren Fahrzeugscheinen befindet sich die Nummer unter dem Punkt „zu 2“.",
        "typschluesselnummer": "Bei einem Versichererwechsel benötigen wir zur eindeutigen Identifizierung Ihres Fahrzeugs auch die Typschlüsselnummer (TSN). Dies ist ein dreistelliger Code (Buchstaben und Zahlen möglich), der in Ihrem gültigen Fahrzeugschein auf der Seite mit dem Halternamen im mittleren Teil oben unter Schlüsselnummer 2.2.steht. Verwenden Sie die ersten drei Zahlen oder Buchstaben rechts neben dem Schlüsselnummer 2.2 Kästchen. Bei älteren Fahrzeugscheinen befindet sich der Code unter dem Punkt „zu 3“. Verwenden Sie die ersten drei Zahlen oder Buchstaben rechts neben dem “zu 3” Kästchen.",
        "fahrzeugkennzeichen":"Bitte geben Sie Ihr Kennzeichen mit Bindestrich und ohne Leerzeichen ein, so wie es in Ihren Fahrzeugpapieren steht. z.B. AB-YZ0001 oder AB-YZ0001H (für H-Kennzeichen)",
        "fin":"Bei einem Versichererwechsel benötigen wir zur eindeutigen Identifizierung des Fahrzeugs die Fahrzeug-Identifikationsnummer (FIN). Die Fahrzeug-Identifikationsnummer finden Sie auf Ihrem gültigen Fahrzeugschein auf der Seite mit dem Halternamen im mittleren Teil unter Ziffer E. Dies ist eine 17 stellige Kombination aus Buchstaben und Zahlen. Geben SIe diese bitte exakt in das Feld ein.",
        "tacho_einheit": "Bitte geben Sie ein, in welcher Einheit - Kilometer oder Meilen - Ihr Tachometer geeicht ist. Die Einheit Meilen kann für britische Fahrzeuge und Fahrzeuge aus den USA mit Tachometern, die nicht in Deutschland umgerüstet wurden und noch in Meilen geeicht sind, gleichermaßen verwendet werden.",
        "tacho_stand": "Bitte geben Sie hier den aktuellen Ziffernstand ihres Tachometers ein. Sie brauchen nur abzulesen und im Falle von Meilen NICHT umzurechnen.",
        "fahrleistung": "Bitte wählen Sie den Bereich, der am besten auf Sie zutrifft, aus der Liste aus. Die Angaben sind in Kilometern, auch wenn Sie im Feld Tachometereinheit Meilen angegeben haben. Die Angabe wieviele Kilometer Sie im Jahr voraussichtlich fahren werden, wird bei einigen Fahrzeugen für eine genauere Berechnung Ihres Versicherungstarifes verwendet.",
        "wiederherstellungswert": "Der Wiederherstellungswert ist der Wert der sich für viele restaurierte klassische Fahrzeuge ergibt. Er kann aber auch den Wert von überdurchschnittlich gut erhaltenen Fahrzeugen, die nicht restauriert wurden, darstellen. Da dieser Wert erheblich vom Marktwert abweichen kann, haben Sie die Möglichkeit den Wiederherstellungswert zu versichern. Wenn Sie hier einen Wiederherstellungswert eintragen, wird dieser Wert als Grundlage für die Prämienberechnung verwendet. Sie müssen den Wiederherstellungswert über ein detailliertes Gutachten (nicht älter als 3 Jahre) nachweisen, dass den Wert eindeutig belegt. Dazu haben Sie 8 Wochen Zeit. Für den Fall, dass Sie nach dieser Frist kein Gutachten vorlegen können, wird der versicherte Wert auf den Marktwert/Wiederbeschaffungswert reduziert und der Versicherungsschein geändert. Im Schadensfall innerhalb der Frist von 8 Wochen, muss der Wiederherstellungswert über das bestehende Gutachten nicht älter als 3 Jahre nachweisbar sein.",
        "erstzulassung_monat": "Bitte tragen Sie Monat und Jahr der Erstzulassung Ihres Fahrzeugs ein. Das Datum finden Sie in Ihrem gültigen Fahrzeugschein auf der Seite mit dem Halternamen in der Mitte der Seite, oben links unter Ziffer B. In Fahrzeugscheinen von vor dem 1.10.2005 finden Sie das Datum unter Ziffer 32 “Tag der ersten Zulassung”.Wenn Sie nur einen Fahrzeugbrief besitzen, weil Ihr Fahrzeug nicht angemeldet ist, finden Sie das Datum im neuen Fahrzeugbrief wieder unter Ziffer B “Datum der Erstzulassung des Fahrzeugs” und im Fahrzeugbrief von vor dem 1.10.2005 wieder unter Ziffer 32 “Tag der ersten Zulassung”.",
        "erstzulassung": "Bitte tragen Sie Monat und Jahr der Erstzulassung Ihres Fahrzeugs ein. Das Datum finden Sie in Ihrem gültigen Fahrzeugschein auf der Seite mit dem Halternamen in der Mitte der Seite, oben links unter Ziffer B. In Fahrzeugscheinen von vor dem 1.10.2005 finden Sie das Datum unter Ziffer 32 “Tag der ersten Zulassung”.Wenn Sie nur einen Fahrzeugbrief besitzen, weil Ihr Fahrzeug nicht angemeldet ist, finden Sie das Datum im neuen Fahrzeugbrief wieder unter Ziffer B “Datum der Erstzulassung des Fahrzeugs” und im Fahrzeugbrief von vor dem 1.10.2005 wieder unter Ziffer 32 “Tag der ersten Zulassung”.",
        "marktwert": "Bitte geben Sie hier den Marktwert oder Wiederbeschaffungswert ihres Fahrzeugs in Euro ein. Der Marktwert/Wiederbeschaffungswert ist der Wert des Fahrzeugs der versichert wird.",
        "standort_plz": "Bitte geben Sie die Postleitzahl ein",
        "standort_strasse": " Bitte geben Sie die Straße des Fahrzeug Standortes ein.",
        "standort_ort": "Bitte geben Sie den Fahrzeug Standort ein.",
        "housenumber": "Bitte geben Sie die Hausnummer ihres Wohnortes ein",
        "personalHousenumber": "Bitte geben Sie die Hausnummer des Fahrzeug Standortes ein",
        "kennzeichen": "Sie haben die Auswahl zwischen 4 Kennzeichenarten. Das normale schwarze Kennzeichen ist das Kennzeichen, das die meisten Fahrzeuge haben und das ganzjährig gültig ist. Das Historische Kennzeichen oder H Kennzeichen wird für qualifizierte Oldtimer vergeben und hat in der Regel steuerliche Vorteile jedoch keine Auswirkungen auf den Versicherungstarif hier. Weiterführende Informationen zum H Kennzeichen finden Sie auch in den FAQ auf dieser Seite. Das Saisonkennzeichen gilt für eine bestimmte Folge von Monaten während des Jahres. Sowohl Steuer als auch Versicherung werde anteilig berechnet. Eine Saison kann allerdings nicht über die WIntermonate gebucht werden, sondern vom Frühjahr bis zum Herbst. Erster Startmonat ist der März, letzter Monat der Saison ist der November. Das historische Saisonkennzeichen oder H Saisonkennzeichen gilt in Verbindung mit einem H Kennzeichen und vereinigt alle Merkmale des H Kennzeichens und der Saisonkennzeichens.",
        "fahreralter": "Um Ihre Versicherung für klassische Fahrzeuge so günstig wie möglich zu gestalten, fliesst das Alter der Fahrer aufgrund der mit dem Alter zunehmenden Erfahrung mit in die Berechnung ein. Bitte wählen Sie einen Bereich aus, der auf das Alter aller Fahrer, die das Fahrzeug fahren werden zutrifft.",
        "leasing": "Seit einiger Zeit gibt es auch interessante Leasingangebote für klassische Fahrzeuge. Bitte geben Sie an, ob ihr Fahrzeug geleast ist oder nicht.",
        "kaufjahr": "Bitte geben Sie den zeitlichen Bereich an, in dem Sie Ihr Fahrzeug erworben oder geleast haben.",
        "selbstbehalt_paket": "Bitte wählen Sie ihr Selbstbehaltspaket",
        "firstname": "Bitte geben Sie ihren Vornamen ein",
        "lastname": "Bitte geben Sie ihren Nachnamen ein",
        "gebdate":"Bitte geben Sie Ihr Geburtsdatum ein. Das benötigte Eingabeformat für das Trennzeichen (TT.MM.YYYY oder TT/MM/YYYY) kann Browserabhängig sein",
        "birthday":"Bitte wählen Sie ihren Geburtstag aus",
        "birthmonth":"Bitte wählen Sie ihren Geburtsmonat aus",
        "birthyear":"Bitte wählen Sie ihr Geburtsjahr aus",
        "street": "Bitte geben Sie ihre Straße ein",
        "zip": "Bitte geben Sie ihre Postleitzahl und ihren Ort ein",
        "city": "Bitte geben Sie ihre Postleitzahl und ihren Ort ein",
        "form_of_address": "Bitte wählen Sie ihre Anrede aus",
        "sameperson": "Bitte auswählen wenn gleich",
        "accountholder": "Bitte geben Sie den Kontoinhaber ein",
        "iban": "Bitte geben Sie ihre IBAN an",
        "bic": "Bitte geben Sie die BIC an",
        "amount": "Ihr Versicherungsbetrag",
        "way_of_payment": "Bitte wählen Sie die Zahlungsweise",
        "saison_start": "Bitte geben Sie hier den Monat ein, in dem ihre persönliche Classic Car Saison beginnt",
        "saison_ende": "Bitte geben Sie hier den Monat ein, in dem ihre persönliche Classic Car Saison endet",
        "type_of_payment": "Bitte wählen Sie die Zahlungsart",
        "nameofbank": "Bitte geben Sie den Namen ihrer Bank an",
        "zustandsDivID": "Zustandsnote 1 = Makelloser Zustand und wie neu Zustandsnote,<br />Zustandsnote 2 = Guter Zustand. Mängelfrei aber mit leichten Gebrauchsspuren,<br /> Zustandsnote 3 = Gebrauchter Zustand aber ohne größere technische und optische Mängel, voll fahrbereit und verkehrssicher,<br />Zustandsnote 4 = Verbrauchter Zustand. Nur eingeschränkt fahrbereit. Sofortige Arbeiten zur erfolgreichen Hauptuntersuchung (TÜV, DEKRA) sind notwendig. Zustandsnote 5 = Restaurierungsbedürftiger Zustand. Fahrzeug in mangelhaftem, nicht fahrbereitem Zustand. Umfangreiche Arbeiten an allen Baugruppen erforderlich.",
        "zustands_note1": "Zustandsnote 1 = Makelloser Zustand und wie neu",
        "zustands_note2": "Zustandsnote 2 = Guter Zustand. Mängelfrei aber mit leichten Gebrauchsspuren",
        "zustands_note3": "Zustandsnote 3 = Gebrauchter Zustand aber ohne größere technische und optische Mängel, voll fahrbereit und verkehrssicher",
        "zustands_note45": "Zustandsnote 4 = Verbrauchter Zustand. Nur eingeschränkt fahrbereit. Sofortige Arbeiten zur erfolgreichen Hauptuntersuchung (TÜV, DEKRA) sind notwendig. Zustandsnote 5 = Restaurierungsbedürftiger Zustand. Fahrzeug in mangelhaftem, nicht fahrbereitem Zustand. Umfangreiche Arbeiten an allen Baugruppen erforderlich.",
        "correct": "Bitte aktivieren Sie dieses Kästchen, wenn Sie fortfahren möchten",
        "correct_right": "Bitte aktivieren Sie dieses Kästchen, wenn Sie fortfahren möchten",

        "emailadresse": "",
        "pwInputA": "",
        "pwInputB": "",
        "nachname": "",
        "vorname": "",
        "permloginchekbox": "Durch Anklicken akzeptieren Sie ein Cookie, durch welches Sie für einen Zeitraum von einer Woche automatisch angemeldet werden, wenn Sie unsere Seite besuchen.",
        "personalStreet": "Bitte geben Sie Ihre Wohnadresse ein",
        "personalzip": "Bitte geben Sie ihre Postleitzahl und ihren Ort ein",
        "personalcity": "Bitte geben Sie ihre Postleitzahl und ihren Ort ein",
        "mobilenumber": "Diese Angabe ist freiwillig"
    };
    // ---------------------  Hinweise --------------------------------------------------

    // mobile version
    if (is_mobile) {
        // add info i
        $('.form-group label').each(function () {
            var $label = $(this);
            var id = $label.attr('for');
            if (!infoMessageArray[id]) return;

            var $trigger = $('<div class="info-trigger"></div>');
            $label.before($trigger);

            $trigger.click(function () {
                if ($label.next().is(':not(.flag.hint)')) {
                    $label.after('<div class="flag hint"><div>' + infoMessageArray[id] + '</div></div>');
                }else{
                    $('.hint').remove();
                }
            });
        });
    }


    // desktop version
    if (!is_mobile) {
        $('input, select, .radio, label, li, b').mouseover(function () {
            var $this = $(this);

            if ($this.is('input') || $this.is('select') || $this.is('label')) {

                var id = $this.attr('id');
                if(!$this.hasClass('has-error')){
                    // checkbox auf beitrags_summary
                    if (id === 'correct' || id === 'correct_right') {
                        $this.next().after('<div class="flag hint"><div>' + infoMessageArray[id] + '</div></div>');
                        // standardfall
                    } else if (id != undefined && infoMessageArray[id] !== '') {
                        var patt = /^zustands/g;
                        var result = patt.test(id);
                        if(!result){
                            $this.closest('.form-group').find('.form-control').last().after('<div class="flag hint"><div>' + infoMessageArray[id] + '</div></div>');
                        }else{
                            if($("#custom_type-error").length < 1 || $("#optradio-error").length < 1){
                                $this.children('input').after('<div class="flag hint"><div>' + infoMessageArray[$this.attr('for')] + '</div></div>');
                            }
                        }
                        // keine Ahnung wofür
                    }else if (infoMessageArray[$this.attr('for')] !== '' && infoMessageArray[$this.attr('for')] !== undefined ) {
                        var patt = /^zustands/g;
                        var result = patt.test($this.attr('for'));
                        if(!result){
                            if($this.children('input').next().attr('localName') === "b"){
                                $this.children('input').next().after('<div class="flag hint"><div>' + infoMessageArray[$this.attr('for')] + '</div></div>');
                            }else{
                                if($("#custom_type-error").length < 1 || $("#optradio-error").length < 1){
                                    $this.children('input').last().after('<div class="flag hint"><div>' + infoMessageArray[$this.attr('for')] + '</div></div>');
                                }
                            }

                        }else{
                            if($("#custom_type-error").length < 1 || $("#optradio-error").length < 1){
                                $this.children('input').after('<div class="flag hint"><div>' + infoMessageArray[$this.attr('for')] + '</div></div>');
                            }
                        }
                    }
                }

            } else if ($this.children().is('label')) {
                if (infoMessageArray[$this.children('label').children('input').attr('id')] !== '') {
                    $('#zustandsnote').parent().after('<div class="flag hint"><div>' + infoMessageArray[$this.children("label").children("input").attr('id')] + '</div></div>');
                }
            }

        }).mouseleave(function () {
            $('.hint').remove();
        });
    }


    $('input,select').keydown(function (event) {
        var inputs = $(this).closest('form').find(':input');
        if (event.which == 13) {
            if (inputs.index(this) != inputs.length && inputs.index(this) > 1) {
                event.preventDefault();
                inputs.eq(inputs.index(this) + 1).focus();
            } else {
                $('form').submit();
            }

        }
    });

    //  ---------------------- Warnungen -------------------------------------------------

    $.validator.setDefaults({
        highlight: function (element) {
            var $element = $(element);
            $element.closest('.form-control').removeClass('has-no-error').addClass('has-error');
            $element.siblings('.hint').hide();
        },
        unhighlight: function (element) {
            var $element = $(element);
            $element.closest('.form-control').removeClass('has-error').addClass('has-no-error');
            $element.siblings('.hint').hide();
        }
    });
    jQuery.validator.addMethod("lettersonlys", function(value, element) {
	return this.optional(element) || /^[A-Za-z\.\-\s\,]*$/.test(value);
    }, '<div class="warning"> Bitte geben Sie nur Buchstaben ein</div>');
    $.validator.addMethod(
        "date",
        function ( value, element ) {
            return this.optional(element) || !/Invalid|NaN/.test(new Date( value ));
        },
        '<div class="warning">Bitte geben Sie ein gültiges Datum nach dem Format dd.mm.yyyy ein</div>'
    );

    $("#userform").validate({
        errorElement: "span",
        errorClass: "flag error",
        onclick: function (element) {
            if (element.nodeName == "SELECT") {
                if(element.id == "birthday" || element.id == "birthmonth" || element.id == "birthyear"){
                    if($('#birthday :selected').val() == "" && $('#birthmonth :selected').val() == "" && $('#birthyear :selected').val() == ""){
                        $("#birthday").removeClass('has-no-error');
                        $("#birthday").addClass('has-error');

                        $("#birthmonth").removeClass('has-no-error');
                        $("#birthmonth").addClass('has-error');

                        $("#birthyear").removeClass('has-no-error');
                        $("#birthyear").addClass('has-error');
                    }
                    if($('#birthday :selected').val() != "" && $('#birthmonth :selected').val() == "" && $('#birthyear :selected').val() == ""){
                        $("#birthday").removeClass('has-error');
                        $("#birthday").addClass('has-no-error');

                        $("#birthmonth").removeClass('has-no-error');
                        $("#birthmonth").addClass('has-error');

                        $("#birthyear").removeClass('has-no-error');
                        $("#birthyear").addClass('has-error');
                    }
                    if($('#birthday :selected').val() != "" && $('#birthmonth :selected').val() != "" && $('#birthyear :selected').val() == ""){
                        $("#birthday").removeClass('has-error');
                        $("#birthday").addClass('has-no-error');

                        $("#birthmonth").removeClass('has-error');
                        $("#birthmonth").addClass('has-no-error');

                        $("#birthyear").removeClass('has-no-error');
                        $("#birthyear").addClass('has-error');
                    }
                    if($('#birthday :selected').val() != "" && $('#birthmonth :selected').val() != "" && $('#birthyear :selected').val() != ""){
                        $("#birthday").removeClass('has-error');
                        $("#birthday").addClass('has-no-error');

                        $("#birthmonth").removeClass('has-error');
                        $("#birthmonth").addClass('has-no-error');

                        $("#birthyear").removeClass('has-error');
                        $("#birthyear").addClass('has-no-error');
                    }
                    if($('#birthday :selected').val() == "" && $('#birthmonth :selected').val() != "" && $('#birthyear :selected').val() == ""){
                        $("#birthday").removeClass('has-no-error');
                        $("#birthday").addClass('has-error');

                        $("#birthmonth").removeClass('has-error');
                        $("#birthmonth").addClass('has-no-error');

                        $("#birthyear").removeClass('has-no-error');
                        $("#birthyear").addClass('has-error');
                    }
                    if($('#birthday :selected').val() == "" && $('#birthmonth :selected').val() == "" && $('#birthyear :selected').val() != ""){
                        $("#birthday").removeClass('has-no-error');
                        $("#birthday").addClass('has-error');

                        $("#birthmonth").removeClass('has-no-error');
                        $("#birthmonth").addClass('has-error');

                        $("#birthyear").removeClass('has-error');
                        $("#birthyear").addClass('has-no-error');
                    }

                }else if (element.value != -1 || element.value != "-1") {
                    this.element(element);
                } else if(element.value == -1 || element.value == "-1"){
                  $(this).removeClass('has-no-error');
                  $(this).addClass('has-error');
                }
            } else {
                $(this).removeClass('has-no-error');
            }
        },
        onfocusout: function (element) {
            console.log("focusOut");
            if(element.value == 0 || element.value != ""){
                return false;
            }else if(element.value == ""){
                $("#" + element.id).addClass('has-error');
            }else{
                this.element(element);
            }

        },
        rules: {
            form_of_address: {
                required: true
            },
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            street: {
                required: true,
		lettersonlys: true
            },
            birthday:{
                required: true
            },
            birthmonth:{
                required: true
            },
            birthyear:{
                required: true
            },
            city: {
                required: true
            },
            zip: {
                required: true,
                number: true
            },
            housenumber: {
                required: true,
                number: true
            },
            mobilenumber: {
                number: true
            },
            personalHousenumber: {
                required: function(){
                    return !$('#sameAddress').attr('checked');
                }
            },
            personalStreet: {
                required: function(){
                    return !$('#sameAddress').attr('checked');
                },
		lettersonlys: function(){
                    return !$('#sameAddress').attr('checked');
                }
            },
            personalcity: {
                required: function(){
                    return !$('#sameAddress').attr('checked');
                }
            },
            personalzip: {
                required: function(){
                    return !$('#sameAddress').attr('checked');
                }
            },
            personalHousenumber: {
                required: function(){
                    return !$('#sameAddress').attr('checked');
                },
                number: true
            }
        },
        groups:{
            city: "zip city"
        },
        messages: {
            form_of_address: {
                required: '<div class="warning"> Bitte geben Sie die gewünschte Anrede ein</div>'
            },
            firstname: {
                required: '<div class="warning"> Bitte geben Sie Ihren Vornamen ein</div>'
            },
            lastname: {
                required: '<div class="warning"> Bitte geben Sie Ihren Nachnamen ein</div>'
            },
            birthday: {
                required: '<div class="warning">Bitte wählen Sie einen Tag aus</div>'
            },
            birthmonth: {
                required: '<div class="warning">Bitte wählen Sie einen Monat aus</div>'
            },
            birthyear: {
                required: '<div class="warning">Bitte wählen Sie ein Jahr aus</div>'
            },
            street: {
                required: '<div class="warning"> Bitte geben Sie Ihre Straße ein</div>'
            },
            city: {
                required: '<div class="warning"> Bitte geben Sie einen gültigen Wohnort ein</div>'
            },
            zip: {
                required: '<div class="warning"> Bitte geben Sie eine gültige Postleitzahl ein</div>'
            },
            housenumber: {
                required: '<div class="warning"> Bitte geben Sie eine gültige Hausnummer ein</div>',
                number:  '<div class="warning"> Bitte geben Sie eine gültige Hausnummer ein</div>'
            },
            mobilenumber: {
                number:  '<div class="warning"> Bitte geben Sie nur Zahlen ein</div>'
            },
            personalStreet: {
                required: '<div class="warning"> Bitte geben Sie Ihre Straße ein</div>'
            },
            personalcity: {
                required: '<div class="warning"> Bitte geben Sie einen gültigen Wohnort ein</div>'
            },
            personalzip: {
                required: '<div class="warning"> Bitte geben Sie eine gültige Postleitzahl ein</div>'
            },
            personalHousenumber: {
                required: '<div class="warning"> Bitte geben Sie eine gültige Hausnummer ein</div>',
                number:  '<div class="warning"> Bitte geben Sie eine gültige Hausnummer ein</div>'
            },
        },
        submitHandler: function (form) {
            if($('#birthday :selected').val() == -1){
              $('#birthday').removeClass('has-no-error');
              $('#birthday').addClass('has-error');
              return false;
            }
            if($('#birthyear :selected').val() == -1){
              $('#birthyear').removeClass('has-no-error');
              $('#birthyear').addClass('has-error');
              return false;
            }
            if($('#birthmonth :selected').val() == -1){
              $('#birthmonth').removeClass('has-no-error');
              $('#birthmonth').addClass('has-error');
              return false;
            }
            if($('#mobilenumber').val() != ""){
                $('#mobilenumber').val().replace("/","").replace("-","").trim();
            }
            
            form.submit();

        }
    });

    $("#bankform").validate({
        errorElement: "span",
        errorClass: "flag error",
        onclick: function (element) {
            this.element(element);
        },
        onfocusout: function (element) {
            this.element(element);
        },
        rules: {
            accountholder: {
                required: true
            },
            iban: {
                required: true,
                iban: true
            },
            bic: {
                required: true
            },
            way_of_payment: {
                required: true
            },
            type_of_payment: {
                required: true
            },
            amount: {
                required: true,
                number: true
            }
        },
        messages: {
            accountholder: {
                required: '<div class="warning"> Bitte geben Sie den Kontoinhaber ein</div>'
            },
            iban: {
                required: '<div class="warning"> Bitte geben Sie Ihre IBAN ein</div>',
                iban: '<div class="warning"> Bitte korrigieren Sie Ihre IBAN</div>'
            },
            bic: {
                required: '<div class="warning"> Bitte geben Sie Ihre BIC ein</div>'
            },
            way_of_payment: {
                required: '<div class="warning"> Bitte wählen Sie die Zahlungsweise aus</div>'
            },
            type_of_payment: {
                required: '<div class="warning"> Bitte wählen Sie die Zahlungsart aus</div>'
            },
            amount: {
                required: '<div class="warning">Bitte geben Sie einen Betrag ein</div>'
            }
        },
        submitHandler: function (form) {
                form.submit();
        }
    });

    function validateEmail(email) {
        var rex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        return rex.test(email);
    }

    var emailInput = document.getElementById('emailadresse'),
        pwInput = document.getElementById('pwInputA');

    if(emailInput != null){
        // Prüfe die Mailadresse mit jeder Eingabe.
        emailInput.addEventListener("input", function(){
            if ( validateEmail(this.value) ) {
                    emailInput.className = '';
                    emailInput.previousElementSibling.className = '';
            }
        });

        emailInput.addEventListener("focusout", function(){
            if ( validateEmail(this.value) ) {
                    emailInput.className = '';
                    emailInput.previousElementSibling.className = '';
            } else {
                    emailInput.className = 'error';
                    emailInput.previousElementSibling.className = 'error';
            }
        });
    }

    if(pwInput != null){
        // Prüfe die Mailadresse mit jeder Eingabe.
        pwInput.addEventListener("input", function(){
            if ( validateEmail(this.value) ) {
                    emailInput.className = '';
                    emailInput.previousElementSibling.className = '';
            }
        });

        pwInput.addEventListener("focusout", function(){
            if ( pwInput.value !== "" ) {
                pwInput.className = '';
                pwInput.previousElementSibling.className = '';
            } else {
                pwInput.className = 'error';
                pwInput.previousElementSibling.className = 'error';
            }
        });
    }

    var page19 = $('#fahrzeugdaten2').validate({
        debug: false,
        onclick: function (element) {
            var $element = $(element);

            if (element.nodeName == "SELECT") {
                if (element.value && element.value != 0 && element.value != -1 && element.id !== "erstzulassung_monat" && element.id !== "erstzulassung") {
                    this.element(element);
                } else {
                    if($element.attr('id') == "erstzulassung"){

                        if($element.val() !== "" && $('#erstzulassung_monat :selected').val() == ""){
                            $element.css('border', '1px solid #4DA42C');
                        }else if($element.val() !== "" && $('#erstzulassung_monat :selected').val() !== ""){
                            $element.css('border', '1px solid #4DA42C');
                            $element.addClass('has-no-error');
                            $('#erstzulassung_monat').css('border', '1px solid #4DA42C');
                        }else if($element.val() === ""){
                            $element.css('border', '1px solid #CCC');
                            $element.removeClass('has-no-error');
                        }
                    }else if($element.attr('id') == "erstzulassung_monat"){

                        if($element.val() !== "" && $('#erstzulassung :selected').val() == ""){
                            $element.css('border', '1px solid #4DA42C');
                            $('#erstzulassung').removeClass("has-no-error");
                        }else if($element.val() !== "" && $('#erstzulassung :selected').val() !== ""){

                            $element.css('border', '1px solid #4DA42C');
                            $element.addClass('has-no-error');
                            $('#erstzulassung').css('border', '1px solid #4DA42C');
                        }else if($element.val() === ""){

                            $element.css('border', '1px solid #CCC');
                            $element.removeClass('has-no-error');
                        }
                    }else{
                        $element.removeClass('has-no-error');
                        if($element.id == "erstzulassung_monat"){
                            $element.css('border','1px solid #ccc');
                        }
                    }
                }
            } else if (element.nodeName == "INPUT" && element.type === 'radio') {
                $element.closest('.row').find('.flag.error').remove();
            }else if(element.nodeName == "INPUT" && element.type === 'checkbox'){
              if(element.id == "reusecheckbox"){
                if($('#reusecheckbox').prop("checked")) {
                  if( $('#wiederherstellungswert').val() <= $('#marktwert').val() || $('#wiederherstellungswert').val() == ""){
                    $("#wiederherstellungswert").removeClass('has-no-error');
                    $("#wiederherstellungswert").addClass('has-error');
                  }
                }else{
                  $('#wiederherstellungswert').val("");
                  if($('#wiederherstellungswert').val() == ""){
                    $("#wiederherstellungswert").removeClass('has-no-error');
                    $("#wiederherstellungswert").removeClass('has-error');
                  }
                }
              }
            }else if(element.id == "wiederherstellungswert"){
              if($('#reusecheckbox').prop("checked") && $('#wiederherstellungswert').val() > $('#marktwert').val() ) {
                $("#" + element.id).addClass('has-error');
              }
            } else {
                $element.removeClass('has-no-error');
            }
        },
        onfocusout: function (element) {
//          console.log(element)
            if (element.nodeName == "SELECT") {
                if(element.value !== ""){

                    if(element.id !== "erstzulassung_monat" || element.id !== "erstzulassung"){
                        //$("#" + element.id).addClass('has-no-error');
                    }else{
                        $("#" + element.id).removeClass('has-error');
                        $("#" + element.id).removeClass('has-no-error');
                    }
                }else{
                    $("#" + element.id).removeClass('has-error');
                    $("#" + element.id).removeClass('has-no-error');
                    if($("#" + element.id+"-error").length > 0){
                        $("#" + element.id+"-error").remove();
                    }
                }
            } else {
                if(element.value == ""){
                    $("#" + element.id).removeClass('has-error');
                    $("#" + element.id).removeClass('has-no-error');
                    $('#'+element.id+'-error').remove();
              }else if(element.id == "fahrzeugkennzeichen"){
                    var akz = /[a-zöüäA-ZÖÜÄ]{1,3}\-[a-zöüäA-ZÖÜÄ]{1,2}[0-9]{1,4}/g;

                    var regexTest = akz.test(element.value);

                    if(!regexTest && element.value != ""){
                        $("#" + element.id).removeClass('has-no-error');
                        $("#" + element.id).addClass('has-error');
                        return false;
                    }else if(regexTest && element.value != ""){
                        $("#" + element.id).addClass('has-no-error');
                        this.element(element);
                        console.log("correct");
                    }else if(element.value == ""){
                        $("#" + element.id).removeClass('has-error');
                        $("#" + element.id).removeClass('has-no-error');
                    }
                }else if(element.id == "wiederherstellungswert"){
                if($('#reusecheckbox').prop("checked")) {
                  if( $('#wiederherstellungswert').val() < $('#marktwert').val() || $('#wiederherstellungswert').val() == ""){
                    $("#" + element.id).removeClass("has-no-error");
                    $("#" + element.id).addClass('has-error');
                  }else if($('#wiederherstellungswert').val() > $('#marktwert').val()){
                    $("#" + element.id).removeClass("has-error");
                    $("#" + element.id).addClass('has-no-error');
                  }
                }else{
                  if( $('#wiederherstellungswert').val() < $('#marktwert').val()){
                    $("#" + element.id).removeClass("has-no-error");
                    $("#" + element.id).addClass('has-error');
                  }else if($('#wiederherstellungswert').val() > $('#marktwert').val()){
                    $("#" + element.id).removeClass("has-error");
                    $("#" + element.id).addClass('has-no-error');
                  }
                }
              }else{
                    this.element(element);
              }
            }
        },
        onkeyup: false,
        errorElement: "span",
        errorClass: "flag error",
        errorPlacement: function(error, element){
            if ( element.is(":radio") ){
                error.appendTo( element.parents('.zustandsDiv') );
            }
            else{ // This is the default behavior
                if(element[0].id === "herstellerschluesselnummer" || element[0].id === "typschluesselnummer"){
                    error[0].style["z-index"] = 99999;
                    error.insertAfter( element );
                }else if(element[0].id === "erstzulassung_monat"){
                    $('#fahrzeugdaten2 > div:nth-child(9)').append(error);
                }else{
                    error.insertAfter( element );
                }
            }
        },
        rules: {
            selbstbehalt_paket: {
                required: true
            },
            optradio: {
                required: true
            },
            geplante_zulassung: {
                required: true
            },
            grund_der_versicherung: {
                required: true
            },
            fahrzeugkennzeichen: {
                required: function (element) {
                    return String($('#grund_der_versicherung :selected').val()) === "change";
                }
            },
            fin: {
                required: function (element) {
                    return String($('#grund_der_versicherung :selected').val()) === "change";
                }
            },
            herstellerschluesselnummer: {
                required: function (element) {
                    return String($('#grund_der_versicherung :selected').val()) === "change";
                }
            },
            typschluesselnummer: {
                required: function (element) {
                    return String($('#grund_der_versicherung :selected').val()) === "change";
                }
            },
            saison_start: {
                required: function (element) {
                    return String($('#kennzeichen').val()) === "105003";
                }
            },
            saison_ende: {
                required: function (element) {
                    return String($('#kennzeichen').val()) === "105003";
                }
            },
            erstzulassung_monat: {
                required: true
            },
            erstzulassung: {
                required: true
            },
            tacho_einheit: {
                required: true
            },
            tacho_stand: {
                required: true,
                number: true
            },
            fahrleistung: {
                required: true,
                number: true
            },
            marktwert: {
                required: true,
                number: true
            },
            wiederherstellungswert: {
                required: function (element) {
                    if(!$('#reusecheckbox').prop("checked")){
                        $('#wiederherstellungswert').removeClass('has-error');
                    }
                    return $('#reusecheckbox').prop("checked");
                },
                number: true
            },
            custom_type: {
                required: true
            },
            kennzeichen: {
                required: true
            },
            fahreralter: {
                required: true
            },
            leasing: {
                required: true
            },
            kaufjahr: {
                required: true
            }
        },
        groups:{
            erstzulassung: "erstzulassung_monat erstzulassung"
        },
        messages: {
            selbstbehalt_paket: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>'
            },
            optradio: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>'
            },
            geplante_zulassung: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>'
            },
            grund_der_versicherung: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>'
            },
            erstzulassung_monat: {
                required: '<div class="warning" style="display:hidden;"></div>',
            },
            erstzulassung: {
                required: '<div class="warning">Dies ist eine Pflichtangabe</div>',
            },
            fahrzeugkennzeichen: {
                required: '<div class="warning">'+infoMessageArray["fahrzeugkennzeichen"]+'</div>'
            },
            fin: {
                required: '<div class="warning">'+infoMessageArray["herstellerschluesselnummer"]+'</div>'
            },
            herstellerschluesselnummer: {
                required: '<div class="warning">'+infoMessageArray["herstellerschluesselnummer"]+'</div>'
            },
            typschluesselnummer: {
                required: '<div class="warning">'+infoMessageArray["typschluesselnummer"]+'</div>'
            },
            tacho_einheit: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>'
            },
            tacho_stand: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>',
                number: '<div class="warning">Ungültiger Tachostand. Bitte geben Sie nur Ziffern ein</div>'
            },
            fahrleistung: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>'
            },
            marktwert: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>',
                number: '<div class="warning">Ungültiger Betrag. Bitte geben Sie nur Ziffern ein</div>'
            },
            wiederherstellungswert: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>',
                number: '<div class="warning">Ungültiger Betrag. Bitte geben Sie nur Ziffern ein</div>'
            },
            custom_type: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe </div>'
            },
            kennzeichen: {
                required: '<div class="warning"> Dies ist eine Pflichtangabe</div>'
            },
            saison_start: {
                required: '<div class="warning">Dies ist eine Pflichtangabe</div>',
            },
            saison_ende: {
                required: '<div class="warning">Dies ist eine Pflichtangabe</div>',
            },
            fahreralter: {
                required: '<div class="warning">Dies ist eine Pflichtangabe</div>',
            },
            leasing: {
                required: '<div class="warning">Dies ist eine Pflichtangabe</div>',
            },
            kaufjahr: {
                required: '<div class="warning">Dies ist eine Pflichtangabe</div>',
            }
        },
        submitHandler: function (form) {

            if(sessionStorage.getItem("Fahrzeugdaten2") == null){
                var fromString = $('form[name="Fahrzeugdaten2"]').serializeAssoc();
                if($('#grund_der_versicherung :selected').val() === "change"){
                    var akz = /[a-zöüäA-ZÖÜÄ]{1,3}\-[a-zöüäA-ZÖÜÄ]{1,2}[0-9]{1,4}[hH]{0,1}/g;

                    if(akz.test($('#fahrzeugkennzeichen').val())){
                        var splittedAkz = $('#fahrzeugkennzeichen').val().replace(/$\H/,"").replace(/$\h/,"").split("-");
                        var partTwoAkz = splittedAkz[1].split(/[0-9]{1,4}/);
                        var partThreeAkz = splittedAkz[1].split(/[A-ZÖÜÄ]{1,2}/);
                        var partOne = "";
                        var partTwo = "";
                        var partThree = "";
                        if(splittedAkz[0].length < 3){
                            for( var i = 3, j=partOne.length; j <= i;j++) {
                                partOne = splittedAkz[0]+" ";
                            }
                        }else{
                            partOne = splittedAkz[0];
                        }
                        if(partTwoAkz[0].length < 2){
                            console.log(partTwo.length);
                            for( var i = 2, j=partTwo.length; j < i;j++) {
                                partTwo = partTwoAkz[0] + " ";
                            }
                        }else{
                            partTwo = partTwoAkz[0];
                        }
                        if(partThreeAkz[1].length < 4){
                            var blankFields = "";
                            for( var i = 4, j=partThreeAkz[1].length; j < i;j++) {
                                blankFields += " ";
                            }
                            partThree += blankFields+partThreeAkz[1];
                        }else{
                            partThree += partThreeAkz[1];
                        }
                        fromString["fahrzeugkennzeichen"] = partOne+partTwo+partThree;
                    }
                }
                sessionStorage.setItem("Fahrzeugdaten2", JSON.stringify(fromString));
            }
            form.submit();
            //return false;
        }
    });

    jQuery.extend(jQuery.validator.messages, {
        minlength: jQuery.validator.format('<div class="warning">'+infoMessageArray["typschluesselnummer"]+'</div>'),
    });


    HTMLElement.prototype.hasClass = function(cls) {
        var i;
        var classes = this.className.split(" ");
        for(i = 0; i < classes.length; i++) {
            if(classes[i] == cls) {
                return true;
            }
        }
        return false;
    };

});
